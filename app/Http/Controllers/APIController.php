<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\CoinTransactionHistory;
use App\GameMaster;
use App\Challenge;
use App\ChallengePlayer;
use App\Country;
use App\UserCoin;
use App\SteamUser;
use App\Region;
use App\Match;
use League\Flysystem\Exception;

class APIController extends Controller {

    protected $request;
    protected $noRegion = "Nor region found";

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->requestData = $request->all();
        $requestData =$request->all();
        if(!empty($requestData))
            $this->requestData =$requestData["Data"];
        //$this->middleware('auth');
    }

    public function get_http_response_code($url) {
        $headers = get_headers($url);
        return substr($headers[0], 9, 3);
    }

    public function connectApi() {
        try {
            if ($this->request->ajax()) {
                $requestData = $this->requestData;
                if (!empty($requestData["requestName"]) && !empty($requestData["region_id"])) {
                    $region_id = $requestData["region_id"];
                    $region = $this->getRegionDetail($region_id);
                    if(!$region){
                        return json_encode($this->noRegion);
                    }
                    $userId = 0;
                    if (!empty($requestData["userId"])) {
                        $userId = $requestData["userId"];
                    }
                    $lolUserID = DB::select("SELECT id FROM lol_user WHERE user_id = $userId LIMIT 0,1");
                    if (count($lolUserID) > 0) {
                        $data = $lolUserID;
                        $isSuccess = true;
                        $msg = "Already Connected";
                    } else {
                        $APIKEY = "RGAPI-00bec1c9-4570-43c9-910b-ae61fe5df766";
                        $region = $requestData["region_id"];
                        $summoner = $requestData["requestName"];
                        $steamurl = "https://$region.api.pvp.net/api/lol/$region_id/v1.4/summoner/by-name/$summoner?api_key=$APIKEY";
                        if($this->get_http_response_code($steamurl)== 200){
                            $json_object = file_get_contents($steamurl);
                            $json_object = json_decode($json_object);
                            $connectData = (array)$json_object;
                            if(isset($connectData[$summoner])) {
                                $summonerID = isset($connectData[$summoner]->id) ? $connectData[$summoner]->id : 0;
                                $region_id = $requestData["region_id"];
                                $regionid = DB::select("SELECT id FROM lu_region WHERE region_id ='".$region_id."'  LIMIT 0,1");

                                $insertData = array(
                                    'summoner_id' => $summonerID,
                                    'user_id' => $userId,
                                    'region_id' => $regionid[0]->id
                                );
                                $data = DB::table('lol_user')->insertGetId($insertData);
                                $msg = "Connected successfully";
                                $isSuccess  = true;
                            }else{
                                $isSuccess  = false;
                                $msg = "API error.".$connectData["status"]->message;
                                $data = "";
                            }
                        }
                        else{
                            $isSuccess  = false;
                            $msg =  "API Error";
                            $data = "";
                        }


                    }
                    $response = array("isSuccess" => $isSuccess, "message" => $msg, "Data" => $data);
                    return json_encode($response);

                } else {
                    $response = array("isSuccess" => fail, "message" => "Data not found");
                    return json_encode($response);
                }
            }
        }
        catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function getRegionDetail($regionID) {
        try {
            $host = DB::select("SELECT * FROM lu_region WHERE region_id = '$regionID'  LIMIT 0,1");
            if($host[0]){
                return !empty($host[0]->host)?$host[0]->host:false;
            }else{
                return false;
            }
        }
        catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }
    public function getMatchListAPI() {
        try {
            $region_id = 'euw';
            $region = $this->getRegionDetail($region_id);
            if(!$region){
                return json_encode($this->noRegion);
            }
            $summoner = 77580270;
            $APIKEY = "RGAPI-00bec1c9-4570-43c9-910b-ae61fe5df766";

            $steamurl = "https://$region/api/lol/$region_id/v2.2/matchlist/by-summoner/$summoner?api_key=$APIKEY";


            if($this->get_http_response_code($steamurl)== 200){
                $json_object = file_get_contents($steamurl);
                $json_object = json_decode($json_object);
                $connectData = (array)$json_object;
                if(isset($connectData)) {
                    $isSuccess  = true;
                    $msg = "success";
                    $data = $connectData;
                }else{
                    $isSuccess  = false;
                    $msg = "API error.".$connectData["status"]->message;
                    $data = "";
                }
            }
            else{
                $isSuccess  = false;
                $msg =  "API Error";
                $data = "";
            }
            $response = array("isSuccess" => $isSuccess, "message" => $msg, "Data" => $data);
            return json_encode($response);
        }
        catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function getMatchDetailAPI() {
        try {
            $region_id = 'euw';
            $region = $this->getRegionDetail($region_id);
            if(!$region){
                return json_encode($this->noRegion);
            }
            $matchID = '2693856172';
            $APIKEY = "RGAPI-00bec1c9-4570-43c9-910b-ae61fe5df766";

            $steamurl = "https://$region/api/lol/$region_id/v2.2/match/$matchID?api_key=$APIKEY";

            //print_r($steamurl);exit;

            if($this->get_http_response_code($steamurl)== 200 ){
                if(!file_get_contents($steamurl)){
                    return json_encode("Wait for a while");
                }
                $json_object = file_get_contents($steamurl);
                $json_object = json_decode($json_object);
                $connectData = (array)$json_object;
                if(isset($connectData)) {
                    $isSuccess  = true;
                    $msg = "success";
                    $data = $connectData;
                }else{
                    $isSuccess  = false;
                    $msg = "API error.".$connectData["status"]->message;
                    $data = "";
                }
            }
            else{
                $isSuccess  = false;
                $msg =  "API Error".$this->get_http_response_code($steamurl);
                $data = "";
            }
            $response = array("isSuccess" => $isSuccess, "message" => $msg, "Data" => $data);
            return json_encode($response);
        }
        catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }
    public function dashboard(){
    print_r("hi");exit;
    }


}
