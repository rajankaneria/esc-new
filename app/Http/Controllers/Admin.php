<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\GameMaster;
use App\Challenge;
use App\ChallengePlayer;
use App\User;
use App\UserDetail;
use App\Country;
use App\UserStatus;
use App\UserCoin;
use App\CoinTransactionHistory;
use App\GlobalSetting;
use App\Region;
use App\Match;

class Admin extends Controller {

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->middleware('auth');
        $this->middleware('checkSession');
    }

    function dashboard() {
        //add view specific js files to js_files array
        if (!Session::has('adminID')) {
            $js_files[] = "admin.login.js";
            $footerData["js_files"] = $js_files;

            $mainContentData = view("admin/login")->render();

            $headerData = array(); //anything that need to be passed in the header should go in this variable
        } else {
            $js_files[] = "admin.dashboard.js";
            $footerData["js_files"] = $js_files;
            $viewData = array(
                "sideBarMenu" => view("admin/sideBarMenu", array("currentView" => "dashboard"))->render()
            );
            $mainContentData = view("admin/dashboard", $viewData)->render();

            $headerData = array(); //anything that need to be passed in the header should go in this variable
        }
        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;

        return view("admin", $data);
    }

    function loginCheck(Request $request) {
        $email = $request["email"];
        
        $password = sha1($email . $request["password"]);

        $userQuery = DB::select("select * from users where email='$email' and password='$password' and role=0");
        if (sizeof($userQuery) == 0) {
            $result = array('status' => 'fail');
        } else {
            $adminObject = $userQuery[0];
            $adminID = $adminObject->id;
            Session::set('adminID', $adminID);
            $result = array('status' => 'success');
        }
        return json_encode($result);
    }

    function logout() {
        Session::flush();
        return redirect()->action('Admin@dashboard');
    }

    function gameCreate() {
        $js_files = array('admin.createGame.js', 'jquery.validate.min.js');
        $footerData["js_files"] = $js_files;

        $viewData = array(
            "sideBarMenu" => view("admin/sideBarMenu", array("currentView" => "gameCreate"))->render()
        );
        /*
         * Get the game data with the respective challenges
         * 
         */
        $challengeData = Challenge::with('Game')->orderBy('created_at', 'desc')->get();

        /*
         * Get Game Names 
         * 
         */
        $gameMaster = GameMaster::get();
        /*
         * Get Region data
         */
        $regionMasterData = Region::get();
        /*
         * Match Master Data
         */
        $matchMasterData = Match::get();

        $viewData['challengeData'] = $challengeData;
        $viewData['MasterData'] = $gameMaster;
        $viewData['regionMasterData'] = $regionMasterData;
        $viewData['matchMasterData'] = $matchMasterData;

        $mainContentData = view("admin/gameCreate", $viewData)->render();

        $headerData = array(); //anything that need to be passed in the header should go in this variable
        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;

        if ($this->request->ajax()) {

            $challenge = new Challenge;
            $referer = $_SERVER['HTTP_REFERER'];
            if (strpos($referer, 'admin') !== false) {
                $challenge->userId = Session::get('adminID');
            } else {
                $challenge->userId = Session::get('user')->id;
            }
            $challenge->game_master_id = $this->request->input('gameName');
            $challenge->amount = $this->request->input('gameAmount');
            $challenge->isSolo = $this->request->input('gameSolo');
            $challenge->region_id = $this->request->input('region_id');
            $challenge->match_id = $this->request->input('match_id');
            $challenge->validUpto = $this->request->input('validUpto');
            if ($challenge->save()) {
                $challengePlayer = new ChallengePlayer;

                $challengePlayer->challenge_id = $challenge->id;
                $challengePlayer->user_id = $challenge->userId;

                if ($challengePlayer->save()) {
                    $result = array('status' => 'success');
                }
            }

            return json_encode($result);
        }

        return view("admin", $data);
    }

    function users() {
        $js_files = array('jquery.validate.min.js');
        $footerData["js_files"] = $js_files;

        $viewData = array(
            "sideBarMenu" => view("admin/sideBarMenu", array("currentView" => "users"))->render()
        );

        $usersData = User::with('UserDetail')->orderBy('created_at', 'desc')->get();
        $viewData['usersData'] = $usersData->toArray();

        $mainContentData = view("admin/users", $viewData)->render();

        $headerData = array(); //anything that need to be passed in the header should go in this variable
        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;

        return view("admin", $data);
    }

    function userEdit($userId) {
        $result = array('status' => 'success');
        $js_files = array('admin.userEdit.js', 'jquery.validate.min.js');
        $footerData["js_files"] = $js_files;

        $viewData = array(
            "sideBarMenu" => view("admin/sideBarMenu", array("currentView" => "users"))->render()
        );

        $userData = User::with('UserDetail')->where('id', $userId)->orderBy('created_at', 'desc')->first();

        $viewData['user'] = $userData->toArray();
        $viewData['countries'] = Country::get();
        $viewData['status'] = UserStatus::get();

        $mainContentData = view("admin/userEdit", $viewData)->render();

        $headerData = array(); //anything that need to be passed in the header should go in this variable
        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;
        if ($this->request->ajax()) {
            if (!empty($this->request->input('role'))) {
                if (trim($this->request->input('role')) == 'no') {
                    $role = 1;
                } else {
                    $role = 0;
                }
                $updateRole = DB::table('users')
                        ->where('id', $this->request->input('id'))
                        ->update(array(
                    'role' => $role,
                ));
            }
            $updateProfile = DB::table('user_detail')
                    ->where('id', $this->request->input('id'))
                    ->update(array(
                //'username' => $this->request->input('username'),
                'first_name' => $this->request->input('first_name'),
                'last_name' => $this->request->input('last_name'),
                'mobile_number' => $this->request->input('mobile_number'),
                'address_1' => $this->request->input('address_1'),
                'address_2' => $this->request->input('address_2'),
                'pincode' => $this->request->input('pincode'),
                'city' => $this->request->input('city'),
                'state' => $this->request->input('state'),
                'country_id' => $this->request->input('country_id'),
                'status_id' => $this->request->input('status_id')
            ));
            if ($updateProfile == 1) {
                $result = array('status' => 'success');
            }
            return json_encode($result);
        }
        return view("admin", $data);
    }

    function accountManage($userId) {
        $result = array('status' => 'success');
        $js_files = array('admin.userEdit.js', 'jquery.validate.min.js');
        $footerData["js_files"] = $js_files;

        $viewData = array(
            "sideBarMenu" => view("admin/sideBarMenu", array("currentView" => "account"))->render()
        );

        $userAccountData = UserCoin::where('user_id', $userId)->first();
        if (!empty($userAccountData)) {
            $viewData['account'] = $userAccountData->toArray();
        }

        $mainContentData = view("admin/userAccount", $viewData)->render();

        $headerData = array(); //anything that need to be passed in the header should go in this variable
        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;
        if ($this->request->ajax()) {
            $updateAccount = DB::table('user_coins')
                    ->where('user_id', $this->request->input('user_id'))
                    ->update(array(
                'amount' => $this->request->input('amount'),
            ));
            if ($updateAccount == 1) {

                $coinTransactionHistory = new CoinTransactionHistory;
                $coinTransactionHistory->user_id = $this->request->input('user_id');
                $coinTransactionHistory->credit = $this->request->input('amount'); //Total remaining amout 
                $coinTransactionHistory->debit = 0; //amount paid at the time of transaction
                $coinTransactionHistory->source_id = 7;
                $coinTransactionHistory->transaction_date = date('Y-m-d H:i:s');
                $coinTransactionHistory->save();
                $result = array('status' => 'success');
            }
            return json_encode($result);
        }
        return view("admin", $data);
    }

    function settings() {
        $result = array('status' => 'success');
        $js_files = array('admin.userEdit.js', 'jquery.validate.min.js');
        $footerData["js_files"] = $js_files;

        $viewData = array(
            "sideBarMenu" => view("admin/sideBarMenu", array("currentView" => "settings"))->render()
        );

        $settingData = GlobalSetting::first();
        if (!empty($settingData)) {
            $viewData['setting'] = $settingData->toArray();
        }

        $mainContentData = view("admin/settings", $viewData)->render();

        $headerData = array(); //anything that need to be passed in the header should go in this variable
        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;
        if ($this->request->ajax()) {
            $updateSetting = DB::table('global_settings')
                    ->where('id', $this->request->input('id'))
                    ->update(array(
                'commision' => $this->request->input('commision'),
            ));
            if ($updateSetting == 1) {
                $result = array('status' => 'success');
            }
            return json_encode($result);
        }
        return view("admin", $data);
    }

}
