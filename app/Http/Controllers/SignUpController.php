<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\CoinTransactionHistory;
use App\GameMaster;
use App\Challenge;
use App\ChallengePlayer;
use App\Country;
use App\UserCoin;
use App\SteamUser;
use App\Region;
use App\Match;

class SignUpController extends Controller {

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->requestData = $request->all();
        $requestData =$request->all();
        if(!empty($requestData))
            $this->requestData =$requestData["Data"];
        //$this->middleware('auth');
    }


    /**
     * Registration for the Front end User
     * 
     */
    public function saveUser() {

        if ($this->request->ajax()) {
            $requestData =$this->requestData;
            //$requestData =$requestData["Data"];
            if (!empty($requestData["email"]) && !empty($requestData["password"])) {

                $emailExists = DB::select("
                                            SELECT id FROM users 
                                            where email = '".$requestData["email"]."'
                                        ");
                if($emailExists){
                    $response = array("isSuccess"=>false,"message"=>"Email already registered");
                    return json_encode($response);
                }
                $insertData = array( 'email' => $requestData["email"],
                                     'password' => sha1($requestData["email"] . $requestData["password"]),
                                     'role' => 1,
                                     'created_at' => date("Y-m-d H:i:s"),
                                     'updated_at' => date("Y-m-d H:i:s"),
                                     'ip_address' => get_client_ip(),
                                     'status_id' => 1,
                                );
                $id =  DB::table('users')->insertGetId($insertData);
                $esc_id = time() . $id;
                $escId = DB::table('users')
                            ->where('id', $id)
                            ->update(array('esc_id' => $esc_id));

                $insertUserDetail = array(  'login_id' => $id,
                                            'email' => $requestData["email"],
                                            'esc_id' => $escId,
                                            'status_id' => 1,
                                            'created_at' => date("Y-m-d H:i:s"),
                                            'updated_at' => date("Y-m-d H:i:s")
                                        );
                DB::table('user_detail')->insertGetId($insertUserDetail);

                $insertUserCoin = array(    'user_id' => $id,
                                            'amount' => 0
                                        );
                DB::table('user_coins')->insertGetId($insertUserCoin);
                $returnData = array("userId"=>$id,"escId"=>$esc_id);
                Session::put('escLoginData',$returnData);
                $response = array("isSuccess"=>true,"message"=>"Data saved successfully","Data"=>$returnData);
                return json_encode($response);

            } else {
                $response = array("isSuccess"=>false,"message"=>"Incorrect data");
                return json_encode($response);
            }
        }
    }

    /**
     * Do Login for Front End User
     */
    public function login() {

        try {
            if ($this->request->ajax()) {
                $loginData = $this->requestData;
                if (!empty($loginData["email"]) && !empty($loginData["password"])) {
                    if ($this->checkUser($loginData["email"], sha1($loginData["email"] . $loginData["password"]))) {
                        $loginId = DB::table('users')
                            ->where('email', $loginData["email"])
                            ->update(array('last_login' => date("Y-m-d H:i:s")));

                        $response = array("isSuccess" => true, "message" => "Login success");
                        return json_encode($response);
                    } else {
                        $response = array("isSuccess" => false, "message" => "Incorrect data");
                        return json_encode($response);
                    }
                }
            } else {
                $response = array("isSuccess" => false, "message" => "Incorrect data");
                return json_encode($response);
            }
        }catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    /**
     *
     * Common Function for the checking front end user login
     *
     * @param type $email,$passsword
     * @return type bool
     */
    public function checkUser($email = '', $password = '') {
        $user = DB::table('users')
            ->where('email', $email)
            ->where('password', $password)
            ->where('status_id', 1)
            //->where('role', 1)
            ->first();

        if (!empty($user)) {
            $returnData = array("userId"=>$user->id,"escId"=>$user->esc_id);
            Session::put('escLoginData',$returnData);
            return true;
        } else {
            return false;
        }
    }

    public function logout() {
        Session::flush();
        return redirect('/');
    }


}
