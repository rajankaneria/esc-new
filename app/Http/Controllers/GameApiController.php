<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class GameApiController extends Controller {

    function recentMatches($summonerID) {

        $host = "na.api.pvp.net";
        $apiKey = "RGAPI-00bec1c9-4570-43c9-910b-ae61fe5df766";
        $url = "https://".$host."/api/lol/na/v1.3/game/by-summoner/".$summonerID."/recent?api_key=".$apiKey;
        
        $json_object= json_decode(file_get_contents($url));
        
        $lastGame = $json_object->games[0];
        $gameData = array(
            "gameId" => $lastGame->gameId,
            "gameMode" => $lastGame->gameMode,
            "gameType" => $lastGame->gameType,
            
        );
        $userData = array(
            "summonerId" => $summonerID,
            "teamId" => $lastGame->teamId,
            "championId" => $lastGame->championId
        );
        $opponentData = $lastGame->fellowPlayers;

        var_dump($gameData);
        var_dump($userData);
        var_dump($opponentData);

    }

    function matchDetails($matchID){
        $host = "na.api.pvp.net";
        $apiKey = "RGAPI-00bec1c9-4570-43c9-910b-ae61fe5df766";
        $url = "https://".$host."/api/lol/na/v2.2/match/".$matchID."?api_key=".$apiKey;
        $json_object= json_decode(file_get_contents($url));
    }
}