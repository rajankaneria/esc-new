<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\CoinTransactionHistory;
use App\GameMaster;
use App\Challenge;
use App\ChallengePlayer;
use App\Country;
use App\UserCoin;
use App\SteamUser;
use App\Region;
use App\Match;

class HomeController extends Controller {

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {

        if(Session::get('escLoginData')){
            return Redirect::to('/dashboard')->send();
        }
        $gamesList = $this->gamesList();
        $data["headerData"] = array();
        $data["mainContent"] = array("gamesList"=>$gamesList);
        $data["footerData"] = array();
        $data["contentView"] = "rightSideBar";
        $data["jsRequire"] = array("models/signupmodel.js","models/homemodel.js");
        $data["include"] = array("frontend/signup_popup");
        return view('frontend/home',$data)->render();
    }

    public function gamesData($gameID = 0) {
        $gamesList = $this->gamesList();
        $otherData = array();
        if($gameID>0){
            $otherData["gameName"] = $gameID." Dota";
        }

        $data["headerData"] = array();
        $data["mainContent"] = array("gamesList"=>$gamesList);
        $data["footerData"] = array();
        $data["contentView"] = "rightSideBar";
        $data["jsRequire"] = array("models/signupmodel.js","models/homemodel.js");
        $data["include"] = array("frontend/signup_popup");
        return view('frontend/home',$data)->render();
    }

    /**
     * Registration for the Front end User
     * 
     */
    public function create() {
print_r("hi");exit;
        if ($this->request->ajax()) {
            if (!empty($this->request->input('captcha')) && Session::get('code') == $this->request->input('captcha')) {
                $this->validate($this->request, [
                    'email' => 'required',
                    'password' => 'required',
                    'identityCheckbox' => 'required',
                ]);
                $user = new User;

                $checkUserExist = DB::table('users')
                        ->where('email', $this->request->input('email'))
                        ->where('status_id', 1)
                        //->where('role', 1)
                        ->count();

                if ($checkUserExist == 0) {
                    $user->email = $this->request->input('email');
                    $user->password = sha1($user->email . $this->request->input('password'));
                    $user->ip_address = get_client_ip();
                    $user->status_id = 1;

                    if ($user->save()) {
                        $userDetail = new UserDetail;

                        $userDetail->login_id = $user->id;
                        $userDetail->email = $user->email;
                        $userDetail->esc_id = time() . $user->id;
                        $userDetail->status_id = 1;
                        if ($userDetail->save()) {
                            $user->id = $user->id;
                            $user->esc_id = $userDetail->esc_id;
                            $user->save();

                            $userCoin = new UserCoin;
                            $userCoin->user_id = $user->id;
                            $userCoin->save();
                        }
                        if ($this->checkUser($user->email, $user->password)) {
                            return 1;
                        }
                    }
                } else {
                    return 0;
                }
            } else {
                return -1;
            }
        }
    }

    /**
     * Do Login for Front End User
     */
    function doLogin() {
        if ($this->request->ajax()) {
            $this->validate($this->request, [
                'email_address' => 'required',
                'login_password' => 'required'
            ]);
            if ($this->checkUser($this->request->input('email_address'), sha1($this->request->input('email_address') . $this->request->input('login_password')))) {
                return 1;
            } else {
                return 0;
            }
        }
    }

    function doLogout() {
        Session::flush();
        return redirect('/');
    }

    /**
     * 
     * Common Function for the checking front end user login
     * 
     * @param type $email,$passsword
     * @return type bool
     */
    protected function checkUser($email = '', $password = '') {
        $user = DB::table('users')
                ->where('email', $email)
                ->where('password', $password)
                ->where('status_id', 1)
                //->where('role', 1)
                ->first();

        if (!empty($user)) {
            Session::set('user', $user);
            return true;
        } else {
            return false;
        }
    }

    /**
     * 
     * Function to update the challenges when logged in user accept/reject the challenge
     *
     */
    function challengeAccept() {
        $result = array('status' => 'error', 'msg' => 'Invalid Request.');
        if ($this->request->ajax()) {
            if (!empty($this->request->input('amount')) && !empty($this->request->input('challengeId')) && !empty($this->request->input('userId'))) {
                // <editor-fold defaultstate="collapsed" desc="Check if the current user has any active challanges or not. If count 0 then proceed otherwise not">
                $user_id = Session::get('user')->id;
                $checkActiveChallenge = DB::select("
                        select count(*) as counter from challenge_players cp
                        join challenge c on cp.challenge_id = c.id
                        where c.isAccepted = 1 and c.challengeStatus = 2 and user_id ='" . $user_id . "' 
                    ");
                if ($checkActiveChallenge[0]->counter > 0) {
                    $result = array('status' => 'error', 'msg' => 'Sorry, You have already active challange there!');
                    return json_encode($result);
                }
                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="Check if challenge id is valid or not">
                $challenge = DB::table('challenge')
                        ->where('id', $this->request->input('challengeId'))
                        ->where('isAccepted', 0)
                        ->first();
                // </editor-fold>
                if (!empty($challenge)) {
                    // <editor-fold defaultstate="collapsed" desc="Check if user has enough funds available in his account">
                    $checkBalance = DB::table('user_coins')
                            ->where('user_id', $this->request->input('userId'))
                            ->first();
                    // </editor-fold>
                    if (!empty($checkBalance)) {
                        // <editor-fold defaultstate="collapsed" desc="If the available balance is lower than the amount of challenge, return error.">
                        if (($checkBalance->amount) < ($this->request->input('amount'))) {
                            $result = array('status' => 'error', 'msg' => 'You do not have enough balanace to accept the challange.');
                            return json_encode($result);
                        }
                        // </editor-fold>
                        // <editor-fold defaultstate="collapsed" desc="Update the challenge details like accepted flag,status etc.">
                        $challengeUpdate = DB::table('challenge')
                                ->where('id', $this->request->input('challengeId'))
                                ->update(array('isAccepted' => 1, 'challengeStatus' => 2));
                        // </editor-fold>
                        // <editor-fold defaultstate="collapsed" desc="Insert the challenge details like oppUSer to the Challenge Players.">
                        $challengePlayer = new ChallengePlayer;
                        $challengePlayer->challenge_id = $this->request->input('challengeId');
                        $challengePlayer->user_id = Session::get('user')->id; // Opponent user id
                        $challengePlayer->save();
                        // </editor-fold>

                        if ($challengeUpdate == 1) {
                            $totalAmount = ($checkBalance->amount) - ($this->request->input('amount'));
                            // <editor-fold defaultstate="collapsed" desc="This section is for maintaining user's transaction history">
                            $coinTransactionHistory = new CoinTransactionHistory;

                            $coinTransactionHistory->user_id = Session::get('user')->id;
                            $coinTransactionHistory->credit = $totalAmount; //Total remaining amout 
                            $coinTransactionHistory->debit = $this->request->input('amount'); //amount paid at the time of transaction
                            $coinTransactionHistory->source_id = 1;
                            $coinTransactionHistory->transaction_date = date('Y-m-d H:i:s');
                            $coinTransactionHistory->save();
                            // </editor-fold>
                            // <editor-fold defaultstate="collapsed" desc="This section will maintain user's coin table and update the total amount">
                            $userCoinUpdate = DB::table('user_coins')
                                    ->where('id', $checkBalance->id)
                                    ->update(array('amount' => $totalAmount));
                            // </editor-fold>
                            $result = array('status' => 'success', 'challengeId' => $this->request->input('challengeId'), 'userId' => $challenge->userId);
                        }
                    }
                }
            }
        }
        return json_encode($result);
    }

    /**
     * 
     * Function to handle user challenges 
     *
     * @param $id 
     */
    function challenge() {
        if (Session::has('adminID')) {
            $user = DB::table('users')
                    ->where('id', Session::get('adminID'))
                    ->where('status_id', 1)
                    ->first();
            if (!empty($user)) {
                Session::set('user', $user);
            }
        }
        $js_files = array('challenges.js');
        $footerData["js_files"] = $js_files;
        $viewData = array(
        );
        /*
         * Fetch the challenges data
         */
        $viewData["challanges"] = array();
        if (isset(Session::get('user')->id)) {
            $challengeData = Challenge::with('User')->with('Game')->where('isAccepted', 0)->get();
            if(!empty($challengeData)){
                $viewData["challanges"] = $challengeData->toArray();
            }
        }
//        echo '<pre>';
//        print_r($viewData["challanges"]);
//        echo '</pre>';
//        die;
        $mainContentData = view("frontend/challenge", $viewData)->render();

        $headerData = array(); //anything that need to be passed in the header should go in this variable

        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;

        return view("frontend", $data);
    }

    function accountSetting() {

        /*
         * Get Country Data
         */
        $countryData = Country::get();
        /*
         * Get Profile Data
         */
        $profileData = UserDetail::where('login_id', Session::get('user')->id)->first();
        /*
         * Get Steam Id if any
         */
        $steamData = SteamUser::where('user_id', Session::get('user')->id)->first();

        $js_files = array('accountSetiing.js');
        $footerData["js_files"] = $js_files;
        $viewData = array(
        );

        $viewData["countries"] = $countryData;
        $viewData["profile"] = $profileData;
        if (!empty($steamData)) {
            $viewData["steamData"] = $steamData;
        }
        $mainContentData = view("frontend/accountSetting", $viewData)->render();
        $headerData = array(); //anything that need to be passed in the header should go in this variable

        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;

        return view("frontend", $data);
    }

    function updateProfile() {
        $result = array('status' => 'error', 'msg' => 'Invalid Request.');
        if ($this->request->ajax()) {
            $profileData = UserDetail::where('login_id', Session::get('user')->id)->first();
            if (!empty($profileData)) {
                $updateProfile = DB::table('user_detail')
                        ->where('id', $profileData->id)
                        ->update(array(
                    'username' => $this->request->input('username'),
                    'first_name' => $this->request->input('first_name'),
                    'last_name' => $this->request->input('last_name'),
                    'mobile_number' => $this->request->input('mobile_number'),
                    'address_1' => $this->request->input('address_1'),
                    'address_2' => $this->request->input('address_2'),
                    'pincode' => $this->request->input('pincode'),
                    'city' => $this->request->input('city'),
                    'state' => $this->request->input('state'),
                    'country_id' => $this->request->input('country_id')
                ));

                if ($updateProfile == 1) {
                    $result = array('status' => 'success');
                }
            }
        }
        return json_encode($result);
    }

    function steamLogin() {
        $opID = new \LightOpenID(url('/accountSetting'));
        if (!$opID->mode) {
            $opID->identity = 'http://steamcommunity.com/openid/?l=english';
            return Redirect::to($opID->authURL());
        } elseif ($opID->mode == 'cancel') {
            return "User has canceled the authentication";
        } else {
            # validation here
            if ($opID->validate()) {
                $id = $opID->identity;
                // identity is something like: http://steamcommunity.com/openid/id/76561197960435530
                // we only care about the unique account ID at the end of the URL.
                $ptn = "/^http:\/\/steamcommunity\.com\/openid\/id\/(7[0-9]{15,25}+)$/";
                preg_match($ptn, $id, $matches);
                $steamid = $matches[1];
                if (!empty(Session::get('user')->id) && !empty($steamid)) {
                    $this->storeSteamUser($steamid, Session::get('user')->id);
                }
                return Redirect::to(url('/accountSetting'));
            } else {
                return 'Not logged';
            }
        }
    }

    function storeSteamUser($steamid = '', $userId = '') {
        $checkSteamUser = SteamUser::where('steam_id', $steamid)->first();
        if (!empty($checkSteamUser)) {
            $updateSteamUser = DB::table('steam_user')
                    ->where('steam_id', $checkSteamUser->steam_id)
                    ->update(array('user_id' => $userId));
        } else {
            $steamUser = new SteamUser;
            $steamUser->steam_id = $steamid;
            $steamUser->user_id = $userId;
            $steamUser->save();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="Create Challenge">
    function createChallenge() {
        $js_files = array('createChallenge.js', 'jquery.validate.min.js');
        $footerData["js_files"] = $js_files;
        $viewData = array(
        );

        /*
         * Get the game data with the respective challenges
         * 
         */
        $challengeData = Challenge::with('Game')->orderBy('created_at', 'desc')->get();

        /*
         * Get Game Names 
         * 
         */
        $gameMaster = GameMaster::get();
        /*
         * Get Region data
         */
        $regionMasterData = Region::get();
        /*
         * Match Master Data
         */
        $matchMasterData = Match::get();
        
        $viewData['challengeData'] = $challengeData;
        $viewData['MasterData'] = $gameMaster;
        $viewData['regionMasterData'] = $regionMasterData;
        $viewData['matchMasterData'] = $matchMasterData;

        $mainContentData = view("frontend/createChallenge", $viewData)->render();
        $headerData = array(); //anything that need to be passed in the header should go in this variable

        $data["headerData"] = $headerData;
        $data["mainContent"] = $mainContentData;
        $data["footerData"] = $footerData;

        return view("frontend", $data);
    }

    // </editor-fold>

    function reChallenge($challengeId) {
        if (!empty($challengeId)) {
            // <editor-fold defaultstate="collapsed" desc="Get the data of given challange Id">
            $previousChallenge = DB::table('challenge')
                    ->where('id', $challengeId)
                    ->first();
            // </editor-fold>

            if (!empty($previousChallenge)) {
                $challenge = new Challenge;

                $challenge->userId = Session::get('user')->id;
                $challenge->game_master_id = $previousChallenge->game_master_id;
                $challenge->amount = $previousChallenge->amount;
                $challenge->isSolo = $previousChallenge->isSolo;
                $challenge->region_id = $previousChallenge->region_id;
                $challenge->validUpto = $previousChallenge->validUpto;
                if ($challenge->save()) {
                    $challengePlayer = new ChallengePlayer;

                    $challengePlayer->challenge_id = $challenge->id;
                    $challengePlayer->user_id = $challenge->userId;

                    if ($challengePlayer->save()) {
                        return Redirect::to(url('/'));
                    }
                }
            }
        }
    }

}
