<?php 
namespace App\Http\Controllers;
use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Steam extends Controller{

    function steam64to3($id)
    {
        $result = substr($id, 3) - 61197960265728;
        return (string) $result;
    }
    function steam3to64($id)
    {
        $result = '765'.($id + 61197960265728);
        return (string) $result;
    }

    function matchDetails($matchID)
    {
        $APIKEY = "28C04A230D7FD71743A8C24AD10B56EB";
		$steamurl = "https://api.steampowered.com/IDOTA2Match_570/GetMatchDetails/v1/?key=$APIKEY&format=json&match_id=$matchID";
		$json_object= file_get_contents($steamurl);
		//header('Content-Type: application/json');
		$json_object = json_decode($json_object);

		$radient_win = $json_object->result->radiant_win;
		$playerArray = (array) $json_object->result->players;
		$result = array();
		foreach($playerArray as $playerObject)
		{
			$playerObject->account_id = $this->steam3to64($playerObject->account_id);
			$playerArray = (array) $playerObject;
			if($playerArray["player_slot"] > 8)
			{ 
				$playerArray["teamName"] = "Dire"; 
			}
			else
			{ 
				$playerArray["teamName"] = "Radiant"; 
			}
			$result[] = $playerArray;
		}

		if($radient_win == 1){ $viewMessage = "Radiants Won the Match"; }else{  $viewMessage = "Radiants Won the Match";  }
        echo $viewMessage;
        var_dump($result);
    }

    function dota2history($steamID,$matches)
    {
        //test steamid is : 76561198205411648
        $APIKEY = "28C04A230D7FD71743A8C24AD10B56EB";
		$steamurl = "https://api.steampowered.com/IDOTA2Match_570/GetMatchHistory/v1/?key=$APIKEY&account_id=$steamID&Matches_Requested=$matches&format=json";
		$json_object= file_get_contents($steamurl);
		//header('Content-Type: application/json');
		$json_object = json_decode($json_object);
		$matches = $json_object->result->matches;
		$result = array();
		foreach($matches as $key=>$matchDetails)
		{
			$matchID = $matchDetails->match_id;
			$result[] = array(
				"matchID" => $matchID,
				"matchStartTime" => date('Y-m-d H:i:s', $matchDetails->start_time)
			);
		}
        var_dump($result);
    }

}