<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\Chat;
use App\Challenge;

class ChatController extends Controller {

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->middleware('auth');
    }

    public static function chat() {
        $user_id = Session::get('user')->id;
        $checkActiveChallenge = DB::select("
                        select count(*) as counter ,cp.challenge_id from challenge_players cp
                        join challenge c on cp.challenge_id = c.id
                        where c.isAccepted = 1 and c.challengeStatus = 2 and user_id ='" . $user_id . "' 
                    ");

        if ($checkActiveChallenge[0]->counter > 0) {
            $challengePlayer = DB::table('challenge_players')
                    ->where('challenge_id', $checkActiveChallenge[0]->challenge_id)
                    ->where('user_id', '!=', Session::get('user')->id)
                    ->first();
        }
        //$js_files = array('chat.js');
        //$footerData["js_files"] = $js_files;
        //$viewData = array();

        $chats = array();
        // </editor-fold>
        if (!empty($challengePlayer)) {
            $viewData["to_id"] = $challengePlayer->user_id;

            $playerID = $challengePlayer->user_id;
            $userID = Session::get('user')->id;

            $chats = DB::select("
                        select c.*,tu.first_name as to_name,fu.first_name as from_name from chat c
                        join user_detail tu on c.to_id = tu.login_id
                        join user_detail fu on c.from_id = fu.login_id 
                        where (to_id='$playerID' and from_id='$userID') or (to_id='$userID' and from_id='$playerID')
                        order by created_at asc
                    ");
            $challengeId = $checkActiveChallenge[0]->challenge_id;
            /*
             * Game Result Block
             */
            if (!empty($chats)) {
                foreach ($chats as $key => $chatValue) {
                        $gameResults = \App\GameResult::where('challenge_id', $chatValue->challenge_id)->first();
                        if (!empty($gameResults)) {
                            $gameDetails = Challenge::with('Game')->where('id', $chatValue->challenge_id)->first();
                            if (!empty($gameDetails)) {
                                $winnerUserDetail = UserDetail::where('login_id', $gameResults->winner_user_id)->first(['first_name']);
                            }
                            $chats['gameDetails'] = $gameDetails;
                            $chats["winnerUserDetail"] = $winnerUserDetail;
                        }
                    
                }
            }
            /*
             * Game Result Block Over
             */
            $update = DB::select("
                        update chat set isRead=1
                        where to_id='$userID' and from_id='$playerID' and isRead='0'
                    ");
        }
        $viewData["chats"] = $chats;
        if (!empty($checkActiveChallenge[0]->challenge_id)) {
            $viewData["challenge_id"] = $checkActiveChallenge[0]->challenge_id;
        }
        //$mainContentData = view("frontend/chat", $viewData)->render();
        //$headerData = array(); //anything that need to be passed in the header should go in this variable
        //$data["headerData"] = $headerData;
        //$data["mainContent"] = $mainContentData;
        //$data["footerData"] = $footerData;
        //return view("frontend", $data);
        return $viewData;
    }

    function doMessage() {
        $result = array('status' => 'error', 'msg' => 'Invalid Request.');
        if ($this->request->ajax()) {
            $chat = new Chat;
            $chat->challenge_id = $this->request->input('challenge_id');
            $chat->to_id = $this->request->input('to_id');
            $chat->from_id = Session::get('user')->id;
            $chat->message = $this->request->input('message');
            if ($chat->save()) {
                $chatInsertID = $chat->id;
                $chats = DB::select("
                    select c.*,tu.first_name as to_name,fu.first_name as from_name from chat c
                    join user_detail tu on c.to_id = tu.login_id
                    join user_detail fu on c.from_id = fu.login_id 
                    where c.id = '$chatInsertID'
                ");
                $viewData["chats"] = $chats;
                $returnHTML = view('frontend/chatAjax', $viewData)->render();
                $result = array('status' => 'success', 'message' => $chat->message, 'html' => $returnHTML);
            }
        }
        return json_encode($result);
    }

    public static function getChats($challengeId) {
        $result = array('status' => 'error');
        if (!Session::has('challenge' . $challengeId)) {
            $challengePlayer = DB::table('challenge_players')
                    ->where('challenge_id', $challengeId)
                    ->where('user_id', '!=', Session::get('user')->id)
                    ->first();
        } else {
            $challengePlayer = Session::get('challenge' . $challengeId);
        }
        if (!empty($challengePlayer) && !empty($challengePlayer->user_id)) {
            $playerID = $challengePlayer->user_id;
            $userID = Session::get('user')->id;
            $chats = DB::select("
                select c.*,tu.first_name as to_name,fu.first_name as from_name from chat c
                join user_detail tu on c.to_id = tu.login_id
                join user_detail fu on c.from_id = fu.login_id 
                where to_id='$userID' and from_id='$playerID' and isRead=0
                order by created_at asc
            ");
        }
        if (!empty($chats)) {

            $update = DB::select("
                        update chat set isRead=1
                        where to_id='$userID' and from_id='$playerID' and isRead='0'
                        ");

            $viewData["chats"] = $chats;
            $returnHTML = view('frontend/chatAjax', $viewData)->render();
            $result = array('status' => 'success', 'html' => $returnHTML);
        }
        return json_encode($result);
    }

}
