<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Report;

class ReportController extends Controller {

    function report($challengeId) {
        if (!empty($challengeId)) {
            // <editor-fold defaultstate="collapsed" desc="Get the data of given challange Id">
            $previousChallenge = DB::table('challenge')
                    ->where('id', $challengeId)
                    ->first();
            // </editor-fold>
            if (!empty($previousChallenge)) {
                $report = new Report;
                //$this->request->input('email');
                $report->from_id = Session::get('user')->id;
                $report->to_id = 2;
                $report->message = "This is abuse";
                $report->report_type_id = 2;
                $report->challenge_id = $challengeId;

                if ($report->save()) {
                    return Redirect::to(url('/'));
                }
            }
        }
    }

}
