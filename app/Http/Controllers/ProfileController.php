<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\CoinTransactionHistory;
use App\GameMaster;
use App\Challenge;
use App\ChallengePlayer;
use App\Country;
use App\UserCoin;
use App\SteamUser;
use App\Region;
use App\Match;

class ProfileController extends Controller {

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->requestData = $request->all();
        $requestData =$request->all();
        if(!empty($requestData))
            $this->requestData =$requestData["Data"];
        //$this->middleware('auth');
    }


    /**
     * Profile page of the user
     * 
     */
    public function index() {
        try {
            $loginData = Session::get('escLoginData');
            $userData = DB::select(" SELECT * FROM users  WHERE id = '" . $loginData["userId"] . "' ");
            $data["headerData"] = (array)$userData[0];
            $data["mainContent"] = array();
            $data["footerData"] = array();
            $data["contentView"] = "profile";
            $data["jsRequire"] = array("models/profilemodel.js", "bootstrap.min.js");
            return view('frontend/content', $data)->render();
        } catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function getUserData()
    {
        try {
            if ($this->request->ajax()) {
                $reqData = $this->requestData;

                $user = DB::select(" SELECT u.*,ud.*,
                                  DATE_FORMAT(u.last_login,'%b %d, %Y %h:%i %p') as last_login ,
                                  DATE_FORMAT(u.created_at,'%b %d, %Y') as created_at 
                                  FROM users u 
                                  LEFT JOIN  user_detail ud 
                                  ON ud.login_id = u.id
                                  WHERE u.id = '" . $reqData . "' LIMIT 0,1");

                $response = array("isSuccess" => true, "message" => "Data saved successfully.", "data" => $user[0]);
                return json_encode($response);
            }
        } catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function getCountryList()
    {
        try {
            if ($this->request->ajax()) {
                $countryList = DB::select(" SELECT id, name FROM country_masters ORDER BY name ASC");
                $response = array("isSuccess" => true, "message" => "", "data" => array($countryList));
                return json_encode($response);
            } else {
                $response = array("isSuccess" => false);
                return json_encode($response);
            }
        } catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function getRegionList()
    {
        try {
            if ($this->request->ajax()) {
                $regionList = DB::select(" SELECT id, region,region_id  FROM lu_region ORDER BY region ASC");
                $response = array("isSuccess" => true, "message" => "", "data" => array($regionList));
                return json_encode($response);
            } else {
                $response = array("isSuccess" => false);
                return json_encode($response);
            }
        } catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function saveUserData() {
        try {
            if ($this->request->ajax()) {
                $requestData = $this->requestData;
                $updateUserDetail = array(
                    'first_name' => $requestData["first_name"],
                    'last_name' => $requestData["last_name"],
                    'mobile_number' => $requestData["mobile_number"],
                    'address_1' => $requestData["address_1"],
                    'address_2' => $requestData["address_2"],
                    'pincode' => $requestData["pincode"],
                    'city' => $requestData["city"],
                    'state' => $requestData["state"],
                    'country_id' => isset($requestData["country_id"]) ? $requestData["country_id"] : NULL,
                    'updated_at' => date("Y-m-d H:i:s")
                );
                DB::table('user_detail')
                    ->where('login_id', $requestData["login_id"])
                    ->update($updateUserDetail);

                $response = array("isSuccess" => true, "message" => "Data saved successfully", "Data" => "");
                return json_encode($response);
            }
        } catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function gameProfile() {
        try {
            $loginData = Session::get('escLoginData');
            $userData = DB::select(" SELECT * FROM users  WHERE id = '" . $loginData["userId"] . "' ");
            $data["headerData"] = (array)$userData[0];
            $data["mainContent"] = array();
            $data["footerData"] = array();
            $data["contentView"] = "gameProfile";
            $data["jsRequire"] = array("models/gameprofilemodel.js", "bootstrap.min.js");
            return view('frontend/content', $data)->render();
        }
        catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }

    public function getUserGamesData()
    {
        try {
            if ($this->request->ajax()) {
                $result  = array();
                $requestData = $this->requestData;

                $totalGames= DB::select(" SELECT COUNT(id) as total_games, GROUP_CONCAT(challenge_id) as game_ids  FROM challenge_players 
                                          WHERE user_id = $requestData 
                                        ");

                $result["totalGames"] = $totalGames[0]->total_games;
                $result["won_games"] = 0;
                $result["won_percentage"] = 0;

                if($totalGames[0]->total_games >0) {
                    $query = " SELECT COUNT(id) as won_games FROM game_results 
                                           WHERE challenge_id IN(".$totalGames[0]->game_ids.") AND winner_user_id = $requestData 
                                        ";
                    $totalWonGames = DB::select($query);
                    $result["won_games"] = $totalWonGames[0]->won_games;
                    $result["won_percentage"] = round($result["won_games"]*100/$result["totalGames"]);


                    $query = " SELECT *,IF(winner_user_id = $requestData,1,0) as win_result  FROM game_results
                                           WHERE challenge_id IN(".$totalGames[0]->game_ids.")  ORDER BY id DESC LIMIT  0,10
                                        ";

                    $last_ten_matches = DB::select($query);
                    $result["last_ten_matches"] = $last_ten_matches;
                }
                $result["lost_games"] = $result["totalGames"]-$result["won_games"];
                $result["lost_percentage"] = round($result["lost_games"]*100/$result["totalGames"]);
                $response = array("isSuccess"=>true,"message"=>"","data"=>array($result));
                return json_encode($response);
            }else{
                $response = array("isSuccess"=>false);
                return json_encode($response);
            }
        }
        catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }
}
