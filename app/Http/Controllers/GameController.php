<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\GameResult;
use App\Chat;
use App\GlobalSetting;
use App\CoinTransactionHistory;

class GameController extends Controller {

    protected $request;
    protected $gamesListArray;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        $requestData =$request->all();
        if(!empty($requestData))
            $this->requestData =$requestData["Data"];
        $this->gamesListArray = $this->gamesList();
        //$this->middleware('auth');
    }

    public function getGameData($gameID) {

        try{
            if ($gameID>0) {
                $loginData = Session::get('escLoginData');
                $gamesList = $this->gamesListArray;
                $headerData = array();
                $contentView = "rightSideBar";
                $jsRequire = array("models/signupmodel.js","models/homemodel.js");
                $include = array("frontend/signup_popup");
                $view = "frontend/home";

                if(isset($loginData)){
                    $userData = DB::select(" SELECT * FROM users  WHERE id = '" . $loginData["userId"] . "'");
                    $headerData = (array)$userData[0];
                    $contentView = "dashboard";
                    $jsRequire = array("models/dashboardmodel.js", "bootstrap.min.js");
                    $include = array();
                    $view = "frontend/content";
                }

                $mainContentData = array("gamesList" => $gamesList,"gameID"=>$gameID);
                $data["headerData"] = $headerData;
                $data["mainContent"] = $mainContentData;
                $data["footerData"] = array();
                $data["contentView"] = $contentView;
                $data["jsRequire"] = $jsRequire;
                $data["include"] = $include;
                return view($view, $data)->render();

            }

        }catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }

    }
    public function getChallengeList() {
        try{
            $otherData = array();
            $requestData =$this->requestData;
            $gameID = $requestData["gameID"];
            $pageSize = $requestData["PageSize"];
            $pageIndex = $requestData["PageIndex"];
            if($pageIndex  == 1){
                $offset = 0;
            }else {
                $offset = ($pageIndex - 1) * $pageSize;
            }
            $limit = " LIMIT ".$offset.",".$pageSize;

            $imagePath= URL::asset('assets/frontend/images/');
            $user_detail_query = " u.username, u.first_name,u.last_name,u.mobile_number,u.email,u.esc_id,u.address_1,u.address_2,u.pincode,u.city,u.state,u.country_id,
                                IF( u.image!='',u.image,'user-img.png') AS user_image,
                                CONCAT(u.first_name,' ',u.last_name) AS user_full_name ";
            if($gameID>0){
                $count_query = "SELECT COUNT(id) AS total FROM challenge WHERE isSolo = 'yes' AND game_master_id = '" . $gameID ."'" ;
                $total_counts = DB::select($count_query);

                $query  = " SELECT c.*,   DATE_FORMAT(c.created_at,'%d-%m-%Y %h:%i %p')  as challenge_created_date,                       
                                g.gameName AS game_name,IF(g.image IS NOT NULL,g.image,'user-img.png') AS game_image,
                                $user_detail_query ,
                                r.region AS regionName,r.host AS region_host,
                                m.name AS match_name,
                                '$imagePath' AS image_path,
                                (
                                  SELECT o.user_id FROM challenge_players o WHERE o.challenge_id = c.id AND o.user_id != c.userId LIMIT 0,1
                                ) as opponent_id
                                FROM challenge c 
                                LEFT JOIN game_master g ON g.id = c.game_master_id  
                                LEFT JOIN lu_region r ON r.id = c.region_id
                                LEFT JOIN match_master m ON m.id = c.match_id
                                LEFT JOIN user_detail u ON u.login_id = c.userId 
                                WHERE c.isSolo = 'yes' AND c.game_master_id = '" . $gameID . "'". $limit;

                $challenges = DB::select($query);
                if(!empty($challenges)){
                    foreach($challenges as &$challenge){

                        $challenge->opponent_id = !empty($challenge->opponent_id)?$challenge->opponent_id:1;
                        $challenge->user_image_full_path = $imagePath."/".$challenge->user_image;
                        $opponent_find = "SELECT u.id, " . $user_detail_query . " FROM user_detail u WHERE  u.login_id = " . $challenge->opponent_id . " LIMIT 0,1";

                        $opponentDetails = DB::select($opponent_find);
                        $opponentDetails[0]->user_image_full_path = $imagePath."/".$opponentDetails[0]->user_image;
                        $challenge->opponent = $opponentDetails[0];

                        $query  = " SELECT cp.*,$user_detail_query,
                                t.name
                                FROM challenge_players cp 
                                LEFT JOIN user_detail u ON u.login_id = cp.user_id  
                                LEFT JOIN team t ON  t.id  =  cp.team_id
                                WHERE cp.challenge_id = '" . $challenge->id . "'";
                        $players = DB::select($query);
                        $challenge->challenge_playes = $players;

                    }
                }
                $otherData["challenges"] = $challenges;

                $otherData["TotalItems"] = $total_counts[0]->total;
                $otherData["ItemsPerPage"] = $pageSize;
                $otherData["TotalPages"] = ceil($otherData["TotalItems"] / $pageSize);
                $otherData["CurrentPage"] = $pageIndex;
                $response = array("isSuccess" => true,"data"=>$otherData ,"message" => "success");
                return json_encode($response);
            }else{
                $response = array("isSuccess" => fail, "message" => "Enter game id.");
                return json_encode($response);
            }
        }catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }

    }
}
