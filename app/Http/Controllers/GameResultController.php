<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\GameResult;
use App\Chat;
use App\GlobalSetting;
use App\CoinTransactionHistory;

class GameResultController extends Controller {

    protected $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        //$this->middleware('auth');
    }

    public static function storeResult($challengeId) {
        if (!empty($challengeId)) {
            // <editor-fold defaultstate="collapsed" desc="Get Amount of Challenge">
            $getAmount = DB::table('challenge')
                    ->where('id', $challengeId)
                    ->first(array('amount', 'userId'));
            // </editor-fold>
            if (!empty($getAmount)) {
                // <editor-fold defaultstate="collapsed" desc="Get commision">
                $commision = GlobalSetting::first();
                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="Calculate winning amount">
                $finalWinningAmount = ($getAmount->amount * 2) - ($getAmount->amount * 2 * ($commision->commision / 100));
                // </editor-fold>
            }

            if (!empty($finalWinningAmount)) {
                // <editor-fold defaultstate="collapsed" desc="Get the current amount of winner user">
                $amount = DB::table('user_coins')
                    ->where('user_id', $getAmount->userId)
                    ->first();
                // </editor-fold>
                // <editor-fold defaultstate="collapsed" desc="Add winning amount to the winner user id">
                DB::table('user_coins')
                        ->where('user_id', $getAmount->userId)
                        ->update(array(
                    'amount' => ($amount->amount + $finalWinningAmount),
                )); 
                // </editor-fold>
            }

            // <editor-fold defaultstate="collapsed" desc="Coin transaction history">
            $coinTransactionHistory = new CoinTransactionHistory;
            $coinTransactionHistory->user_id = $getAmount->userId;
            $coinTransactionHistory->credit = $finalWinningAmount; //Total remaining amout 
            $coinTransactionHistory->debit = 0; //amount paid at the time of transaction
            $coinTransactionHistory->source_id = 1;
            $coinTransactionHistory->transaction_date = date('Y-m-d H:i:s');
            $coinTransactionHistory->save();
            // </editor-fold>
             
            // <editor-fold defaultstate="collapsed" desc="Update the challange status">
            $updateChallengeStatus = DB::table('challenge')
                    ->where('id', $challengeId)
                    ->update(array(
                'challengeStatus' => 3,
            ));
            // </editor-fold>

            $checkExistingResult = DB::table('game_results')
                    ->where('challenge_id', $challengeId)
                    ->count();
            if ($checkExistingResult == 0) {
                // <editor-fold defaultstate="collapsed" desc="Insert the challenge id and winner user id into Game Result">
                $gameResult = new GameResult;
                $gameResult->challenge_id = $challengeId;
                $gameResult->winner_user_id = $getAmount->userId;//This will be the diff user id
                if ($gameResult->save()) {
                    $challengePlayer = DB::table('challenge_players')
                            ->where('challenge_id', $challengeId)
                            ->where('user_id', '!=', Session::get('user')->id)
                            ->first();

                    if (!empty($challengePlayer) && !empty($challengePlayer->user_id)) {
                        $playerID = $challengePlayer->user_id;
                        $userID = Session::get('user')->id;
                        // <editor-fold defaultstate="collapsed" desc="Enter 2 seprate enries for chat result">
                        $chatResultFirstEntry = new Chat;

                        $chatResultFirstEntry->to_id = $userID;
                        $chatResultFirstEntry->from_id = $playerID;
                        $chatResultFirstEntry->message = 'Result';
                        $chatResultFirstEntry->isRead = 0;
                        $chatResultFirstEntry->isResult = 1;
                        $chatResultFirstEntry->challenge_id = $challengeId;
                        $chatResultFirstEntry->save();

                        $chatResultSecondEntry = new Chat;

                        $chatResultSecondEntry->to_id = $playerID;
                        $chatResultSecondEntry->from_id = $userID;
                        $chatResultSecondEntry->message = 'Result';
                        $chatResultSecondEntry->isRead = 0;
                        $chatResultSecondEntry->isResult = 1;
                        $chatResultSecondEntry->challenge_id = $challengeId;
                        $chatResultSecondEntry->save();
                        // </editor-fold>
                    }
                }
                // </editor-fold>
            }
        }
    }

}
