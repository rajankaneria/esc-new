<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;
use App\UserDetail;
use App\CoinTransactionHistory;
use App\GameMaster;
use App\Challenge;
use App\ChallengePlayer;
use App\Country;
use App\UserCoin;
use App\SteamUser;
use App\Region;
use App\Match;

class DashboardController extends Controller {

    protected $request;
    protected $loginData;
    protected $gamesListArray;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        $this->requestData = $request->all();
        if(!Session::get('escLoginData')){
            Redirect::to('/')->send();
        }
        $this->loginData = Session::get('escLoginData');
        $this->gamesListArray = $this->gamesList();

        //$this->middleware('auth');
    }

    /**
     * Dashboard action
     * 
     */
    public function index() {
        try {
            $loginData = Session::get('escLoginData');
            $userData = DB::select(" SELECT * FROM users  WHERE id = '" . $loginData["userId"] . "'");

            $data["headerData"] = (array)$userData[0];
            $data["mainContent"] = array("gamesList" => $this->gamesListArray);
            $data["footerData"] = array();
            $data["contentView"] = "dashboard";
            $data["jsRequire"] = array("models/dashboardmodel.js", "bootstrap.min.js");
            $data["include"] = array();
            return view('frontend/content', $data)->render();
        }catch (Exception $e){
            $response = array("isSuccess" => fail, "message" => $e->getMessage());
            return json_encode($response);
        }
    }



}
