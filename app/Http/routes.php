<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();

Route::get('/', 'HomeController@index');
Route::get('/index', 'HomeController@index');
Route::post('/saveuser', 'SignUpController@saveUser');
Route::get('/dashboard', 'DashboardController@index');
Route::get('/logout', 'SignUpController@logout');
Route::post('/login', 'SignUpController@login');
Route::get('/profile', 'ProfileController@index');
Route::post('/getprofiledata', 'ProfileController@getUserData');
Route::get('/getcountrylist', 'ProfileController@getCountryList');
Route::post('/saveuserdata', 'ProfileController@saveUserData');
Route::get('/getregionlist', 'ProfileController@getRegionList');
Route::post('/connectapi', 'APIController@connectApi');
Route::get('/gameprofile', 'ProfileController@gameProfile');
Route::post('/getusergamesdata', 'ProfileController@getUserGamesData');
Route::get('/getmatchlistapi', 'APIController@getMatchListAPI');
Route::get('/getmatchdetailapi', 'APIController@getMatchDetailAPI');



Route::get('/game/{id}', 'GameController@getGameData');
Route::post('/challengelist', 'GameController@getChallengeList');



//Route::get('/game/{id}','GameController@getGameData');

/*
Route::post('/game/saveuser', 'SignUpController@saveUser');
Route::get('/game/dashboard', 'DashboardController@index');
Route::post('/game/login', 'SignUpController@login');*/
/*
Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function ()    {
        // Uses Auth Middleware
    });

    Route::get('user/profile', function () {
        // Uses Auth Middleware
    });
});*/





Route::post('/create', array('uses'=>'HomeController@create'));
Route::post('/challengeAccept', array('uses'=>'HomeController@challengeAccept'));
Route::post('/doLogin', array('uses'=>'HomeController@doLogin'));


Route::get('/chat/{challengeID}', 'ChatController@chat');
Route::get('/chatData/{challengeID}', 'ChatController@getChats');
Route::post('/doMessage', 'ChatController@doMessage');
Route::get('/challenge', 'HomeController@challenge');
//Route::get('/logout','HomeController@doLogout');
Route::get('/accountSetting','HomeController@accountSetting');
Route::post('/updateProfile','HomeController@updateProfile');
Route::get('/steamLogin','HomeController@steamLogin');
Route::get('/storeResult/{challengeID}','GameResultController@storeResult');
Route::get('/reChallenge/{challengeID}','HomeController@reChallenge');
Route::get('/createChallenge', 'HomeController@createChallenge');
Route::get('/report/{challengeID}', 'ReportController@report');



// admin routes
Route::get('/admin', 'Admin@dashboard');

Route::get('/admin/login', 'Admin@loginCheck');

Route::get('/admin/gameCreate', 'Admin@gameCreate');
Route::post('/admin/gameCreate', 'Admin@gameCreate');
Route::get('/admin/users', 'Admin@users');
Route::get('/admin/user/{userId}', 'Admin@userEdit');
Route::post('/admin/user/{userId}', 'Admin@userEdit');
Route::get('/admin/account/{userId}', 'Admin@accountManage');
Route::post('/admin/account/{userId}', 'Admin@accountManage');
Route::get('/admin/settings', 'Admin@settings');
Route::post('/admin/settings', 'Admin@settings');

Route::get('/admin/logout/', 'Admin@logout');

// steam routes
Route::get('/steam/match/{matchID}', 'Steam@matchDetails');

Route::get('/steam/history/{steamID}/{matches}', 'Steam@dota2history');


Route::get('/lol/recentMatches/{summonerID}', 'GameApiController@recentMatches');




