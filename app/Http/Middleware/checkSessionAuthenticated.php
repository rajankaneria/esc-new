<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class checkSessionAuthenticated {

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
            //
    ];

    public function handle($request, Closure $next) {
        if (Session::has('user')) {
            $id = Session::get('user')->id;
            $userQuery = DB::select("select * from users where id='$id' and role=0");
            if (sizeof($userQuery) != 0) {
                $adminObject = $userQuery[0];
                $adminID = $adminObject->id;
                Session::set('adminID', $adminID);
            }
        }
        return $next($request);
    }

}
