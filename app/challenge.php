<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Challenge extends Model {

    protected $table = 'challenge';

    // public $timestamps = false;
    public function User() {
        return $this->belongsTo('App\UserDetail','userId','login_id');
    }
    
    public function Game() {
        return $this->belongsTo('App\GameMaster','game_master_id','id');
    }

}
?>

