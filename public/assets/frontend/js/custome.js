 $(document).ready(function(){
  	"use strict";
/*Review*/
	$('#carousel-reviews').carousel();
/*Menu*/
	var $menu = $('#default-mobiMenu'),$menulink = $('#spinner-Mform');
	$(document).on('click', '#spinner-Mform', function() {
		$menulink.toggleClass('open');
		$menu.toggleClass('active');
	});
	$(function () {
	var serdate = '08/01/2016';
	var nextdate = '08/02/2016';
        $('#datetimepicker6').datetimepicker({
			format: 'DD/MM/YYYY',
			defaultDate: serdate,
			icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                }
	});
    $('#datetimepicker7').datetimepicker({
			format: 'DD/MM/YYYY',
			defaultDate: nextdate,
			 icons: {
                    time: "fa fa-clock-o",
                    date: "fa fa-calendar",
                    up: "fa fa-arrow-up",
                    down: "fa fa-arrow-down"
                },
			useCurrent: false //Important! See issue #1075
			
    });
    $("#datetimepicker6").on("dp.change", function (e) {
            $('#datetimepicker7').data("DateTimePicker").minDate(e.date);
    });
    $("#datetimepicker7").on("dp.change", function (e) {
            $('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
    });
    });
/*Scroll sidebar
	var $scrollingDiv = $(".sidebar");
 
	$(window).scroll(function(){			
		$scrollingDiv
		.stop()
		.animate({"marginTop": ($(window).scrollTop() + 15) + "px"}, "slow" );			
	});*/
	
		
	
});