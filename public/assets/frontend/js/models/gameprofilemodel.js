var userDataURL = 'getprofiledata';
var userGamesDataURL = 'getusergamesdata';

var GameProfileModel = function() {
    var self = this;

    self.UserObj = function() {
        var um = this;
        um.first_name = ko.observable();
        um.last_name = ko.observable();
        um.email = ko.observable();
        um.mobile_number = ko.observable();
        um.last_login = ko.observable();
    };
    self.UserModel = ko.observable(new self.UserObj());

    self.getUserData = function() {
        AjaxCall(userDataURL, ko.toJSON({
            Data: loginUserData
        }), "post", "json", "application/json").done(function(response) {
            if (response.isSuccess) {
                ko.mapping.fromJS(response.data,{}, self.UserModel);
            } else {
                alert(response.message);
                /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
            }
        });
    };

    self.GamesDataObj = function() {
        var gd = this;
        gd.totalGames = ko.observable(0);
        gd.won_games = ko.observable(0);
        gd.lost_games = ko.observable(0);
        gd.won_percentage = ko.observable(0);
        gd.lost_percentage = ko.observable(0);
        gd.last_ten_matches = ko.observableArray();
    };
    self.GamesDataModel = ko.observable(new self.GamesDataObj());

    self.getUserGamesData = function() {
        AjaxCall(userGamesDataURL, ko.toJSON({
            Data: loginUserData
        }), "post", "json", "application/json").done(function(response) {
            if (response.isSuccess) {
                ko.mapping.fromJS(response.data[0],{}, self.GamesDataModel);
                $("#win_percentage").addClass('p'+self.GamesDataModel().won_percentage());
                $("#lost_percentage").addClass('p'+self.GamesDataModel().lost_percentage());
                $("#win_progress_bar").css({"width":self.GamesDataModel().won_percentage()+"%"});
            } else {
                alert(response.message);
                /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
            }
        });
    };


    self.getUserData();
    self.getUserGamesData();

};
window.Required_first_name = "First name is required."
window.Required_last_name = "Last name is required."
window.Required_mobile_number = "Mobile number is required."
window.Required_Email = "Email is required."
window.Required_address_1= "Address is required."
window.Required_pincode = "Pincode is required."
window.Required_city = "City is required."
window.Required_state = "State is required."
window.Required_request_name = "Request name  is required."
window.Required_region_id = "Region is required."
$(document).ready(function() {
    var vm = new GameProfileModel()
    ko.applyBindings(vm);
});