var homeView;
var challengeListURL = 'challengelist';
var HomeModel = function() {
    var self = this;
    self.pager = new Pager();
    self.pager.isSearch(false);
    //self.pageSize = ko.observableArray(docPageSizeArray);
    self.test = ko.observable();
    self.test.subscribe(function (val) {debugger;

    });
        self.game_id = ko.observable(gameID);

    self.challengesData = function(){
        var cd = this;
        cd.challengeListArray = ko.observableArray();
        cd.pagination = ko.observable();
    }

    self.challengesModel = ko.observable(new self.challengesData());

    self.PageLoad = function() {
        var param = {
            PageIndex: self.pager.currentPage(),
            PageSize : self.pager.iPageSize(),
            gameID: self.game_id()
        };
        AjaxCall(challengeListURL,ko.toJSON({
            Data:param
        }), "post", "json", "application/json").done(function(response) {
            if (response.isSuccess) {
                ko.mapping.fromJS(response.data.challenges,{}, self.challengesModel().challengeListArray);

                self.pager.currentPage(response.data.CurrentPage);
                self.pager.iPageSize(response.data.ItemsPerPage);
                self.pager.iTotalDisplayRecords(response.data.challenges.length);
                self.pager.iTotalRecords(response.data.TotalItems);
            } else {
                alert(response.message);
                /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
            }
        });
    };
    self.pager.getDataCallback = self.PageLoad;

    self.pager.selectedPageSize(docDefaultPageSize);
    self.getImage = function(imagePath,imageName){
        return imagePath+'/'+imageName;
    }
};
homeView = new HomeModel();
/*
$(document).ready(function() {
    homeView = new HomeModel();
    ko.applyBindings(homeView, document.getElementById('RightPannel'));
});*/
