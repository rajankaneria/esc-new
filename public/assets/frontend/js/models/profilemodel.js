var userDataURL = 'getprofiledata';
var getCountryListURL = 'getcountrylist';
var saveUserDataURL = 'saveuserdata';
var getRegionList = 'getregionlist';
var connectAPIURL = 'connectapi';
var ProfileModel = function() {
    var self = this;
    self.countryArray = ko.observableArray();
    self.regionListArray = ko.observableArray();
    self.displaySuccessMessage = ko.observable(0);
    self.displayConnectSuccessMessage = ko.observable(0);
    self.successMessage = ko.observable();
    self.errorClass=ko.observable(0);
    self.UserObj = function() {
        var um = this;
        um.login_id = ko.observable();
        um.first_name = ko.observable().extend({
            required: {
                message: window.Required_first_name
            }
        });
        um.last_name = ko.observable().extend({
            required: {
                message: window.Required_last_name
            }
        });
        um.email = ko.observable().extend({
            required: {
                message: window.Required_Email
            },
            email: true
        });
        um.mobile_number = ko.observable().extend({
            required: {
                message: window.Required_mobile_number
            },
            number:true
        });
        um.last_login = ko.observable();
        um.created_at = ko.observable();
        um.address_1 = ko.observable().extend({
            required: {
                message: window.Required_address_1
            }
        });
        um.address_2 = ko.observable();
        um.pincode = ko.observable().extend({
            required: {
                message: window.Required_pincode
            },
            number:true
        });
        um.city = ko.observable().extend({
            required: {
                message: window.Required_city
            }
        });
        um.state = ko.observable().extend({
            required: {
                message: window.Required_state
            }
        });
        um.country_id = ko.observable();

    };
    self.UserModel = ko.observable(new self.UserObj());
    self.requestDataObj = function(){
        var rm = this;
        rm.requestName = ko.observable();
        rm.requestName = ko.observable().extend({
            required: {
                message: window.Required_request_name
            }
        });
        rm.region_id = ko.observable().extend({
            required: {
                message: window.Required_region_id
            }
        });
    }

    self.requestModel = ko.observable(new self.requestDataObj());

    self.getUserData = function() {
        AjaxCall(userDataURL, ko.toJSON({
            Data: loginUserData
        }), "post", "json", "application/json").done(function(response) {
            if (response.isSuccess) {
                ko.mapping.fromJS(response.data,{}, self.UserModel);
            } else {
                alert(response.message);
                /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
            }
        });
    };
    self.saveUserData = function(data) {
        var error = ko.validation.group(data, {
            deep: true
        });
        if (error().length == 0) {
            delete data.errors;
            AjaxCall(saveUserDataURL, ko.toJSON({
                Data: data
            }), "post", "json", "application/json").done(function(response) {
                if (response.isSuccess) {
                    self.getUserData();
                    self.displaySuccessMessage(1);
                    self.successMessage(response.message);
                    setTimeout(function(){
                        self.displaySuccessMessage(0);
                        self.successMessage("");
                    }, 3000);
                } else {
                    alert(response.message);
                    /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
                }
            });
        } else {
            error.showAllMessages(true);
        }
    };
    self.getCountryList = function() {
        AjaxCall(getCountryListURL, {}, "get", "json", "application/json").done(function(response) {
            if (response.isSuccess) {
                ko.mapping.fromJS(response.data[0],{}, self.countryArray);
            }
        });
    };

    self.getRegionList = function() {
        AjaxCall(getRegionList, {}, "get", "json", "application/json").done(function(response) {
            if (response.isSuccess) {
                ko.mapping.fromJS(response.data[0],{}, self.regionListArray);
            }
        });
    };

    self.getUserData();
    self.getCountryList();
    self.getRegionList();


    self.connectApi = function(data) {
        var error = ko.validation.group(data, {
            deep: true
        });
        if (error().length == 0) {
            delete data.errors;
            data.userId = self.UserModel().login_id;
            AjaxCall(connectAPIURL, ko.toJSON({
                Data: data
            }), "post", "json", "application/json").done(function(response) {
                if (response.isSuccess) {
                    self.getUserData();
                    self.displayConnectSuccessMessage(1);
                    self.successMessage(response.message);
                    setTimeout(function(){
                        self.displayConnectSuccessMessage(0);
                        self.successMessage("");
                        self.errorClass(0);
                    }, 3000);
                } else {
                    self.displayConnectSuccessMessage(1);
                    self.errorClass(1);
                    self.successMessage(response.message);
                    /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
                }
            });
        } else {
            error.showAllMessages(true);
        }
    };
   // window.location.assign("dashboard");

};
window.Required_first_name = "First name is required."
window.Required_last_name = "Last name is required."
window.Required_mobile_number = "Mobile number is required."
window.Required_Email = "Email is required."
window.Required_address_1= "Address is required."
window.Required_pincode = "Pincode is required."
window.Required_city = "City is required."
window.Required_state = "State is required."
window.Required_request_name = "Request name  is required."
window.Required_region_id = "Region is required."
$(document).ready(function() {
    /*var SessionExpired = $('#SessionExpired').val();
    if (SessionExpired != '') {
        ShowAlertMessage(SessionExpired, 'error', window.ConfirmDialogSomethingWrong);
        $('#SessionExpired').val('');
    }*/
    ko.applyBindings(new ProfileModel());
    /*$("#main").show();*/
});