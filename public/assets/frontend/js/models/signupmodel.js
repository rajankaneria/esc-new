var signUpView;
var saveUserURL = 'saveuser';
var signInUserURL = 'login';
var challengeListURL = 'challengelist';
var SignUpModel = function() {
    var self = this;
    self.UserObj = function() {
        var um = this;
        um.email = ko.observable().extend({
            required: {
                message: window.Required_Email
            },
            email: true
        });
        um.password = ko.observable("").extend({
            required: {
                message: window.Required_Password
            }
        });
        um.cPassword = ko.observable("").extend({
            required: {
                message: window.Required_CPassword
            },
            /*validation: {
                validator: function (val, someOtherVal) {
                    return val == um.password;
                },
                message: "Password doesn't match"

            }*/
        });

    };
    self.UserModel = ko.observable(new self.UserObj());
    self.saveUserData = function(data) {
        //data.IsAcceptTerm(data.IsAcceptTerm() === true ? true : '');
        var error = ko.validation.group(data, {
            deep: true
        });
        if (error().length == 0) {
            delete data.errors;

            AjaxCall(saveUserURL, ko.toJSON({
                Data: data
            }), "post", "json", "application/json").done(function(response) {
                if (response.isSuccess) {
                    $("#myModal").modal("toggle");
                    window.location.assign("dashboard");
                } else {
                    alert(response.message);
                    /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
                }
            });
        } else {
            error.showAllMessages(true);
        }
    };


    self.loginUserObj = function() {
        var um = this;
        um.email = ko.observable("").extend({
            required: {
                message: window.Required_Email
            },
            email: true
        });
        um.password = ko.observable("").extend({
            required: {
                message: window.Required_Password
            }
        });
    };
    self.loginUserModel = ko.observable(new self.loginUserObj());
    self.signInUser = function(data) {

        var error = ko.validation.group(data, {
            deep: true
        });
        if (error().length == 0) {
            delete data.errors;
            AjaxCall(signInUserURL, ko.toJSON({
                Data: data
            }), "post", "json", "application/json").done(function(response) {
                if (response.isSuccess) {
                    window.location.assign(window.location.href);
                } else {
                    alert(response.message);
                    /*ShowAlertMessage(response.Message, 'error', window.ConfirmDialogSomethingWrong);*/
                }
            });
        } else {
            error.showAllMessages(true);
        }
    };
};
window.Required_Email = "Email is required."
window.Required_Password = "Password is required."
window.Required_CPassword = "Confirm password is required."
signUpView = new SignUpModel();
/*
$(document).ready(function() {
    *var SessionExpired = $('#SessionExpired').val();
    if (SessionExpired != '') {
        ShowAlertMessage(SessionExpired, 'error', window.ConfirmDialogSomethingWrong);
        $('#SessionExpired').val('');
    }
    signUpView = new SignUpModel();
    ko.applyBindings(signUpView);
    !*$("#main").show();
});*/
