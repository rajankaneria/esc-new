/*!
 * jQuery Cookie Plugin v1.3.1
 * https://github.com/carhartl/jquery-cookie
 * Plugin file name changed to jquery.cokie.min to prevent blocking by ModSecurity module
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function(a) {
    if (typeof define === "function" && define.amd) {
        define(["jquery"], a)
    } else {
        a(jQuery)
    }
}(function(e) {
    var a = /\+/g;

    function d(g) {
        return g
    }

    function b(g) {
        return decodeURIComponent(g.replace(a, " "))
    }

    function f(g) {
        if (g.indexOf('"') === 0) {
            g = g.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, "\\")
        }
        try {
            return c.json ? JSON.parse(g) : g
        } catch (h) {}
    }
    var c = e.cookie = function(p, o, u) {
        if (o !== undefined) {
            u = e.extend({}, c.defaults, u);
            if (typeof u.expires === "number") {
                var q = u.expires,
                    s = u.expires = new Date();
                s.setDate(s.getDate() + q)
            }
            o = c.json ? JSON.stringify(o) : String(o);
            return (document.cookie = [c.raw ? p : encodeURIComponent(p), "=", c.raw ? o : encodeURIComponent(o), u.expires ? "; expires=" + u.expires.toUTCString() : "", u.path ? "; path=" + u.path : "", u.domain ? "; domain=" + u.domain : "", u.secure ? "; secure" : ""].join(""))
        }
        var g = c.raw ? d : b;
        var r = document.cookie.split("; ");
        var v = p ? undefined : {};
        for (var n = 0, k = r.length; n < k; n++) {
            var m = r[n].split("=");
            var h = g(m.shift());
            var j = g(m.join("="));
            if (p && p === h) {
                v = f(j);
                break
            }
            if (!p) {
                v[h] = f(j)
            }
        }
        return v
    };
    c.defaults = {};
    e.removeCookie = function(h, g) {
        if (e.cookie(h) !== undefined) {
            e.cookie(h, "", e.extend({}, g, {
                expires: -1
            }));
            return true
        }
        return false
    }
}));

var dateFormatToPassToServerSide = "YYYY-MM-DD";
var DateTimeFormat = "MM/DD/YYYY";
var DefaultCookieName = "DefaultMessage";
var exts = ["jpg", "jpeg", "png", "gif"];
var allowed_length = 400000;
var error_message_header = 'Oops! Something went wrong';
var uploadphotoexts = ["jpg", "jpeg", "png", "gif", "JPG", "JPEG", "GIF", "PNG"];
var uploadphotoallowed_length = 400000000;
var uploadpdfexts = ["pdf"];
var AllText = 'All';
var docDefaultPageSize = 2;
var PageSizeFor10Records = 10;
var docDefaultPageSchoolSize = 5;
var docDefaultPageSizeVideoDeshboard = 8;
var docDefaultPageNewsSize = 10;
var serverError = "Oops! Something went wrong on server, suspected error would be ";
var hideLoading = true;
var years18Days = 6574;
$.xhrPool = [];
$.xhrPool.abortAll = function() {
    $(this).each(function(idx, xhr) {
        xhr.abort();
    });
    $.xhrPool = [];
};

function AjaxCall(url, postData, httpmethod, calldatatype, contentType, showLoading, hideLoadingParam, isAsync, processData, xhr) {
    url = baseUrl+"/"+url;
    if (hideLoadingParam != undefined && !hideLoadingParam)
        hideLoading = hideLoadingParam;
    if (contentType == undefined)
        contentType = "application/x-www-form-urlencoded;charset=UTF-8";
    if (showLoading == undefined)
        showLoading = true;
    if (showLoading == false || showLoading.toString().toLowerCase() == "false")
        showLoading = false;
    else
        showLoading = true;
    if (processData == undefined)
        processData = true;
    if (isAsync == undefined)
        isAsync = true;
    if (xhr == undefined)
        xhr = false;
    return jQuery.ajax({
        type: httpmethod,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url:url,
        data: postData,
        global: showLoading,
        dataType: calldatatype,
        contentType: contentType,
        processData: processData,
        async: isAsync,
        beforeSend: function() {
            if (xhr) {
                xhr.abort();
            }
            if (showLoading) {
                $('body').addClass("loading");
            }
        },
        error: function(xhr, textStatus, errorThrown) {
            if (!userAborted(xhr)) {
                if (xhr.status == 403) {
                    var isJson = false;
                    try {
                        var response = $.parseJSON(xhr.responseText);
                        isJson = true;
                    } catch (e) {}
                    if (isJson && response != null && response.Type == "NotAuthorized" && response.Link != undefined)
                        window.location = baseUrl + response.Link;
                    else
                        window.location = window.baseUrl;
                } else {
                    var alertText = "";
                    switch (xhr.status) {
                        case 404:
                            alertText = serverError + "'Method " + xhr.statusText + "'";
                            break;
                        case 200:
                            alertText = "";
                            break;
                        default:
                            alertText = serverError + "'" + xhr.statusText + "'";
                            break;
                    }
                    alert(alertText);
                    OnError(alertText, "", "", "");
                }
            }
        }
    });
    return xhr;
}

function OnError(message, file, line, error) {
    var apiUrl = baseUrl + '/javascripterror';
    if (line == undefined || line == "") {
        line = "";
    }
    if (file == undefined || file == "") {
        file = "";
    }
    if (error == undefined || error == "") {
        error = "";
    } else {
        error = error.stack;
    }
    var suppressErrors = true;
    $.ajax({
        url: apiUrl,
        type: 'POST',
        data: {
            errorMsg: message,
            errorLine: line,
            queryString: file,
            url: document.location.pathname,
            referrer: document.referrer,
            stack: error,
            userAgent: navigator.userAgent
        }
    });
    return suppressErrors;
}
window.onerror = function(message, file, line, error) {
    alert(message);
    OnError(message, file, line, error);
};
ko.bindingHandlers.ApplyFileUpload = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var onErrorCallback = allBindings().onErrorCallback;
        var onSuccessCallback = allBindings().onSuccessCallback;
        var param = allBindings().paraMeter;
        var url = valueAccessor()();
        var acceptedFiles = allBindings().acceptedFiles;
        if (param != undefined)
            url = url + '/' + param;
        var loader = $(".loader");
        var maxUploadSize = parseInt(400);
        $(element).fileupload({
            url: baseUrl + url,
            maxFileSize: 400,
            type: 'POST',
            dataType: 'json',
            fail: onErrorCallback,
            done: onSuccessCallback,
            progress: function(e, data) {
                $(loader).html("");
            },
            beforeSend: function() {
                $('body').addClass("loading");
            },
            send: function(filedata, data, formData) {
                var uploadErrors = [];
                var isValidImage = true;
                $.each(data.files, function(index, file) {
                    var extension = file.name.substring(file.name.lastIndexOf('.') + 1).toLowerCase();
                    if (acceptedFiles == 'image') {
                        if (extension !== 'jpg' && extension !== 'jpeg' && extension !== 'png' && extension !== 'gif' || file.size > maxUploadSize * 1024) {
                            uploadErrors.push(window.ImageFileAllowedMessage);
                            isValidImage = false;
                        }
                    }
                });
                if (uploadErrors.length > 0) {
                    ShowAlertMessage(uploadErrors.join("\n"), 'error', 'Error Message');
                    return false;
                }
                $(loader).html("<img  src='/assets/images/ajax-loader.gif'>");
            }
        });
    }
};

function BlockUI() {
    $('body').addClass("loading");
}

function UnBlockUI() {
    $('body').removeClass("loading");
}
$(document).ajaxStop(function(jqXHR, settings) {
    if (hideLoading) {
        UnBlockUI();
        $("#main").show();
    }
});

function userAborted(xhr) {
    return !xhr.getAllResponseHeaders();
}
ko.bindingHandlers.FileUpload = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var url = valueAccessor();
        var Send = allBindingsAccessor().Send;
        var Done = allBindingsAccessor().Done;
        $(element).fileupload({
            dataType: 'json',
            url: url,
            send: Send,
            done: Done
        });
    }
};

function SetMessageForPageLoad(data, cookieName) {
    if (cookieName == undefined) {
        cookieName = DefaultCookieName;
    }
    $.cookie(cookieName, JSON.stringify(data), {
        path: '/'
    });
}
var fileDownloadCheckTimer;

function finishDownload(tokenValueID, cookieName) {
    window.clearInterval(fileDownloadCheckTimer);
    UnBlockUI();
    $('#' + tokenValueID).val('');
    $.removeCookie(cookieName);
    $.cookie(cookieName, null, {
        path: '/'
    });
}

function blockUIForDownload(tokenValueID, cookieName) {
    BlockUI();
    var token = new Date().getTime();
    $('#' + tokenValueID).val(token);
    fileDownloadCheckTimer = window.setInterval(function() {
        var cookieValue = $.cookie(cookieName, cookieValue, {
            expires: 1,
            path: '/',
            domain: 'localhost'
        });
        if (cookieValue == token) {
            finishDownload(tokenValueID, cookieName);
        }
    }, 1000);
}

function ShowPageLoadMessage(cookieName) {
    /*if (cookieName == undefined) {
        cookieName = DefaultCookieName;
    }
    if ($.cookie(cookieName) != null && $.cookie(cookieName) != "null") {
        ShowSuccessMessage($.cookie(cookieName), 'success', 'Success');
        $.cookie(cookieName, null, {
            path: '/'
        });
    }*/
}
$(document).ready(function() {
    $('input:first').focus();
    ShowPageLoadMessage();
});
var init = false;
var initDecorate = true;
ko.bindingHandlers.decorateErrorElement = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        initDecorate = true;
    },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        if (valueAccessor != undefined && valueAccessor() != undefined && valueAccessor().isValid) {
            var valueIsValid = valueAccessor().isValid();
            var valueIsmodified = valueAccessor().isModified();
            if (allBindingsAccessor().multipleSelect != undefined) {
                if (initDecorate) {
                    initDecorate = false;
                    return;
                }
                element = $(element).next('.ms-parent').children(".ms-choice");
            }
            var showToolTip = function(element) {
                element.attr("data-original-title", valueAccessor().error()).addClass("tooltip-danger").addClass('ko-validation').siblings('span.validationMessage').show();
                element.closest('.input-col').addClass('has-error');
                element.closest('.form-group').addClass('error');
                element.closest('.form-sub-group').addClass('error');
                element.closest('.input-group').addClass('error');
            };
            var hideToolTip = function(element) {
                element.removeClass("tooltip-danger").removeClass('ko-validation').siblings('span.validationMessage').hide();
                element.closest('.input-col').removeClass('has-error');
                element.closest('.form-group').removeClass('error');
                element.closest('.form-sub-group').removeClass('error');
                element.closest('.input-group').removeClass('error');
            };
            $('.tooltip-danger').on('focus', function() {
                $(this).closest('.input-group').removeClass('error');
                $(this).removeClass('tooltip-danger').removeClass('ko-validation');
            });
            if (valueIsmodified) {
                if (!valueIsValid) {
                    showToolTip($(element));
                } else {
                    hideToolTip($(element));
                }
            }
        }
    }
};

function CheckPageError(form) {
    var isError = false;
    $('#' + form + ' input,' + '#' + form + ' select, ' + '#' + form + ' textarea, ' + '#' + form + ' button').each(function(index) {
        var element = $(this);
        if (element.hasClass('tooltip-danger ko-validation')) {
            isError = true;
            return false;
        }
    });
    return isError;
}
ko.validation.configure({
    registerExtenders: true,
    messagesOnModified: true,
    insertMessages: true,
    parseInputAttributes: true,
    messageTemplate: null,
    grouping: {
        deep: true,
        observable: true,
        live: false
    }
});

function UpdateDateJS(data, format) {
    if (format == undefined) {
        format = dateFormatToPassToServerSide;
    }
    return moment(data).format(format);
}

function ShowSuccessMessage(message, type, header) {
    $.msgGrowl({
        type: type ? type : 'success',
        text: message,
        position: 'top-center',
        lifetime: 5000
    });
}

function ShowConfirm(message, callback, header, successButtonText, cancelButtonText) {
    var success = "Yes";
    var cancel = "No";
    if (successButtonText != null)
        success = successButtonText;
    if (cancelButtonText != null)
        cancel = cancelButtonText;
    new BootstrapDialog({
        title: window.confirmdialogtitle,
        message: window.confirmdialogmessage + message,
        closable: true,
        data: {
            'callback': callback
        },
        buttons: [{
            label: success,
            cssClass: 'btn btn-danger',
            action: function(dialogItself) {
                dialogItself.close();
                typeof dialogItself.getData('callback') === 'function' && dialogItself.getData('callback')(false);
            }
        }, {
            label: cancel,
            cssClass: 'btn btn-grey',
            action: function(dialogItself) {
                dialogItself.close();
            }
        }]
    }).open();
}

function ShowAlertMessage(message, type, header) {
    var classname;
    if (header)
        header = header;
    else
        header = '';
    switch (type) {
        case 'alert':
            classname = 'warning';
            break;
        case 'info':
            classname = 'info';
            break;
        case 'error':
            classname = 'error';
            break;
        default:
            classname = 'warning';
            type = 'alert';
            break;
    };
    BootstrapDialog.show({
        title: header,
        message: message,
        buttons: [{
            label: 'Close',
            cssClass: 'btn btn-danger',
            action: function(dialogItself) {
                dialogItself.close();
            }
        }]
    });
}

function generateUUID() {
    $.cookie("enc_userid", enc_id);
    var d = new Date();
    var milliseconds = d.getMilliseconds();
    var userid = $.cookie('enc_userid');
    var str = milliseconds + userid;
    var hex_chr = "0123456789abcdef";
    hash = '';
    for (j = 0; j < str.length; j++)
        hash += hex_chr.charAt((str >> (j * 8 + 4)) & 0x0F) +
        hex_chr.charAt((str >> (j * 8)) & 0x0F);
    var seconds = d.getSeconds();
    if (seconds < 10) {
        seconds = '0' + seconds;
    }
    var minutes = d.getMinutes();
    if (minutes < 10) {
        minutes = '0' + minutes;
    }
    var hours = d.getHours();
    if (hours < 10) {
        hours = '0' + hours;
    }
    var day = d.getDate();
    if (day < 10) {
        day = '0' + day;
    }
    var month = d.getMonth() + 1;
    if (month < 10) {
        month = '0' + month;
    }
    var year = d.getFullYear();
    var uuid = "" + hash + "" + "-" + "" + seconds + "" + minutes + "" + hours + "" + "-" + "" + month + "" + day + "" + year + "";
    return uuid;
}

function generateUUID3(val) {
    enc_id = val;
    return generateUUID();
}

function generateUUID2(val) {
    enc_id = val.enc_userid;
    return generateUUID();
}
ko.bindingHandlers.CkEditor = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var value = valueAccessor();
        var ckobj;
        if ($(element).attr('id') == undefined) {
            $(element).attr('id', +new Date);
        }
        if ($(element).is("div")) {
            if ($(element).attr('contenteditable') == undefined) {
                $(element).attr("contenteditable", "true");
            }
            ckobj = CKEDITOR.inline($(element).attr('id'));
        } else {
            ckobj = CKEDITOR.replace($(element).attr('id'));
        }
        ckobj.setData(value());
        ckobj.on('change', function() {
            value(ckobj.getData());
        });
        element.ckobj = ckobj;
        element.isupdated = false;
    },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var value = valueAccessor();
        if (value() != element.ckobj.getData())
            element.ckobj.setData(value());
        if (element.isupdated) {
            $(".ckeditorvalidation").each(function(i, element) {
                if (!$(element).valid()) {
                    $(element).parent('div').addClass("tooltip-danger");
                } else {
                    $(element).parent('div').removeClass("tooltip-danger");
                }
            });
        }
        element.isupdated = true;
    }
};
ko.bindingHandlers.datepicker = {
    init: function(element, valueAccessor, allBindingsAccessor) {
        var startDate = allBindingsAccessor().StartDate || "";
        var endDate = allBindingsAccessor().EndDate || "";
        var options = allBindingsAccessor().datepickerOptions || {
            autoclose: true,
            endDate: endDate,
            startDate: startDate
        };
        $(element).datepicker(options);
        $(element).on("change", function() {
            if ($(this).val().length == 0) {
                var observable = valueAccessor();
                observable("");
            }
        });
        $(element).on("click", function() {
            $(element).datepicker("show");
        });
        $(element).on('keypress', function(e) {
            var leng = $(this).val().length;
            var code;
            if (window.event) {
                code = e.keyCode;
            } else {
                code = e.which;
            }
            var allowedCharacters = [49, 50, 51, 52, 53, 54, 55, 56, 57, 48, 47];
            var isValidInput = false;
            for (var i = allowedCharacters.length - 1; i >= 0; i--) {
                if (allowedCharacters[i] == code) {
                    isValidInput = true;
                }
            }
            if (isValidInput === false || (code == 47 && (leng < 2 || leng > 5 || leng == 3 || leng == 4)) || ((leng == 2 || leng == 5) && code !== 47) || leng == 10) {
                if (e.preventDefault()) {
                    e.preventDefault();
                    return;
                }
            }
        });
        if (valueAccessor()() == "0001-01-01T00:00:00") {}
        ko.utils.registerEventHandler(element, "changeDate", function() {
            var observable = valueAccessor();
            if ($(element).datepicker("getDate") != "Invalid Date") {
                observable($(element).datepicker("getDate"));
            } else {
                observable(null);
            }
        });
        ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
            $(element).datepicker("destroy");
        });
    },
    update: function(element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor()),
            current = $(element).datepicker("getDate");
        if (isNaN(value - current)) {
            if (value != null) {
                var date = moment(value);
                if (date.toString() == "Invalid date") {
                    $(element).val(null);
                } else {
                    $(element).datepicker("setDate", date.format(DateTimeFormat));
                }
            }
        } else {
            if (value - current !== 0) {
                $(element).datepicker("setDate", value);
            }
        }
    }
};
ko.bindingHandlers.dateTimepicker = {
    init: function(element, valueAccessor) {
        if (valueAccessor()() == "0001-01-01T00:00:00") {
            valueAccessor()('');
            $(element).val('');
        }
        var options = {
            format: 'mm/dd/yyyy HH:ii p',
            showMeridian: true,
            pickerPosition: "top-right"
        };
        $(element).datetimepicker(options);
        $(element).on("change", function() {
            if ($(this).val().length == 0) {
                var observable = valueAccessor();
                observable("");
            }
        });
        $(element).on("click", function() {
            $(element).datetimepicker('show');
        });
        ko.utils.registerEventHandler(element, "changeDate", function(event) {
            var value = valueAccessor();
            if (ko.isObservable(value)) {
                value(moment(event.date).utc().format('YYYY/MM/DD H:mm'));
            }
            $(element).datetimepicker('hide');
        });
    },
    update: function(element, valueAccessor) {
        var widget = $(element).data("datetimepicker");
        if (widget) {
            if (ko.utils.unwrapObservable(valueAccessor())) {
                if (moment(ko.utils.unwrapObservable(valueAccessor())).format(dateTimeFormatToPassServerSide) != undefined && moment(ko.utils.unwrapObservable(valueAccessor()), 'YYYY-MM-DD HH:mm').format('YYYY/MM/DD H:mm A') != "Invalid date") {
                    widget.setDate(new Date(moment(ko.utils.unwrapObservable(valueAccessor()), 'YYYY-MM-DD hh:mm:ss a').format('YYYY/MM/DD H:mm')));
                } else {
                    widget.date = new Date(moment().format('YYYY/MM/DD  H:mm'));
                }
            }
        }
    }
};

function resize(file, max_width, max_height, imageEncoding) {
    var fileLoader = new FileReader(),
        canvas = document.createElement('canvas'),
        context = null,
        imageObj = new Image(),
        blob = null;
    canvas.id = "hiddenCanvas";
    canvas.width = max_width;
    canvas.height = max_height;
    canvas.style.visibility = "hidden";
    document.body.appendChild(canvas);
    context = canvas.getContext('2d');
    if (file.type.match('image.*')) {
        fileLoader.readAsDataURL(file);
    } else {
        alert('File is not an image');
    }
    fileLoader.onload = function() {
        var data = this.result;
        imageObj.src = data;
    };
    fileLoader.onabort = function() {
        alert("The upload was aborted.");
    };
    fileLoader.onerror = function() {
        alert("An error occured while reading the file.");
    };
    imageObj.onload = function() {
        if (this.width == 0 || this.height == 0) {
            alert('Image is empty');
        } else {
            context.clearRect(0, 0, max_width, max_height);
            context.drawImage(imageObj, 0, 0, this.width, this.height, 0, 0, max_width, max_height);
            blob = dataURItoBlob(canvas.toDataURL(imageEncoding));
            upload(blob);
        }
    };
    imageObj.onabort = function() {
        alert("Image load was aborted.");
    };
    imageObj.onerror = function() {
        alert("An error occured while loading image.");
    };
}

function loaderDisplayTillFullyLoad(imageID, loaderImageID) {
    $('#' + imageID).on('load', function() {
        $('#' + loaderImageID).css('display', 'none');
        $('#' + imageID).css('display', 'block');
    });
}

function Awsfileupload(options) {
    var form = options.form;
    var new_filename;
    var tempname;
    var AWSSettingsModel = options.AWSSettingsModel;
    var AllowedExts = options.AllwoedExts;
    var AlertMessage = options.AlertMessage;
    var ResizeRequired = options.ResizeRequired;
    form.fileupload({
        url: form.attr('action'),
        type: 'POST',
        datatype: 'xml',
        add: function(event, data) {
            $(".btn-signup1").attr('disabled', 'disabled');
            $(".cancel").attr('disabled', 'disabled');
            $('.bar').removeClass('red');
            $('.progress').removeClass('red');
            $('.bar').css('width', '0%');
            tempname = data.originalFiles[0].name;
            var filename = data.originalFiles[0].name.split(".");
            var ext = filename[filename.length - 1];
            if (AllowedExts.indexOf(ext) >= 0) {
                var options = {
                    file: data.files[0],
                    callback: function(canvasfile) {
                        var name = generateUUID3(AWSSettingsModel.enc_userid());
                        new_filename = (canvasfile.name);
                        if (AWSSettingsModel.folder())
                            new_filename = AWSSettingsModel.folder() + "/" + name + "." + ext;
                        $('.progress').show();
                        $('#key').val(new_filename);
                        data.files[0] = canvasfile;
                        data.submit();
                    }
                };
                if (ResizeRequired) {
                    ResizeFile(options);
                } else {
                    options.callback(data.files[0]);
                }
            } else {
                ShowAlertMessage(AlertMessage, 'error', error_message_header);
            }
        },
        progress: function(e, data) {
            $('#file').attr('disabled', 'disabled');
            if (options.progressCallback != undefined) {
                options.progressCallback();
            }
            model.EnableAddButton(false);
            var percent = Math.round((data.loaded / data.total) * 100);
            $('.bar').css('width', percent + '%');
        },
        fail: function(e, data) {
            $('.bar').css('width', '100%').addClass('red');
            $('.progress').addClass('red');
            $("#btn-remove").removeAttr('enabled');
            $('#file').removeAttr('enabled');
            $(".btn-signup1").removeAttr('disabled');
            $(".cancel").removeAttr('disabled');
            $('#file').removeAttr('disabled');
        },
        done: function(event, data) {
            window.onbeforeunload = null;
            options.successCallBack(new_filename, tempname);
        }
    });
}

function AWSImageLoader() {
    $('.DisplaySlider').show();
    $.each(document.images, function() {
        var this_image = this;
        var src = $(this_image).attr('src') || '';
        var id = $(this_image).attr('id');
        $('#loader' + id).css('display', 'block');
        if (!src.length > 0) {
            var lsrc = $(this_image).attr('lsrc');
            if (lsrc || lsrc.length > 0) {
                var img = new Image();
                img.src = lsrc;
                $(img).load(function() {
                    this_image.src = this.src;
                    $('#loader' + id).css('display', 'none');
                });
            }
        }
    });
}

function blobToFile(theBlob, fileName) {
    theBlob.lastModifiedDate = new Date();
    theBlob.name = fileName;
    return theBlob;
}

function CheckSizeBeforeResizing(resizeFile, width, height, iteration, callback) {
    var maxHeight = window.DefaultUploadPhotoHeight;
    var maxWidth = window.DefaultUploadPhotoWidth;
    var maxIteration = 3;
    var proposedHeight = (height / 2);
    var proposedWidth = (width / 2);
    if (proposedHeight < parseInt(maxHeight) && proposedWidth >= parseInt(maxWidth)) {
        proposedHeight = parseInt(maxHeight);
    }
    if (proposedWidth < parseInt(maxWidth) && proposedHeight >= parseInt(maxHeight)) {
        proposedWidth = parseInt(maxWidth);
    }
    if (iteration < maxIteration && proposedHeight >= maxHeight && proposedWidth >= maxWidth) {
        canvasResize(resizeFile, {
            width: proposedWidth,
            height: proposedHeight,
            crop: false,
            quality: 100,
            rotate: 0,
            callback: function(data, width, height) {
                iteration++;
                var newfile = blobToFile(canvasResize('dataURLtoBlob', data), resizeFile.name);
                var options = {
                    file: newfile,
                    width: width,
                    height: height,
                    iteration: iteration,
                    callback: callback
                }
                ResizeFile(options);
            }
        });
    } else {
        callback(resizeFile);
    }
}

function ResizeFile(options) {
    var iteration = options.iteration == undefined ? 0 : options.iteration;
    if (options.width == undefined && options.height == undefined) {
        img = new Image();
        img.onload = function() {
            CheckSizeBeforeResizing(options.file, img.width, img.height, iteration, options.callback)
        };
        var _URL = window.URL || window.webkitURL;
        img.src = _URL.createObjectURL(options.file);
    } else {
        CheckSizeBeforeResizing(options.file, options.width, options.height, iteration, options.callback);
    }
}

function getArrayColumn(matrix, col) {
    var column = [];
    for (var i = 0; i < matrix.length; i++) {
        column.push(matrix[i][col]);
    }
    return column;
}
var DeleteFileFromAWS = '/deleteawsfile';
window.ConfirmUploadMessage = "Do you want to save the uploaded files ? ";

function RemoveFileFromAws(async, UploadFilesArray) {
    AjaxCall(DeleteFileFromAWS, ko.toJSON({
        Data: UploadFilesArray
    }), "post", "json", "application/json", true, undefined, async).done(function(response) {
        if (response.IsSuccess) {
            UploadFilesArray = [];
        }
    });
    return true;
}

function CheckOnloadBefore(UploadFilesArray, TempArray) {
    window.onunload = function() {
        if (UploadFilesArray.length > 0) {
            if (UploadFilesArray[0] != TempArray[0]) {
                RemoveFileFromAws(false, UploadFilesArray);
            }
        }
    }
    jQuery(window).bind('beforeunload', function(e) {
        if (UploadFilesArray.length > 0) {
            if (UploadFilesArray[0] != TempArray[0]) {
                e.returnValue = window.ConfirmUploadMessage;
                return window.ConfirmUploadMessage;
            }
        }
    });
}

function SearchObservableArrayWithKeyValue(ObservableArray, KeyName, KeyValue) {
    ko.utils.arrayFirst(ObservableArray, function(item) {
        return item.KeyName == KeyValue;
    });
}

function CalculateAgeDays(startDate, endDate) {
    var start = moment(startDate);
    var end = moment(endDate);
    var days = start.diff(end, "days");
    return days;
}
(function($) {
    $.fn.getStyleObject = function() {
        var dom = this.get(0);
        var style;
        var returns = {};
        if (window.getComputedStyle) {
            var camelize = function(a, b) {
                return b.toUpperCase();
            }
            style = window.getComputedStyle(dom, null);
            for (var i = 0; i < style.length; i++) {
                var prop = style[i];
                var camel = prop.replace(/\-([a-z])/g, camelize);
                var val = style.getPropertyValue(prop);
                returns[camel] = val;
            }
            return returns;
        }
        if (dom.currentStyle) {
            style = dom.currentStyle;
            for (var prop in style) {
                returns[prop] = style[prop];
            }
            return returns;
        }
        return this.css();
    }
})(jQuery);
ko.bindingHandlers.dropzone = {
    init: function(element, valueAccessor) {
        var value = valueAccessor();
        var Dropzone = require("enyo-dropzone");
        Dropzone.autoDiscover = false;
        var FileUploadSettings = value.FileUploadSettings;
        var previewNode = document.querySelector("#template");
        previewNode.id = "";
        var previewTemplate = previewNode.parentNode.innerHTML;
        previewNode.parentNode.removeChild(previewNode);
        window.FileCount = 0;
        var options = {
            url: 'fsdfsd',
            thumbnailWidth: 80,
            thumbnailHeight: 80,
            parallelUploads: 20,
            previewTemplate: previewTemplate,
            autoQueue: false,
            previewsContainer: "#previews",
            clickable: ".fileinput-button",
            accept: function(file, done) {},
            init: function() {
                window.myDropzone = this;
                myDropzone.on("addedfile", function(file) {
                    var allow = model.CheckSelection();
                    file.previewElement.querySelector(".start").onclick = function() {
                        myDropzone.enqueueFile(file);
                    };
                    var filename = file.name.split(".");
                    var ext = filename[filename.length - 1];
                    var name = generateUUID2(model.FileUploadSettingsModel());
                    ext = ext.toLowerCase();
                    if (uploaddocexts.indexOf(ext) >= 0) {
                        FileCount++;
                        var options = {
                            successcallback: function(new_filename) {
                                window.SFmodel = new model.StudentFile();
                                var section = new_filename.substr(new_filename.lastIndexOf('/') + 1);
                                var parts = section.match(/[^\.]+/);
                                var test2 = file.previewElement.querySelector("#studentid");
                                var studentidclass = "studentid_" + parts[0];
                                SFmodel.StudentClass(studentidclass);
                                SFmodel.FilePath(new_filename);
                                SFmodel.FileName(file.name);
                                SFmodel.FileSize(file.size);
                                SFmodel.StudentID();
                                $(test2).addClass(SFmodel.StudentClass());
                                $('.' + SFmodel.StudentClass()).attr("id", SFmodel.StudentClass());
                                model.UploadedFilesKeysArray.push({
                                    filename: file.name,
                                    filepath: new_filename
                                });
                                model.UploadFilesArray.push(SFmodel);
                                ko.applyBindings(model, document.getElementById(studentidclass));
                            },
                            awsSettingModel: model.FileUploadSettingsModel,
                            file: file,
                            ext: ext,
                            generateUniqueID: generateUUID3
                        };
                        if (FileCount == MaxFileUpload) {
                            $('.addFiles').prop('disabled', true);
                        }
                        if (FileCount > MaxFileUpload) {
                            myDropzone.cancelUpload(file);
                            var studentid = file.previewElement.querySelector("#studentid");
                            $(studentid).remove();
                            var CancelBtn = file.previewElement.querySelector(".btn.cancel");
                            $(CancelBtn).removeAttr('disabled');
                            var fileError = file.previewElement.querySelector(".error.text-danger");
                            $(fileError).text('Maximum ' + MaxFileUpload + ' files upload limit exceeded');
                            $('.addFiles').prop('disabled', true);
                        } else {
                            GetDropZoneForAmazonFileUpload(options);
                        }
                    } else {
                        myDropzone.cancelUpload(file);
                        var studentid = file.previewElement.querySelector("#studentid");
                        $(studentid).remove();
                        var CancelBtn = file.previewElement.querySelector(".btn.cancel");
                        $(CancelBtn).removeAttr('disabled');
                        var fileError = file.previewElement.querySelector(".error.text-danger");
                        var MSg = uploaddocexts.indexOf(ext) < 0 ? ' : ' + 'Invalid file format (only .pdf format supported)' : ' : ' + FileSizeExceed;
                        $(fileError).text('Upload failed' + MSg);
                    }
                });
                myDropzone.on("totaluploadprogress", function(progress) {
                    document.querySelector("#total-progress .progress-bar").style.width = progress + "%";
                });
                myDropzone.on("sending", function(file) {
                    document.querySelector("#total-progress").style.opacity = "1";
                });
                myDropzone.on("successmultiple", function(progress) {
                    document.querySelector("#total-progress").style.opacity = "0";
                });
                document.querySelector("#actions .cancel").onclick = function() {
                    myDropzone.removeAllFiles(true);
                    FileCount = 0;
                    $('.addFiles').prop('disabled', false);
                };
                myDropzone.on("removedfile", function(file) {
                    if (file.status == "added") {
                        var filetoremove = SearchObservableArrayByPropertyValue(model.UploadedFilesKeysArray(), file.name);
                        if (model.UploadedFilesKeysArray()[filetoremove]) {
                            model.RemoveFile(model.UploadedFilesKeysArray()[filetoremove].filepath);
                            if (FileCount > MaxFileUpload)
                                FileCount = MaxFileUpload - 1;
                            else
                                FileCount = FileCount - 1;
                            $('.addFiles').prop('disabled', false);
                        } else {
                            return true;
                        }
                    } else {
                        return true;
                    }
                });
                myDropzone.on("canceledmultiple", function(file) {
                    var filetoremove = SearchObservableArrayByPropertyValue(model.UploadedFilesKeysArray(), file.name);
                    model.RemoveFile(model.UploadedFilesKeysArray()[filetoremove].filepath);
                });
            }
        };
        $.extend(options, value);
        new Dropzone(element, options);
    }
};

function GetDropZoneForAmazonFileUpload(options) {
    var awsSettingModel = options.awsSettingModel();
    var file = options.file;
    var name = options.generateUniqueID(awsSettingModel.enc_userid());
    var new_filename = name + "." + options.ext;
    if (awsSettingModel.folder()) {
        new_filename = awsSettingModel.folder() + "/" + new_filename;
        var progressbardiv = file.previewElement.querySelector("#totalprogressbar .progress-bar");
        var progressbarparentdiv = progressbardiv.parentNode;
        var fd = new FormData();
        fd.append('key', new_filename);
        fd.append('acl', awsSettingModel.acl());
        fd.append('AWSAccessKeyId', awsSettingModel.accesskey());
        fd.append('policy', awsSettingModel.base64Policy());
        fd.append('signature', awsSettingModel.signature());
        fd.append('success_action_status', awsSettingModel.success_action());
        fd.append("file", file);
        var xhr = new XMLHttpRequest();
        xhr.upload.addEventListener("progress", function(oEvent) {
            var percent = Math.round((oEvent.loaded / oEvent.total) * 100);
            progressbardiv.style.width = percent + '%';
        }, false);
        xhr.addEventListener("load", function() {
            progressbardiv.remove();
            progressbarparentdiv.style.background = "#85C220";
            document.querySelector("#total-progress").style.opacity = "0";
            var CancelBtn = file.previewElement.querySelector(".btn.cancel");
            $(CancelBtn).removeAttr('disabled');
        }, false);
        xhr.addEventListener("loadstart", function() {
            model.requestCounter(model.requestCounter() + 1);
        });
        xhr.addEventListener("loadend", function(oEvent) {
            if (model.EnableSubmitButton() != undefined)
                model.EnableSubmitButton(true);
            model.responseCounter(model.responseCounter() + 1);
            if (awsSettingModel.success_action() == oEvent.currentTarget.status) {
                options.successcallback(new_filename);
            } else {
                if (progressbardiv != undefined) {
                    progressbardiv.remove();
                    if (progressbarparentdiv != undefined)
                        progressbarparentdiv.style.background = "red";
                }
            }
        });
        xhr.addEventListener("error", function() {
            progressbardiv.remove();
            progressbarparentdiv.style.background = "red";
        }, false);
        xhr.addEventListener("abort", function() {
            progressbardiv.remove();
            progressbarparentdiv.style.background = "red";
        }, false);
        xhr.open('POST', awsSettingModel.url(), true);
        $(file.previewElement.querySelector("button.cancel")).click(function() {
            if (xhr && xhr.readyState != 4) {
                xhr.abort();
            }
        });
        xhr.send(fd);
    }
}
var SearchObservableArrayByPropertyValue = function(obj, value) {
    var returnKey = -1;
    $.each(obj, function(key, info) {
        console.log(info.filename);
        if (info.filename == value) {
            returnKey = key;
            return false;
        }
    });
    return returnKey;
};
ko.bindingHandlers.allowBindings = {
    init: function(elem, valueAccessor) {
        var shouldAllowBindings = ko.utils.unwrapObservable(valueAccessor());
        console.log(shouldAllowBindings);
        return {
            controlsDescendantBindings: shouldAllowBindings
        };
    }
};
ko.bindingHandlers.withProperties = {
    init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {
        var childBindingContext = bindingContext.createChildContext(bindingContext.$rawData, null, function(context) {
            ko.utils.extend(context, valueAccessor());
        });
        ko.applyBindingsToDescendants(childBindingContext, element);
        return {
            controlsDescendantBindings: true
        };
    }
};
ko.subscribable.fn.subscribeChanged = function(callback) {
    var oldValue;
    this.subscribe(function(_oldValue) {
        oldValue = _oldValue;
    }, this, 'beforeChange');
    this.subscribe(function(newValue) {
        callback(newValue, oldValue);
    });
};
ko.bindingHandlers.numeric = {
    init: function(element, valueAccessor) {
        $(element).on("keydown", function(event) {
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) || (event.keyCode >= 35 && event.keyCode <= 39)) {
                return;
            } else {
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        });
    }
};
(function($) {
    "use strict";
    var BootstrapDialog = function(options) {
        this.defaultOptions = {
            id: BootstrapDialog.newGuid(),
            type: BootstrapDialog.TYPE_PRIMARY,
            size: BootstrapDialog.SIZE_NORMAL,
            cssClass: '',
            title: null,
            message: null,
            buttons: [],
            closable: true,
            spinicon: BootstrapDialog.ICON_SPINNER,
            data: {},
            onshow: null,
            onhide: null,
            autodestroy: true,
            draggable: false
        };
        this.indexedButtons = {};
        this.registeredButtonHotkeys = {};
        this.draggableData = {
            isMouseDown: false,
            mouseOffset: {}
        };
        this.realized = false;
        this.opened = false;
        this.initOptions(options);
        this.holdThisInstance();
    };
    BootstrapDialog.NAMESPACE = 'bootstrap-dialog';
    BootstrapDialog.TYPE_DEFAULT = 'type-default';
    BootstrapDialog.TYPE_INFO = 'type-info';
    BootstrapDialog.TYPE_PRIMARY = 'type-primary';
    BootstrapDialog.TYPE_SUCCESS = 'type-success';
    BootstrapDialog.TYPE_WARNING = 'type-warning';
    BootstrapDialog.TYPE_DANGER = 'type-danger';
    BootstrapDialog.DEFAULT_TEXTS = {};
    BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_DEFAULT] = 'Information';
    BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_INFO] = 'Information';
    BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_PRIMARY] = 'Information';
    BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_SUCCESS] = 'Success';
    BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_WARNING] = 'Warning';
    BootstrapDialog.DEFAULT_TEXTS[BootstrapDialog.TYPE_DANGER] = 'Danger';
    BootstrapDialog.SIZE_NORMAL = 'size-normal';
    BootstrapDialog.SIZE_LARGE = 'size-large';
    BootstrapDialog.BUTTON_SIZES = {};
    BootstrapDialog.BUTTON_SIZES[BootstrapDialog.SIZE_NORMAL] = '';
    BootstrapDialog.BUTTON_SIZES[BootstrapDialog.SIZE_LARGE] = 'btn-lg';
    BootstrapDialog.ICON_SPINNER = 'glyphicon glyphicon-asterisk';
    BootstrapDialog.dialogs = {};
    BootstrapDialog.openAll = function() {
        $.each(BootstrapDialog.dialogs, function(id, dialogInstance) {
            dialogInstance.open();
        });
    };
    BootstrapDialog.closeAll = function() {
        $.each(BootstrapDialog.dialogs, function(id, dialogInstance) {
            dialogInstance.close();
        });
    };
    BootstrapDialog.prototype = {
        constructor: BootstrapDialog,
        initOptions: function(options) {
            this.options = $.extend(true, this.defaultOptions, options);
            return this;
        },
        holdThisInstance: function() {
            BootstrapDialog.dialogs[this.getId()] = this;
            return this;
        },
        initModalStuff: function() {
            this.setModal(this.createModal()).setModalDialog(this.createModalDialog()).setModalContent(this.createModalContent()).setModalHeader(this.createModalHeader()).setModalBody(this.createModalBody()).setModalFooter(this.createModalFooter());
            this.getModal().append(this.getModalDialog());
            this.getModalDialog().append(this.getModalContent());
            this.getModalContent().append(this.getModalHeader()).append(this.getModalBody()).append(this.getModalFooter());
            return this;
        },
        createModal: function() {
            var $modal = $('<div class="modal fade" tabindex="-1"></div>');
            $modal.prop('id', this.getId());
            return $modal;
        },
        getModal: function() {
            return this.$modal;
        },
        setModal: function($modal) {
            this.$modal = $modal;
            return this;
        },
        createModalDialog: function() {
            return $('<div class="modal-dialog"></div>');
        },
        getModalDialog: function() {
            return this.$modalDialog;
        },
        setModalDialog: function($modalDialog) {
            this.$modalDialog = $modalDialog;
            return this;
        },
        createModalContent: function() {
            return $('<div class="modal-content"></div>');
        },
        getModalContent: function() {
            return this.$modalContent;
        },
        setModalContent: function($modalContent) {
            this.$modalContent = $modalContent;
            return this;
        },
        createModalHeader: function() {
            return $('<div class="modal-header"></div>');
        },
        getModalHeader: function() {
            return this.$modalHeader;
        },
        setModalHeader: function($modalHeader) {
            this.$modalHeader = $modalHeader;
            return this;
        },
        createModalBody: function() {
            return $('<div class="modal-body"></div>');
        },
        getModalBody: function() {
            return this.$modalBody;
        },
        setModalBody: function($modalBody) {
            this.$modalBody = $modalBody;
            return this;
        },
        createModalFooter: function() {
            return $('<div class="modal-footer"></div>');
        },
        getModalFooter: function() {
            return this.$modalFooter;
        },
        setModalFooter: function($modalFooter) {
            this.$modalFooter = $modalFooter;
            return this;
        },
        createDynamicContent: function(rawContent) {
            var content = null;
            if (typeof rawContent === 'function') {
                content = rawContent.call(rawContent, this);
            } else {
                content = rawContent;
            }
            if (typeof content === 'string') {
                content = this.formatStringContent(content);
            }
            return content;
        },
        formatStringContent: function(content) {
            return content.replace(/\r\n/g, '<br />').replace(/[\r\n]/g, '<br />');
        },
        setData: function(key, value) {
            this.options.data[key] = value;
            return this;
        },
        getData: function(key) {
            return this.options.data[key];
        },
        setId: function(id) {
            this.options.id = id;
            return this;
        },
        getId: function() {
            return this.options.id;
        },
        getType: function() {
            return this.options.type;
        },
        setType: function(type) {
            this.options.type = type;
            return this;
        },
        getSize: function() {
            return this.options.size;
        },
        setSize: function(size) {
            this.options.size = size;
            return this;
        },
        getCssClass: function() {
            return this.options.cssClass;
        },
        setCssClass: function(cssClass) {
            this.options.cssClass = cssClass;
            return this;
        },
        getTitle: function() {
            return this.options.title;
        },
        setTitle: function(title) {
            this.options.title = title;
            this.updateTitle();
            return this;
        },
        updateTitle: function() {
            if (this.isRealized()) {
                var title = this.getTitle() !== null ? this.createDynamicContent(this.getTitle()) : this.getDefaultText();
                this.getModalHeader().find('.' + this.getNamespace('title')).html('').append(title);
            }
            return this;
        },
        getMessage: function() {
            return this.options.message;
        },
        setMessage: function(message) {
            this.options.message = message;
            this.updateMessage();
            return this;
        },
        updateMessage: function() {
            if (this.isRealized()) {
                var message = this.createDynamicContent(this.getMessage());
                this.getModalBody().find('.' + this.getNamespace('message')).html('').append(message);
            }
            return this;
        },
        isClosable: function() {
            return this.options.closable;
        },
        setClosable: function(closable) {
            this.options.closable = closable;
            this.updateClosable();
            return this;
        },
        getSpinicon: function() {
            return this.options.spinicon;
        },
        setSpinicon: function(spinicon) {
            this.options.spinicon = spinicon;
            return this;
        },
        addButton: function(button) {
            this.options.buttons.push(button);
            return this;
        },
        addButtons: function(buttons) {
            var that = this;
            $.each(buttons, function(index, button) {
                that.addButton(button);
            });
            return this;
        },
        getButtons: function() {
            return this.options.buttons;
        },
        setButtons: function(buttons) {
            this.options.buttons = buttons;
            return this;
        },
        getButton: function(id) {
            if (typeof this.indexedButtons[id] !== 'undefined') {
                return this.indexedButtons[id];
            }
            return null;
        },
        getButtonSize: function() {
            if (typeof BootstrapDialog.BUTTON_SIZES[this.getSize()] !== 'undefined') {
                return BootstrapDialog.BUTTON_SIZES[this.getSize()];
            }
            return '';
        },
        isAutodestroy: function() {
            return this.options.autodestroy;
        },
        setAutodestroy: function(autodestroy) {
            this.options.autodestroy = autodestroy;
        },
        getDefaultText: function() {
            return BootstrapDialog.DEFAULT_TEXTS[this.getType()];
        },
        getNamespace: function(name) {
            return BootstrapDialog.NAMESPACE + '-' + name;
        },
        createHeaderContent: function() {
            var $container = $('<div></div>');
            $container.addClass(this.getNamespace('header'));
            $container.append(this.createTitleContent());
            $container.append(this.createCloseButton());
            return $container;
        },
        createTitleContent: function() {
            var $title = $('<div></div>');
            $title.addClass(this.getNamespace('title'));
            return $title;
        },
        createCloseButton: function() {
            var $container = $('<div></div>');
            $container.addClass(this.getNamespace('close-button'));
            var $icon = $('<button class="close">&times;</button>');
            $container.append($icon);
            $container.on('click', {
                dialog: this
            }, function(event) {
                event.data.dialog.close();
            });
            return $container;
        },
        createBodyContent: function() {
            var $container = $('<div></div>');
            $container.addClass(this.getNamespace('body'));
            $container.append(this.createMessageContent());
            return $container;
        },
        createMessageContent: function() {
            var $message = $('<div></div>');
            $message.addClass(this.getNamespace('message'));
            return $message;
        },
        createFooterContent: function() {
            var $container = $('<div></div>');
            $container.addClass(this.getNamespace('footer'));
            $container.append(this.createFooterButtons());
            return $container;
        },
        createFooterButtons: function() {
            var that = this;
            var $container = $('<div></div>');
            $container.addClass(this.getNamespace('footer-buttons'));
            this.indexedButtons = {};
            $.each(this.options.buttons, function(index, button) {
                if (!button.id) {
                    button.id = BootstrapDialog.newGuid();
                }
                var $button = that.createButton(button);
                that.indexedButtons[button.id] = $button;
                $container.append($button);
            });
            return $container;
        },
        createButton: function(button) {
            var $button = $('<button class="btn"></button>');
            $button.addClass(this.getButtonSize());
            $button.prop('id', button.id);
            if (typeof button.icon !== undefined && $.trim(button.icon) !== '') {
                $button.append(this.createButtonIcon(button.icon));
            }
            if (typeof button.label !== undefined) {
                $button.append(button.label);
            }
            if (typeof button.cssClass !== undefined && $.trim(button.cssClass) !== '') {
                $button.addClass(button.cssClass);
            } else {
                $button.addClass('btn-default');
            }
            if (typeof button.hotkey !== undefined) {
                this.registeredButtonHotkeys[button.hotkey] = $button;
            }
            $button.on('click', {
                dialog: this,
                $button: $button,
                button: button
            }, function(event) {
                var dialog = event.data.dialog;
                var $button = event.data.$button;
                var button = event.data.button;
                if (typeof button.action === 'function') {
                    button.action.call($button, dialog);
                }
                if (button.autospin) {
                    $button.toggleSpin(true);
                }
            });
            this.enhanceButton($button);
            return $button;
        },
        enhanceButton: function($button) {
            $button.dialog = this;
            $button.toggleEnable = function(enable) {
                var $this = this;
                $this.prop("disabled", !enable).toggleClass('disabled', !enable);
                return $this;
            };
            $button.enable = function() {
                var $this = this;
                $this.toggleEnable(true);
                return $this;
            };
            $button.disable = function() {
                var $this = this;
                $this.toggleEnable(false);
                return $this;
            };
            $button.toggleSpin = function(spin) {
                var $this = this;
                var dialog = $this.dialog;
                var $icon = $this.find('.' + dialog.getNamespace('button-icon'));
                if (spin) {
                    $icon.hide();
                    $button.prepend(dialog.createButtonIcon(dialog.getSpinicon()).addClass('icon-spin'));
                } else {
                    $icon.show();
                    $button.find('.icon-spin').remove();
                }
                return $this;
            };
            $button.spin = function() {
                var $this = this;
                $this.toggleSpin(true);
                return $this;
            };
            $button.stopSpin = function() {
                var $this = this;
                $this.toggleSpin(false);
                return $this;
            };
            return this;
        },
        createButtonIcon: function(icon) {
            var $icon = $('<span></span>');
            $icon.addClass(this.getNamespace('button-icon')).addClass(icon);
            return $icon;
        },
        enableButtons: function(enable) {
            $.each(this.indexedButtons, function(id, $button) {
                $button.toggleEnable(enable);
            });
            return this;
        },
        updateClosable: function() {
            if (this.isRealized()) {
                this.getModalHeader().find('.' + this.getNamespace('close-button')).toggle(this.isClosable());
            }
            return this;
        },
        onShow: function(onshow) {
            this.options.onshow = onshow;
            return this;
        },
        onHide: function(onhide) {
            this.options.onhide = onhide;
            return this;
        },
        isRealized: function() {
            return this.realized;
        },
        setRealized: function(realized) {
            this.realized = realized;
            return this;
        },
        isOpened: function() {
            return this.opened;
        },
        setOpened: function(opened) {
            this.opened = opened;
            return this;
        },
        handleModalEvents: function() {
            this.getModal().on('show.bs.modal', {
                dialog: this
            }, function(event) {
                var dialog = event.data.dialog;
                typeof dialog.options.onshow === 'function' && dialog.options.onshow(dialog);
                dialog.showPageScrollBar(true);
            });
            this.getModal().on('hide.bs.modal', {
                dialog: this
            }, function(event) {
                var dialog = event.data.dialog;
                typeof dialog.options.onhide === 'function' && dialog.options.onhide(dialog);
            });
            this.getModal().on('hidden.bs.modal', {
                dialog: this
            }, function(event) {
                var dialog = event.data.dialog;
                dialog.isAutodestroy() && $(this).remove();
                dialog.showPageScrollBar(false);
            });
            this.getModal().on('click', {
                dialog: this
            }, function(event) {
                event.target === this && event.data.dialog.isClosable() && event.data.dialog.close();
            });
            this.getModal().on('keyup', {
                dialog: this
            }, function(event) {
                event.which === 27 && event.data.dialog.isClosable() && event.data.dialog.close();
            });
            this.getModal().on('keyup', {
                dialog: this
            }, function(event) {
                var dialog = event.data.dialog;
                if (typeof dialog.registeredButtonHotkeys[event.which] !== 'undefined') {
                    var $button = $(dialog.registeredButtonHotkeys[event.which]);
                    !$button.prop('disabled') && $button.focus().trigger('click');
                }
            });
            return this;
        },
        makeModalDraggable: function() {
            if (this.options.draggable) {
                this.getModalHeader().addClass(this.getNamespace('draggable')).on('mousedown', {
                    dialog: this
                }, function(event) {
                    var dialog = event.data.dialog;
                    dialog.draggableData.isMouseDown = true;
                    var dialogOffset = dialog.getModalContent().offset();
                    dialog.draggableData.mouseOffset = {
                        top: event.clientY - dialogOffset.top,
                        left: event.clientX - dialogOffset.left
                    };
                });
                this.getModal().on('mouseup mouseleave', {
                    dialog: this
                }, function(event) {
                    event.data.dialog.draggableData.isMouseDown = false;
                });
                $('body').on('mousemove', {
                    dialog: this
                }, function(event) {
                    var dialog = event.data.dialog;
                    if (!dialog.draggableData.isMouseDown) {
                        return;
                    }
                    dialog.getModalContent().offset({
                        top: event.clientY - dialog.draggableData.mouseOffset.top,
                        left: event.clientX - dialog.draggableData.mouseOffset.left
                    });
                });
            }
            return this;
        },
        showPageScrollBar: function(show) {
            $(document.body).toggleClass('modal-open', show);
        },
        realize: function() {
            this.initModalStuff();
            this.getModal().addClass(BootstrapDialog.NAMESPACE).addClass(this.getType()).addClass(this.getSize()).addClass(this.getCssClass());
            this.getModalFooter().append(this.createFooterContent());
            this.getModalHeader().append(this.createHeaderContent());
            this.getModalBody().append(this.createBodyContent());
            this.getModal().modal({
                backdrop: 'static',
                keyboard: false,
                show: false
            });
            this.makeModalDraggable();
            this.handleModalEvents();
            this.setRealized(true);
            this.updateTitle();
            this.updateMessage();
            this.updateClosable();
            return this;
        },
        open: function() {
            !this.isRealized() && this.realize();
            this.getModal().modal('show');
            this.setOpened(true);
            return this;
        },
        close: function() {
            this.getModal().modal('hide');
            if (this.isAutodestroy()) {
                delete BootstrapDialog.dialogs[this.getId()];
            }
            this.setOpened(false);
            return this;
        }
    };
    BootstrapDialog.newGuid = function() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random() * 16 | 0,
                v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
    BootstrapDialog.show = function(options) {
        return new BootstrapDialog(options).open();
    };
    BootstrapDialog.alert = function() {
        var options = {};
        var defaultOptions = {
            type: BootstrapDialog.TYPE_PRIMARY,
            title: null,
            message: null,
            closable: true,
            buttonLabel: 'OK',
            callback: null
        };
        if (typeof arguments[0] === 'object' && arguments[0].constructor === {}.constructor) {
            options = $.extend(true, defaultOptions, arguments[0]);
        } else {
            options = $.extend(true, defaultOptions, {
                message: arguments[0],
                closable: false,
                buttonLabel: 'OK',
                callback: typeof arguments[1] !== 'undefined' ? arguments[1] : null
            });
        }
        return new BootstrapDialog({
            type: options.type,
            title: options.title,
            message: options.message,
            closable: options.closable,
            data: {
                callback: options.callback
            },
            onhide: function(dialog) {
                !dialog.getData('btnClicked') && dialog.isClosable() && typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(false);
            },
            buttons: [{
                label: options.buttonLabel,
                action: function(dialog) {
                    dialog.setData('btnClicked', true);
                    typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(true);
                    dialog.close();
                }
            }]
        }).open();
    };
    BootstrapDialog.confirm = function(message, callback) {
        return new BootstrapDialog({
            title: 'Confirmation',
            message: message,
            closable: false,
            data: {
                'callback': callback
            },
            buttons: [{
                label: 'Cancel',
                action: function(dialog) {
                    typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(false);
                    dialog.close();
                }
            }, {
                label: 'OK',
                cssClass: 'btn-primary',
                action: function(dialog) {
                    typeof dialog.getData('callback') === 'function' && dialog.getData('callback')(true);
                    dialog.close();
                }
            }]
        }).open();
    };
    BootstrapDialog.init = function() {
        var hasModule = (typeof module !== 'undefined' && module.exports);
        if (hasModule)
            module.exports = BootstrapDialog;
        else if (typeof define === "function" && define.amd)
            define("bootstrap-dialog", function() {
                return BootstrapDialog;
            });
        else
            window.BootstrapDialog = BootstrapDialog;
    };
    BootstrapDialog.init();
})(window.jQuery);
(function($) {
    $.msgGrowl = function(config) {
        var defaults, options, container, msgGrowl, content, title, text, close;
        defaults = {
            type: '',
            title: '',
            text: '',
            lifetime: 6500,
            sticky: false,
            position: 'bottom-right',
            closeTrigger: true,
            onOpen: function() {},
            onClose: function() {}
        };
        options = $.extend(defaults, config);
        container = $('.msgGrowl-container.' + options.position);
        if (!container.length) {
            container = $('<div>', {
                'class': 'msgGrowl-container ' + options.position
            }).appendTo('body');
        }
        msgGrowl = $('<div>', {
            'class': 'msgGrowl ' + options.type
        });
        content = $('<div>', {
            'class': 'msgGrowl-content'
        }).appendTo(msgGrowl);
        text = $('<strong>Awesome!&nbsp;</strong>', {
            text: options.text
        }).appendTo(content);
        text = $('<span>', {
            text: options.text
        }).appendTo(content);
        if (options.closeTrigger) {
            close = $('<div>', {
                'class': 'msgGrowl-close',
                'click': function(e) {
                    e.preventDefault();
                    $(this).parent().fadeOut('medium', function() {
                        $(this).remove();
                        if (typeof options.onClose === 'function') {
                            options.onClose();
                        }
                    });
                }
            }).appendTo(msgGrowl);
        }
        if (options.title != '') {
            title = $('<h4>', {
                text: options.title
            }).prependTo(content);
        }
        if (options.lifetime > 0 && !options.sticky) {
            setTimeout(function() {
                if (typeof options.onClose === 'function') {
                    options.onClose();
                }
                msgGrowl.fadeOut('medium', function() {
                    $(this).remove();
                });
            }, options.lifetime);
        }
        container.addClass(options.position);
        if (options.position.split('-')[0] == 'top') {
            msgGrowl.prependTo(container).hide().fadeIn('slow');
        } else {
            msgGrowl.appendTo(container).hide().fadeIn('slow');
        }
        if (typeof options.onOpen === 'function') {
            options.onOpen();
        }
    }
})(jQuery);
(function(a) {
    function b() {
        return {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1
        }
    }

    function c(a, b) {
        return function(c) {
            return k(a.call(this, c), b)
        }
    }

    function d(a, b) {
        return function(c) {
            return this.lang().ordinal(a.call(this, c), b)
        }
    }

    function e() {}

    function f(a) {
        w(a), h(this, a)
    }

    function g(a) {
        var b = q(a),
            c = b.year || 0,
            d = b.month || 0,
            e = b.week || 0,
            f = b.day || 0,
            g = b.hour || 0,
            h = b.minute || 0,
            i = b.second || 0,
            j = b.millisecond || 0;
        this._milliseconds = +j + 1e3 * i + 6e4 * h + 36e5 * g, this._days = +f + 7 * e, this._months = +d + 12 * c, this._data = {}, this._bubble()
    }

    function h(a, b) {
        for (var c in b) b.hasOwnProperty(c) && (a[c] = b[c]);
        return b.hasOwnProperty("toString") && (a.toString = b.toString), b.hasOwnProperty("valueOf") && (a.valueOf = b.valueOf), a
    }

    function i(a) {
        var b, c = {};
        for (b in a) a.hasOwnProperty(b) && qb.hasOwnProperty(b) && (c[b] = a[b]);
        return c
    }

    function j(a) {
        return 0 > a ? Math.ceil(a) : Math.floor(a)
    }

    function k(a, b, c) {
        for (var d = "" + Math.abs(a), e = a >= 0; d.length < b;) d = "0" + d;
        return (e ? c ? "+" : "" : "-") + d
    }

    function l(a, b, c, d) {
        var e, f, g = b._milliseconds,
            h = b._days,
            i = b._months;
        g && a._d.setTime(+a._d + g * c), (h || i) && (e = a.minute(), f = a.hour()), h && a.date(a.date() + h * c), i && a.month(a.month() + i * c), g && !d && db.updateOffset(a), (h || i) && (a.minute(e), a.hour(f))
    }

    function m(a) {
        return "[object Array]" === Object.prototype.toString.call(a)
    }

    function n(a) {
        return "[object Date]" === Object.prototype.toString.call(a) || a instanceof Date
    }

    function o(a, b, c) {
        var d, e = Math.min(a.length, b.length),
            f = Math.abs(a.length - b.length),
            g = 0;
        for (d = 0; e > d; d++)(c && a[d] !== b[d] || !c && s(a[d]) !== s(b[d])) && g++;
        return g + f
    }

    function p(a) {
        if (a) {
            var b = a.toLowerCase().replace(/(.)s$/, "$1");
            a = Tb[a] || Ub[b] || b
        }
        return a
    }

    function q(a) {
        var b, c, d = {};
        for (c in a) a.hasOwnProperty(c) && (b = p(c), b && (d[b] = a[c]));
        return d
    }

    function r(b) {
        var c, d;
        if (0 === b.indexOf("week")) c = 7, d = "day";
        else {
            if (0 !== b.indexOf("month")) return;
            c = 12, d = "month"
        }
        db[b] = function(e, f) {
            var g, h, i = db.fn._lang[b],
                j = [];
            if ("number" == typeof e && (f = e, e = a), h = function(a) {
                    var b = db().utc().set(d, a);
                    return i.call(db.fn._lang, b, e || "")
                }, null != f) return h(f);
            for (g = 0; c > g; g++) j.push(h(g));
            return j
        }
    }

    function s(a) {
        var b = +a,
            c = 0;
        return 0 !== b && isFinite(b) && (c = b >= 0 ? Math.floor(b) : Math.ceil(b)), c
    }

    function t(a, b) {
        return new Date(Date.UTC(a, b + 1, 0)).getUTCDate()
    }

    function u(a) {
        return v(a) ? 366 : 365
    }

    function v(a) {
        return a % 4 === 0 && a % 100 !== 0 || a % 400 === 0
    }

    function w(a) {
        var b;
        a._a && -2 === a._pf.overflow && (b = a._a[jb] < 0 || a._a[jb] > 11 ? jb : a._a[kb] < 1 || a._a[kb] > t(a._a[ib], a._a[jb]) ? kb : a._a[lb] < 0 || a._a[lb] > 23 ? lb : a._a[mb] < 0 || a._a[mb] > 59 ? mb : a._a[nb] < 0 || a._a[nb] > 59 ? nb : a._a[ob] < 0 || a._a[ob] > 999 ? ob : -1, a._pf._overflowDayOfYear && (ib > b || b > kb) && (b = kb), a._pf.overflow = b)
    }

    function x(a) {
        return null == a._isValid && (a._isValid = !isNaN(a._d.getTime()) && a._pf.overflow < 0 && !a._pf.empty && !a._pf.invalidMonth && !a._pf.nullInput && !a._pf.invalidFormat && !a._pf.userInvalidated, a._strict && (a._isValid = a._isValid && 0 === a._pf.charsLeftOver && 0 === a._pf.unusedTokens.length)), a._isValid
    }

    function y(a) {
        return a ? a.toLowerCase().replace("_", "-") : a
    }

    function z(a, b) {
        return b._isUTC ? db(a).zone(b._offset || 0) : db(a).local()
    }

    function A(a, b) {
        return b.abbr = a, pb[a] || (pb[a] = new e), pb[a].set(b), pb[a]
    }

    function B(a) {
        delete pb[a]
    }

    function C(a) {
        var b, c, d, e, f = 0,
            g = function(a) {
                if (!pb[a] && rb) try {
                    require("./lang/" + a)
                } catch (b) {}
                return pb[a]
            };
        if (!a) return db.fn._lang;
        if (!m(a)) {
            if (c = g(a)) return c;
            a = [a]
        }
        for (; f < a.length;) {
            for (e = y(a[f]).split("-"), b = e.length, d = y(a[f + 1]), d = d ? d.split("-") : null; b > 0;) {
                if (c = g(e.slice(0, b).join("-"))) return c;
                if (d && d.length >= b && o(e, d, !0) >= b - 1) break;
                b--
            }
            f++
        }
        return db.fn._lang
    }

    function D(a) {
        return a.match(/\[[\s\S]/) ? a.replace(/^\[|\]$/g, "") : a.replace(/\\/g, "")
    }

    function E(a) {
        var b, c, d = a.match(vb);
        for (b = 0, c = d.length; c > b; b++) d[b] = Yb[d[b]] ? Yb[d[b]] : D(d[b]);
        return function(e) {
            var f = "";
            for (b = 0; c > b; b++) f += d[b] instanceof Function ? d[b].call(e, a) : d[b];
            return f
        }
    }

    function F(a, b) {
        return a.isValid() ? (b = G(b, a.lang()), Vb[b] || (Vb[b] = E(b)), Vb[b](a)) : a.lang().invalidDate()
    }

    function G(a, b) {
        function c(a) {
            return b.longDateFormat(a) || a
        }
        var d = 5;
        for (wb.lastIndex = 0; d >= 0 && wb.test(a);) a = a.replace(wb, c), wb.lastIndex = 0, d -= 1;
        return a
    }

    function H(a, b) {
        var c, d = b._strict;
        switch (a) {
            case "DDDD":
                return Ib;
            case "YYYY":
            case "GGGG":
            case "gggg":
                return d ? Jb : zb;
            case "Y":
            case "G":
            case "g":
                return Lb;
            case "YYYYYY":
            case "YYYYY":
            case "GGGGG":
            case "ggggg":
                return d ? Kb : Ab;
            case "S":
                if (d) return Gb;
            case "SS":
                if (d) return Hb;
            case "SSS":
                if (d) return Ib;
            case "DDD":
                return yb;
            case "MMM":
            case "MMMM":
            case "dd":
            case "ddd":
            case "dddd":
                return Cb;
            case "a":
            case "A":
                return C(b._l)._meridiemParse;
            case "X":
                return Fb;
            case "Z":
            case "ZZ":
                return Db;
            case "T":
                return Eb;
            case "SSSS":
                return Bb;
            case "MM":
            case "DD":
            case "YY":
            case "GG":
            case "gg":
            case "HH":
            case "hh":
            case "mm":
            case "ss":
            case "ww":
            case "WW":
                return d ? Hb : xb;
            case "M":
            case "D":
            case "d":
            case "H":
            case "h":
            case "m":
            case "s":
            case "w":
            case "W":
            case "e":
            case "E":
                return xb;
            default:
                return c = new RegExp(P(O(a.replace("\\", "")), "i"))
        }
    }

    function I(a) {
        a = a || "";
        var b = a.match(Db) || [],
            c = b[b.length - 1] || [],
            d = (c + "").match(Qb) || ["-", 0, 0],
            e = +(60 * d[1]) + s(d[2]);
        return "+" === d[0] ? -e : e
    }

    function J(a, b, c) {
        var d, e = c._a;
        switch (a) {
            case "M":
            case "MM":
                null != b && (e[jb] = s(b) - 1);
                break;
            case "MMM":
            case "MMMM":
                d = C(c._l).monthsParse(b), null != d ? e[jb] = d : c._pf.invalidMonth = b;
                break;
            case "D":
            case "DD":
                null != b && (e[kb] = s(b));
                break;
            case "DDD":
            case "DDDD":
                null != b && (c._dayOfYear = s(b));
                break;
            case "YY":
                e[ib] = s(b) + (s(b) > 68 ? 1900 : 2e3);
                break;
            case "YYYY":
            case "YYYYY":
            case "YYYYYY":
                e[ib] = s(b);
                break;
            case "a":
            case "A":
                c._isPm = C(c._l).isPM(b);
                break;
            case "H":
            case "HH":
            case "h":
            case "hh":
                e[lb] = s(b);
                break;
            case "m":
            case "mm":
                e[mb] = s(b);
                break;
            case "s":
            case "ss":
                e[nb] = s(b);
                break;
            case "S":
            case "SS":
            case "SSS":
            case "SSSS":
                e[ob] = s(1e3 * ("0." + b));
                break;
            case "X":
                c._d = new Date(1e3 * parseFloat(b));
                break;
            case "Z":
            case "ZZ":
                c._useUTC = !0, c._tzm = I(b);
                break;
            case "w":
            case "ww":
            case "W":
            case "WW":
            case "d":
            case "dd":
            case "ddd":
            case "dddd":
            case "e":
            case "E":
                a = a.substr(0, 1);
            case "gg":
            case "gggg":
            case "GG":
            case "GGGG":
            case "GGGGG":
                a = a.substr(0, 2), b && (c._w = c._w || {}, c._w[a] = b)
        }
    }

    function K(a) {
        var b, c, d, e, f, g, h, i, j, k, l = [];
        if (!a._d) {
            for (d = M(a), a._w && null == a._a[kb] && null == a._a[jb] && (f = function(b) {
                    var c = parseInt(b, 10);
                    return b ? b.length < 3 ? c > 68 ? 1900 + c : 2e3 + c : c : null == a._a[ib] ? db().weekYear() : a._a[ib]
                }, g = a._w, null != g.GG || null != g.W || null != g.E ? h = Z(f(g.GG), g.W || 1, g.E, 4, 1) : (i = C(a._l), j = null != g.d ? V(g.d, i) : null != g.e ? parseInt(g.e, 10) + i._week.dow : 0, k = parseInt(g.w, 10) || 1, null != g.d && j < i._week.dow && k++, h = Z(f(g.gg), k, j, i._week.doy, i._week.dow)), a._a[ib] = h.year, a._dayOfYear = h.dayOfYear), a._dayOfYear && (e = null == a._a[ib] ? d[ib] : a._a[ib], a._dayOfYear > u(e) && (a._pf._overflowDayOfYear = !0), c = U(e, 0, a._dayOfYear), a._a[jb] = c.getUTCMonth(), a._a[kb] = c.getUTCDate()), b = 0; 3 > b && null == a._a[b]; ++b) a._a[b] = l[b] = d[b];
            for (; 7 > b; b++) a._a[b] = l[b] = null == a._a[b] ? 2 === b ? 1 : 0 : a._a[b];
            l[lb] += s((a._tzm || 0) / 60), l[mb] += s((a._tzm || 0) % 60), a._d = (a._useUTC ? U : T).apply(null, l)
        }
    }

    function L(a) {
        var b;
        a._d || (b = q(a._i), a._a = [b.year, b.month, b.day, b.hour, b.minute, b.second, b.millisecond], K(a))
    }

    function M(a) {
        var b = new Date;
        return a._useUTC ? [b.getUTCFullYear(), b.getUTCMonth(), b.getUTCDate()] : [b.getFullYear(), b.getMonth(), b.getDate()]
    }

    function N(a) {
        a._a = [], a._pf.empty = !0;
        var b, c, d, e, f, g = C(a._l),
            h = "" + a._i,
            i = h.length,
            j = 0;
        for (d = G(a._f, g).match(vb) || [], b = 0; b < d.length; b++) e = d[b], c = (h.match(H(e, a)) || [])[0], c && (f = h.substr(0, h.indexOf(c)), f.length > 0 && a._pf.unusedInput.push(f), h = h.slice(h.indexOf(c) + c.length), j += c.length), Yb[e] ? (c ? a._pf.empty = !1 : a._pf.unusedTokens.push(e), J(e, c, a)) : a._strict && !c && a._pf.unusedTokens.push(e);
        a._pf.charsLeftOver = i - j, h.length > 0 && a._pf.unusedInput.push(h), a._isPm && a._a[lb] < 12 && (a._a[lb] += 12), a._isPm === !1 && 12 === a._a[lb] && (a._a[lb] = 0), K(a), w(a)
    }

    function O(a) {
        return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(a, b, c, d, e) {
            return b || c || d || e
        })
    }

    function P(a) {
        return a.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
    }

    function Q(a) {
        var c, d, e, f, g;
        if (0 === a._f.length) return a._pf.invalidFormat = !0, a._d = new Date(0 / 0), void 0;
        for (f = 0; f < a._f.length; f++) g = 0, c = h({}, a), c._pf = b(), c._f = a._f[f], N(c), x(c) && (g += c._pf.charsLeftOver, g += 10 * c._pf.unusedTokens.length, c._pf.score = g, (null == e || e > g) && (e = g, d = c));
        h(a, d || c)
    }

    function R(a) {
        var b, c, d = a._i,
            e = Mb.exec(d);
        if (e) {
            for (a._pf.iso = !0, b = 0, c = Ob.length; c > b; b++)
                if (Ob[b][1].exec(d)) {
                    a._f = Ob[b][0] + (e[6] || " ");
                    break
                }
            for (b = 0, c = Pb.length; c > b; b++)
                if (Pb[b][1].exec(d)) {
                    a._f += Pb[b][0];
                    break
                }
            d.match(Db) && (a._f += "Z"), N(a)
        } else a._d = new Date(d)
    }

    function S(b) {
        var c = b._i,
            d = sb.exec(c);
        c === a ? b._d = new Date : d ? b._d = new Date(+d[1]) : "string" == typeof c ? R(b) : m(c) ? (b._a = c.slice(0), K(b)) : n(c) ? b._d = new Date(+c) : "object" == typeof c ? L(b) : b._d = new Date(c)
    }

    function T(a, b, c, d, e, f, g) {
        var h = new Date(a, b, c, d, e, f, g);
        return 1970 > a && h.setFullYear(a), h
    }

    function U(a) {
        var b = new Date(Date.UTC.apply(null, arguments));
        return 1970 > a && b.setUTCFullYear(a), b
    }

    function V(a, b) {
        if ("string" == typeof a)
            if (isNaN(a)) {
                if (a = b.weekdaysParse(a), "number" != typeof a) return null
            } else a = parseInt(a, 10);
        return a
    }

    function W(a, b, c, d, e) {
        return e.relativeTime(b || 1, !!c, a, d)
    }

    function X(a, b, c) {
        var d = hb(Math.abs(a) / 1e3),
            e = hb(d / 60),
            f = hb(e / 60),
            g = hb(f / 24),
            h = hb(g / 365),
            i = 45 > d && ["s", d] || 1 === e && ["m"] || 45 > e && ["mm", e] || 1 === f && ["h"] || 22 > f && ["hh", f] || 1 === g && ["d"] || 25 >= g && ["dd", g] || 45 >= g && ["M"] || 345 > g && ["MM", hb(g / 30)] || 1 === h && ["y"] || ["yy", h];
        return i[2] = b, i[3] = a > 0, i[4] = c, W.apply({}, i)
    }

    function Y(a, b, c) {
        var d, e = c - b,
            f = c - a.day();
        return f > e && (f -= 7), e - 7 > f && (f += 7), d = db(a).add("d", f), {
            week: Math.ceil(d.dayOfYear() / 7),
            year: d.year()
        }
    }

    function Z(a, b, c, d, e) {
        var f, g, h = U(a, 0, 1).getUTCDay();
        return c = null != c ? c : e, f = e - h + (h > d ? 7 : 0) - (e > h ? 7 : 0), g = 7 * (b - 1) + (c - e) + f + 1, {
            year: g > 0 ? a : a - 1,
            dayOfYear: g > 0 ? g : u(a - 1) + g
        }
    }

    function $(a) {
        var b = a._i,
            c = a._f;
        return null === b ? db.invalid({
            nullInput: !0
        }) : ("string" == typeof b && (a._i = b = C().preparse(b)), db.isMoment(b) ? (a = i(b), a._d = new Date(+b._d)) : c ? m(c) ? Q(a) : N(a) : S(a), new f(a))
    }

    function _(a, b) {
        db.fn[a] = db.fn[a + "s"] = function(a) {
            var c = this._isUTC ? "UTC" : "";
            return null != a ? (this._d["set" + c + b](a), db.updateOffset(this), this) : this._d["get" + c + b]()
        }
    }

    function ab(a) {
        db.duration.fn[a] = function() {
            return this._data[a]
        }
    }

    function bb(a, b) {
        db.duration.fn["as" + a] = function() {
            return +this / b
        }
    }

    function cb(a) {
        var b = !1,
            c = db;
        "undefined" == typeof ender && (a ? (gb.moment = function() {
            return !b && console && console.warn && (b = !0, console.warn("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.")), c.apply(null, arguments)
        }, h(gb.moment, c)) : gb.moment = db)
    }
    for (var db, eb, fb = "2.5.1", gb = this, hb = Math.round, ib = 0, jb = 1, kb = 2, lb = 3, mb = 4, nb = 5, ob = 6, pb = {}, qb = {
            _isAMomentObject: null,
            _i: null,
            _f: null,
            _l: null,
            _strict: null,
            _isUTC: null,
            _offset: null,
            _pf: null,
            _lang: null
        }, rb = "undefined" != typeof module && module.exports && "undefined" != typeof require, sb = /^\/?Date\((\-?\d+)/i, tb = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, ub = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, vb = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g, wb = /(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g, xb = /\d\d?/, yb = /\d{1,3}/, zb = /\d{1,4}/, Ab = /[+\-]?\d{1,6}/, Bb = /\d+/, Cb = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, Db = /Z|[\+\-]\d\d:?\d\d/gi, Eb = /T/i, Fb = /[\+\-]?\d+(\.\d{1,3})?/, Gb = /\d/, Hb = /\d\d/, Ib = /\d{3}/, Jb = /\d{4}/, Kb = /[+-]?\d{6}/, Lb = /[+-]?\d+/, Mb = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/, Nb = "YYYY-MM-DDTHH:mm:ssZ", Ob = [
            ["YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/],
            ["YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/],
            ["GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/],
            ["GGGG-[W]WW", /\d{4}-W\d{2}/],
            ["YYYY-DDD", /\d{4}-\d{3}/]
        ], Pb = [
            ["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d{1,3}/],
            ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/],
            ["HH:mm", /(T| )\d\d:\d\d/],
            ["HH", /(T| )\d\d/]
        ], Qb = /([\+\-]|\d\d)/gi, Rb = "Date|Hours|Minutes|Seconds|Milliseconds".split("|"), Sb = {
            Milliseconds: 1,
            Seconds: 1e3,
            Minutes: 6e4,
            Hours: 36e5,
            Days: 864e5,
            Months: 2592e6,
            Years: 31536e6
        }, Tb = {
            ms: "millisecond",
            s: "second",
            m: "minute",
            h: "hour",
            d: "day",
            D: "date",
            w: "week",
            W: "isoWeek",
            M: "month",
            y: "year",
            DDD: "dayOfYear",
            e: "weekday",
            E: "isoWeekday",
            gg: "weekYear",
            GG: "isoWeekYear"
        }, Ub = {
            dayofyear: "dayOfYear",
            isoweekday: "isoWeekday",
            isoweek: "isoWeek",
            weekyear: "weekYear",
            isoweekyear: "isoWeekYear"
        }, Vb = {}, Wb = "DDD w W M D d".split(" "), Xb = "M D H h m s w W".split(" "), Yb = {
            M: function() {
                return this.month() + 1
            },
            MMM: function(a) {
                return this.lang().monthsShort(this, a)
            },
            MMMM: function(a) {
                return this.lang().months(this, a)
            },
            D: function() {
                return this.date()
            },
            DDD: function() {
                return this.dayOfYear()
            },
            d: function() {
                return this.day()
            },
            dd: function(a) {
                return this.lang().weekdaysMin(this, a)
            },
            ddd: function(a) {
                return this.lang().weekdaysShort(this, a)
            },
            dddd: function(a) {
                return this.lang().weekdays(this, a)
            },
            w: function() {
                return this.week()
            },
            W: function() {
                return this.isoWeek()
            },
            YY: function() {
                return k(this.year() % 100, 2)
            },
            YYYY: function() {
                return k(this.year(), 4)
            },
            YYYYY: function() {
                return k(this.year(), 5)
            },
            YYYYYY: function() {
                var a = this.year(),
                    b = a >= 0 ? "+" : "-";
                return b + k(Math.abs(a), 6)
            },
            gg: function() {
                return k(this.weekYear() % 100, 2)
            },
            gggg: function() {
                return k(this.weekYear(), 4)
            },
            ggggg: function() {
                return k(this.weekYear(), 5)
            },
            GG: function() {
                return k(this.isoWeekYear() % 100, 2)
            },
            GGGG: function() {
                return k(this.isoWeekYear(), 4)
            },
            GGGGG: function() {
                return k(this.isoWeekYear(), 5)
            },
            e: function() {
                return this.weekday()
            },
            E: function() {
                return this.isoWeekday()
            },
            a: function() {
                return this.lang().meridiem(this.hours(), this.minutes(), !0)
            },
            A: function() {
                return this.lang().meridiem(this.hours(), this.minutes(), !1)
            },
            H: function() {
                return this.hours()
            },
            h: function() {
                return this.hours() % 12 || 12
            },
            m: function() {
                return this.minutes()
            },
            s: function() {
                return this.seconds()
            },
            S: function() {
                return s(this.milliseconds() / 100)
            },
            SS: function() {
                return k(s(this.milliseconds() / 10), 2)
            },
            SSS: function() {
                return k(this.milliseconds(), 3)
            },
            SSSS: function() {
                return k(this.milliseconds(), 3)
            },
            Z: function() {
                var a = -this.zone(),
                    b = "+";
                return 0 > a && (a = -a, b = "-"), b + k(s(a / 60), 2) + ":" + k(s(a) % 60, 2)
            },
            ZZ: function() {
                var a = -this.zone(),
                    b = "+";
                return 0 > a && (a = -a, b = "-"), b + k(s(a / 60), 2) + k(s(a) % 60, 2)
            },
            z: function() {
                return this.zoneAbbr()
            },
            zz: function() {
                return this.zoneName()
            },
            X: function() {
                return this.unix()
            },
            Q: function() {
                return this.quarter()
            }
        }, Zb = ["months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin"]; Wb.length;) eb = Wb.pop(), Yb[eb + "o"] = d(Yb[eb], eb);
    for (; Xb.length;) eb = Xb.pop(), Yb[eb + eb] = c(Yb[eb], 2);
    for (Yb.DDDD = c(Yb.DDD, 3), h(e.prototype, {
            set: function(a) {
                var b, c;
                for (c in a) b = a[c], "function" == typeof b ? this[c] = b : this["_" + c] = b
            },
            _months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            months: function(a) {
                return this._months[a.month()]
            },
            _monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            monthsShort: function(a) {
                return this._monthsShort[a.month()]
            },
            monthsParse: function(a) {
                var b, c, d;
                for (this._monthsParse || (this._monthsParse = []), b = 0; 12 > b; b++)
                    if (this._monthsParse[b] || (c = db.utc([2e3, b]), d = "^" + this.months(c, "") + "|^" + this.monthsShort(c, ""), this._monthsParse[b] = new RegExp(d.replace(".", ""), "i")), this._monthsParse[b].test(a)) return b
            },
            _weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            weekdays: function(a) {
                return this._weekdays[a.day()]
            },
            _weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            weekdaysShort: function(a) {
                return this._weekdaysShort[a.day()]
            },
            _weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
            weekdaysMin: function(a) {
                return this._weekdaysMin[a.day()]
            },
            weekdaysParse: function(a) {
                var b, c, d;
                for (this._weekdaysParse || (this._weekdaysParse = []), b = 0; 7 > b; b++)
                    if (this._weekdaysParse[b] || (c = db([2e3, 1]).day(b), d = "^" + this.weekdays(c, "") + "|^" + this.weekdaysShort(c, "") + "|^" + this.weekdaysMin(c, ""), this._weekdaysParse[b] = new RegExp(d.replace(".", ""), "i")), this._weekdaysParse[b].test(a)) return b
            },
            _longDateFormat: {
                LT: "h:mm A",
                L: "MM/DD/YYYY",
                LL: "MMMM D YYYY",
                LLL: "MMMM D YYYY LT",
                LLLL: "dddd, MMMM D YYYY LT"
            },
            longDateFormat: function(a) {
                var b = this._longDateFormat[a];
                return !b && this._longDateFormat[a.toUpperCase()] && (b = this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(a) {
                    return a.slice(1)
                }), this._longDateFormat[a] = b), b
            },
            isPM: function(a) {
                return "p" === (a + "").toLowerCase().charAt(0)
            },
            _meridiemParse: /[ap]\.?m?\.?/i,
            meridiem: function(a, b, c) {
                return a > 11 ? c ? "pm" : "PM" : c ? "am" : "AM"
            },
            _calendar: {
                sameDay: "[Today at] LT",
                nextDay: "[Tomorrow at] LT",
                nextWeek: "dddd [at] LT",
                lastDay: "[Yesterday at] LT",
                lastWeek: "[Last] dddd [at] LT",
                sameElse: "L"
            },
            calendar: function(a, b) {
                var c = this._calendar[a];
                return "function" == typeof c ? c.apply(b) : c
            },
            _relativeTime: {
                future: "in %s",
                past: "%s ago",
                s: "a few seconds",
                m: "a minute",
                mm: "%d minutes",
                h: "an hour",
                hh: "%d hours",
                d: "a day",
                dd: "%d days",
                M: "a month",
                MM: "%d months",
                y: "a year",
                yy: "%d years"
            },
            relativeTime: function(a, b, c, d) {
                var e = this._relativeTime[c];
                return "function" == typeof e ? e(a, b, c, d) : e.replace(/%d/i, a)
            },
            pastFuture: function(a, b) {
                var c = this._relativeTime[a > 0 ? "future" : "past"];
                return "function" == typeof c ? c(b) : c.replace(/%s/i, b)
            },
            ordinal: function(a) {
                return this._ordinal.replace("%d", a)
            },
            _ordinal: "%d",
            preparse: function(a) {
                return a
            },
            postformat: function(a) {
                return a
            },
            week: function(a) {
                return Y(a, this._week.dow, this._week.doy).week
            },
            _week: {
                dow: 0,
                doy: 6
            },
            _invalidDate: "Invalid date",
            invalidDate: function() {
                return this._invalidDate
            }
        }), db = function(c, d, e, f) {
            var g;
            return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, g._i = c, g._f = d, g._l = e, g._strict = f, g._isUTC = !1, g._pf = b(), $(g)
        }, db.utc = function(c, d, e, f) {
            var g;
            return "boolean" == typeof e && (f = e, e = a), g = {}, g._isAMomentObject = !0, g._useUTC = !0, g._isUTC = !0, g._l = e, g._i = c, g._f = d, g._strict = f, g._pf = b(), $(g).utc()
        }, db.unix = function(a) {
            return db(1e3 * a)
        }, db.duration = function(a, b) {
            var c, d, e, f = a,
                h = null;
            return db.isDuration(a) ? f = {
                ms: a._milliseconds,
                d: a._days,
                M: a._months
            } : "number" == typeof a ? (f = {}, b ? f[b] = a : f.milliseconds = a) : (h = tb.exec(a)) ? (c = "-" === h[1] ? -1 : 1, f = {
                y: 0,
                d: s(h[kb]) * c,
                h: s(h[lb]) * c,
                m: s(h[mb]) * c,
                s: s(h[nb]) * c,
                ms: s(h[ob]) * c
            }) : (h = ub.exec(a)) && (c = "-" === h[1] ? -1 : 1, e = function(a) {
                var b = a && parseFloat(a.replace(",", "."));
                return (isNaN(b) ? 0 : b) * c
            }, f = {
                y: e(h[2]),
                M: e(h[3]),
                d: e(h[4]),
                h: e(h[5]),
                m: e(h[6]),
                s: e(h[7]),
                w: e(h[8])
            }), d = new g(f), db.isDuration(a) && a.hasOwnProperty("_lang") && (d._lang = a._lang), d
        }, db.version = fb, db.defaultFormat = Nb, db.updateOffset = function() {}, db.lang = function(a, b) {
            var c;
            return a ? (b ? A(y(a), b) : null === b ? (B(a), a = "en") : pb[a] || C(a), c = db.duration.fn._lang = db.fn._lang = C(a), c._abbr) : db.fn._lang._abbr
        }, db.langData = function(a) {
            return a && a._lang && a._lang._abbr && (a = a._lang._abbr), C(a)
        }, db.isMoment = function(a) {
            return a instanceof f || null != a && a.hasOwnProperty("_isAMomentObject")
        }, db.isDuration = function(a) {
            return a instanceof g
        }, eb = Zb.length - 1; eb >= 0; --eb) r(Zb[eb]);
    for (db.normalizeUnits = function(a) {
            return p(a)
        }, db.invalid = function(a) {
            var b = db.utc(0 / 0);
            return null != a ? h(b._pf, a) : b._pf.userInvalidated = !0, b
        }, db.parseZone = function(a) {
            return db(a).parseZone()
        }, h(db.fn = f.prototype, {
            clone: function() {
                return db(this)
            },
            valueOf: function() {
                return +this._d + 6e4 * (this._offset || 0)
            },
            unix: function() {
                return Math.floor(+this / 1e3)
            },
            toString: function() {
                return this.clone().lang("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
            },
            toDate: function() {
                return this._offset ? new Date(+this) : this._d
            },
            toISOString: function() {
                var a = db(this).utc();
                return 0 < a.year() && a.year() <= 9999 ? F(a, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : F(a, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
            },
            toArray: function() {
                var a = this;
                return [a.year(), a.month(), a.date(), a.hours(), a.minutes(), a.seconds(), a.milliseconds()]
            },
            isValid: function() {
                return x(this)
            },
            isDSTShifted: function() {
                return this._a ? this.isValid() && o(this._a, (this._isUTC ? db.utc(this._a) : db(this._a)).toArray()) > 0 : !1
            },
            parsingFlags: function() {
                return h({}, this._pf)
            },
            invalidAt: function() {
                return this._pf.overflow
            },
            utc: function() {
                return this.zone(0)
            },
            local: function() {
                return this.zone(0), this._isUTC = !1, this
            },
            format: function(a) {
                var b = F(this, a || db.defaultFormat);
                return this.lang().postformat(b)
            },
            add: function(a, b) {
                var c;
                return c = "string" == typeof a ? db.duration(+b, a) : db.duration(a, b), l(this, c, 1), this
            },
            subtract: function(a, b) {
                var c;
                return c = "string" == typeof a ? db.duration(+b, a) : db.duration(a, b), l(this, c, -1), this
            },
            diff: function(a, b, c) {
                var d, e, f = z(a, this),
                    g = 6e4 * (this.zone() - f.zone());
                return b = p(b), "year" === b || "month" === b ? (d = 432e5 * (this.daysInMonth() + f.daysInMonth()), e = 12 * (this.year() - f.year()) + (this.month() - f.month()), e += (this - db(this).startOf("month") - (f - db(f).startOf("month"))) / d, e -= 6e4 * (this.zone() - db(this).startOf("month").zone() - (f.zone() - db(f).startOf("month").zone())) / d, "year" === b && (e /= 12)) : (d = this - f, e = "second" === b ? d / 1e3 : "minute" === b ? d / 6e4 : "hour" === b ? d / 36e5 : "day" === b ? (d - g) / 864e5 : "week" === b ? (d - g) / 6048e5 : d), c ? e : j(e)
            },
            from: function(a, b) {
                return db.duration(this.diff(a)).lang(this.lang()._abbr).humanize(!b)
            },
            fromNow: function(a) {
                return this.from(db(), a)
            },
            calendar: function() {
                var a = z(db(), this).startOf("day"),
                    b = this.diff(a, "days", !0),
                    c = -6 > b ? "sameElse" : -1 > b ? "lastWeek" : 0 > b ? "lastDay" : 1 > b ? "sameDay" : 2 > b ? "nextDay" : 7 > b ? "nextWeek" : "sameElse";
                return this.format(this.lang().calendar(c, this))
            },
            isLeapYear: function() {
                return v(this.year())
            },
            isDST: function() {
                return this.zone() < this.clone().month(0).zone() || this.zone() < this.clone().month(5).zone()
            },
            day: function(a) {
                var b = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
                return null != a ? (a = V(a, this.lang()), this.add({
                    d: a - b
                })) : b
            },
            month: function(a) {
                var b, c = this._isUTC ? "UTC" : "";
                return null != a ? "string" == typeof a && (a = this.lang().monthsParse(a), "number" != typeof a) ? this : (b = this.date(), this.date(1), this._d["set" + c + "Month"](a), this.date(Math.min(b, this.daysInMonth())), db.updateOffset(this), this) : this._d["get" + c + "Month"]()
            },
            startOf: function(a) {
                switch (a = p(a)) {
                    case "year":
                        this.month(0);
                    case "month":
                        this.date(1);
                    case "week":
                    case "isoWeek":
                    case "day":
                        this.hours(0);
                    case "hour":
                        this.minutes(0);
                    case "minute":
                        this.seconds(0);
                    case "second":
                        this.milliseconds(0)
                }
                return "week" === a ? this.weekday(0) : "isoWeek" === a && this.isoWeekday(1), this
            },
            endOf: function(a) {
                return a = p(a), this.startOf(a).add("isoWeek" === a ? "week" : a, 1).subtract("ms", 1)
            },
            isAfter: function(a, b) {
                return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) > +db(a).startOf(b)
            },
            isBefore: function(a, b) {
                return b = "undefined" != typeof b ? b : "millisecond", +this.clone().startOf(b) < +db(a).startOf(b)
            },
            isSame: function(a, b) {
                return b = b || "ms", +this.clone().startOf(b) === +z(a, this).startOf(b)
            },
            min: function(a) {
                return a = db.apply(null, arguments), this > a ? this : a
            },
            max: function(a) {
                return a = db.apply(null, arguments), a > this ? this : a
            },
            zone: function(a) {
                var b = this._offset || 0;
                return null == a ? this._isUTC ? b : this._d.getTimezoneOffset() : ("string" == typeof a && (a = I(a)), Math.abs(a) < 16 && (a = 60 * a), this._offset = a, this._isUTC = !0, b !== a && l(this, db.duration(b - a, "m"), 1, !0), this)
            },
            zoneAbbr: function() {
                return this._isUTC ? "UTC" : ""
            },
            zoneName: function() {
                return this._isUTC ? "Coordinated Universal Time" : ""
            },
            parseZone: function() {
                return this._tzm ? this.zone(this._tzm) : "string" == typeof this._i && this.zone(this._i), this
            },
            hasAlignedHourOffset: function(a) {
                return a = a ? db(a).zone() : 0, (this.zone() - a) % 60 === 0
            },
            daysInMonth: function() {
                return t(this.year(), this.month())
            },
            dayOfYear: function(a) {
                var b = hb((db(this).startOf("day") - db(this).startOf("year")) / 864e5) + 1;
                return null == a ? b : this.add("d", a - b)
            },
            quarter: function() {
                return Math.ceil((this.month() + 1) / 3)
            },
            weekYear: function(a) {
                var b = Y(this, this.lang()._week.dow, this.lang()._week.doy).year;
                return null == a ? b : this.add("y", a - b)
            },
            isoWeekYear: function(a) {
                var b = Y(this, 1, 4).year;
                return null == a ? b : this.add("y", a - b)
            },
            week: function(a) {
                var b = this.lang().week(this);
                return null == a ? b : this.add("d", 7 * (a - b))
            },
            isoWeek: function(a) {
                var b = Y(this, 1, 4).week;
                return null == a ? b : this.add("d", 7 * (a - b))
            },
            weekday: function(a) {
                var b = (this.day() + 7 - this.lang()._week.dow) % 7;
                return null == a ? b : this.add("d", a - b)
            },
            isoWeekday: function(a) {
                return null == a ? this.day() || 7 : this.day(this.day() % 7 ? a : a - 7)
            },
            get: function(a) {
                return a = p(a), this[a]()
            },
            set: function(a, b) {
                return a = p(a), "function" == typeof this[a] && this[a](b), this
            },
            lang: function(b) {
                return b === a ? this._lang : (this._lang = C(b), this)
            }
        }), eb = 0; eb < Rb.length; eb++) _(Rb[eb].toLowerCase().replace(/s$/, ""), Rb[eb]);
    _("year", "FullYear"), db.fn.days = db.fn.day, db.fn.months = db.fn.month, db.fn.weeks = db.fn.week, db.fn.isoWeeks = db.fn.isoWeek, db.fn.toJSON = db.fn.toISOString, h(db.duration.fn = g.prototype, {
        _bubble: function() {
            var a, b, c, d, e = this._milliseconds,
                f = this._days,
                g = this._months,
                h = this._data;
            h.milliseconds = e % 1e3, a = j(e / 1e3), h.seconds = a % 60, b = j(a / 60), h.minutes = b % 60, c = j(b / 60), h.hours = c % 24, f += j(c / 24), h.days = f % 30, g += j(f / 30), h.months = g % 12, d = j(g / 12), h.years = d
        },
        weeks: function() {
            return j(this.days() / 7)
        },
        valueOf: function() {
            return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * s(this._months / 12)
        },
        humanize: function(a) {
            var b = +this,
                c = X(b, !a, this.lang());
            return a && (c = this.lang().pastFuture(b, c)), this.lang().postformat(c)
        },
        add: function(a, b) {
            var c = db.duration(a, b);
            return this._milliseconds += c._milliseconds, this._days += c._days, this._months += c._months, this._bubble(), this
        },
        subtract: function(a, b) {
            var c = db.duration(a, b);
            return this._milliseconds -= c._milliseconds, this._days -= c._days, this._months -= c._months, this._bubble(), this
        },
        get: function(a) {
            return a = p(a), this[a.toLowerCase() + "s"]()
        },
        as: function(a) {
            return a = p(a), this["as" + a.charAt(0).toUpperCase() + a.slice(1) + "s"]()
        },
        lang: db.fn.lang,
        toIsoString: function() {
            var a = Math.abs(this.years()),
                b = Math.abs(this.months()),
                c = Math.abs(this.days()),
                d = Math.abs(this.hours()),
                e = Math.abs(this.minutes()),
                f = Math.abs(this.seconds() + this.milliseconds() / 1e3);
            return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (a ? a + "Y" : "") + (b ? b + "M" : "") + (c ? c + "D" : "") + (d || e || f ? "T" : "") + (d ? d + "H" : "") + (e ? e + "M" : "") + (f ? f + "S" : "") : "P0D"
        }
    });
    for (eb in Sb) Sb.hasOwnProperty(eb) && (bb(eb, Sb[eb]), ab(eb.toLowerCase()));
    bb("Weeks", 6048e5), db.duration.fn.asMonths = function() {
        return (+this - 31536e6 * this.years()) / 2592e6 + 12 * this.years()
    }, db.lang("en", {
        ordinal: function(a) {
            var b = a % 10,
                c = 1 === s(a % 100 / 10) ? "th" : 1 === b ? "st" : 2 === b ? "nd" : 3 === b ? "rd" : "th";
            return a + c
        }
    }), rb ? (module.exports = db, cb(!0)) : "function" == typeof define && define.amd ? define("moment", function(b, c, d) {
        return d.config && d.config() && d.config().noGlobal !== !0 && cb(d.config().noGlobal === a), db
    }) : cb()
}).call(this);
/*!
 * Lazy Load - jQuery plugin for lazy loading images
 *
 * Copyright (c) 2007-2015 Mika Tuupola
 *
 * Licensed under the MIT license:
 *   http://www.opensource.org/licenses/mit-license.php
 *
 * Project home:
 *   http://www.appelsiini.net/projects/lazyload
 *
 * Version:  1.9.5
 *
 */
(function($, window, document, undefined) {
    var $window = $(window);
    $.fn.lazyload = function(options) {
        var elements = this;
        var $container;
        var settings = {
            threshold: 0,
            failure_limit: 0,
            event: "scroll",
            effect: "show",
            container: window,
            data_attribute: "original",
            skip_invisible: false,
            appear: null,
            load: null,
            placeholder: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAANSURBVBhXYzh8+PB/AAffA0nNPuCLAAAAAElFTkSuQmCC"
        };

        function update() {
            var counter = 0;
            elements.each(function() {
                var $this = $(this);
                if (settings.skip_invisible && !$this.is(":visible")) {
                    return;
                }
                if ($.abovethetop(this, settings) || $.leftofbegin(this, settings)) {} else if (!$.belowthefold(this, settings) && !$.rightoffold(this, settings)) {
                    $this.trigger("appear");
                    counter = 0;
                } else {
                    if (++counter > settings.failure_limit) {
                        return false;
                    }
                }
            });
        }
        if (options) {
            if (undefined !== options.failurelimit) {
                options.failure_limit = options.failurelimit;
                delete options.failurelimit;
            }
            if (undefined !== options.effectspeed) {
                options.effect_speed = options.effectspeed;
                delete options.effectspeed;
            }
            $.extend(settings, options);
        }
        $container = (settings.container === undefined || settings.container === window) ? $window : $(settings.container);
        if (0 === settings.event.indexOf("scroll")) {
            $container.bind(settings.event, function() {
                return update();
            });
        }
        this.each(function() {
            var self = this;
            var $self = $(self);
            self.loaded = false;
            if ($self.attr("src") === undefined || $self.attr("src") === false) {
                if ($self.is("img")) {
                    $self.attr("src", settings.placeholder);
                }
            }
            $self.one("appear", function() {
                if (!this.loaded) {
                    if (settings.appear) {
                        var elements_left = elements.length;
                        settings.appear.call(self, elements_left, settings);
                    }
                    $("<img />").bind("load", function() {
                        var original = $self.attr("data-" + settings.data_attribute);
                        $self.hide();
                        if ($self.is("img")) {
                            $self.attr("src", original);
                        } else {
                            $self.css("background-image", "url('" + original + "')");
                        }
                        $self[settings.effect](settings.effect_speed);
                        self.loaded = true;
                        var temp = $.grep(elements, function(element) {
                            return !element.loaded;
                        });
                        elements = $(temp);
                        if (settings.load) {
                            var elements_left = elements.length;
                            settings.load.call(self, elements_left, settings);
                        }
                    }).attr("src", $self.attr("data-" + settings.data_attribute));
                }
            });
            if (0 !== settings.event.indexOf("scroll")) {
                $self.bind(settings.event, function() {
                    if (!self.loaded) {
                        $self.trigger("appear");
                    }
                });
            }
        });
        $window.bind("resize", function() {
            update();
        });
        if ((/(?:iphone|ipod|ipad).*os 5/gi).test(navigator.appVersion)) {
            $window.bind("pageshow", function(event) {
                if (event.originalEvent && event.originalEvent.persisted) {
                    elements.each(function() {
                        $(this).trigger("appear");
                    });
                }
            });
        }
        $(document).ready(function() {
            update();
        });
        return this;
    };
    $.belowthefold = function(element, settings) {
        var fold;
        if (settings.container === undefined || settings.container === window) {
            fold = (window.innerHeight ? window.innerHeight : $window.height()) + $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top + $(settings.container).height();
        }
        return fold <= $(element).offset().top - settings.threshold;
    };
    $.rightoffold = function(element, settings) {
        var fold;
        if (settings.container === undefined || settings.container === window) {
            fold = $window.width() + $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left + $(settings.container).width();
        }
        return fold <= $(element).offset().left - settings.threshold;
    };
    $.abovethetop = function(element, settings) {
        var fold;
        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollTop();
        } else {
            fold = $(settings.container).offset().top;
        }
        return fold >= $(element).offset().top + settings.threshold + $(element).height();
    };
    $.leftofbegin = function(element, settings) {
        var fold;
        if (settings.container === undefined || settings.container === window) {
            fold = $window.scrollLeft();
        } else {
            fold = $(settings.container).offset().left;
        }
        return fold >= $(element).offset().left + settings.threshold + $(element).width();
    };
    $.inviewport = function(element, settings) {
        return !$.rightoffold(element, settings) && !$.leftofbegin(element, settings) && !$.belowthefold(element, settings) && !$.abovethetop(element, settings);
    };
    $.extend($.expr[":"], {
        "below-the-fold": function(a) {
            return $.belowthefold(a, {
                threshold: 0
            });
        },
        "above-the-top": function(a) {
            return !$.belowthefold(a, {
                threshold: 0
            });
        },
        "right-of-screen": function(a) {
            return $.rightoffold(a, {
                threshold: 0
            });
        },
        "left-of-screen": function(a) {
            return !$.rightoffold(a, {
                threshold: 0
            });
        },
        "in-viewport": function(a) {
            return $.inviewport(a, {
                threshold: 0
            });
        },
        "above-the-fold": function(a) {
            return !$.belowthefold(a, {
                threshold: 0
            });
        },
        "right-of-fold": function(a) {
            return $.rightoffold(a, {
                threshold: 0
            });
        },
        "left-of-fold": function(a) {
            return !$.rightoffold(a, {
                threshold: 0
            });
        }
    });
})(jQuery, window, document);
/*! lazysizes - v1.3.2 */
! function(a, b) {
    var c = b(a, a.document);
    a.lazySizes = c, "object" == typeof module && module.exports ? module.exports = c : "function" == typeof define && define.amd && define(c)
}(window, function(a, b) {
    "use strict";
    if (b.getElementsByClassName) {
        var c, d = b.documentElement,
            e = a.HTMLPictureElement && "sizes" in b.createElement("img"),
            f = "addEventListener",
            g = a[f],
            h = a.setTimeout,
            i = a.requestAnimationFrame || h,
            j = /^picture$/i,
            k = ["load", "error", "lazyincluded", "_lazyloaded"],
            l = {},
            m = Array.prototype.forEach,
            n = function(a, b) {
                return l[b] || (l[b] = new RegExp("(\\s|^)" + b + "(\\s|$)")), l[b].test(a.className) && l[b]
            },
            o = function(a, b) {
                n(a, b) || (a.className = a.className.trim() + " " + b)
            },
            p = function(a, b) {
                var c;
                (c = n(a, b)) && (a.className = a.className.replace(c, " "))
            },
            q = function(a, b, c) {
                var d = c ? f : "removeEventListener";
                c && q(a, b), k.forEach(function(c) {
                    a[d](c, b)
                })
            },
            r = function(a, c, d, e, f) {
                var g = b.createEvent("CustomEvent");
                return g.initCustomEvent(c, !e, !f, d || {}), a.dispatchEvent(g), g
            },
            s = function(b, d) {
                var f;
                !e && (f = a.picturefill || c.pf) ? f({
                    reevaluate: !0,
                    elements: [b]
                }) : d && d.src && (b.src = d.src)
            },
            t = function(a, b) {
                return (getComputedStyle(a, null) || {})[b]
            },
            u = function(a, b, d) {
                for (d = d || a.offsetWidth; d < c.minSize && b && !a._lazysizesWidth;) d = b.offsetWidth, b = b.parentNode;
                return d
            },
            v = function(b) {
                var c, d = 0,
                    e = a.Date,
                    f = function() {
                        c = !1, d = e.now(), b()
                    },
                    g = function() {
                        h(f)
                    },
                    j = function() {
                        i(g)
                    };
                return function() {
                    if (!c) {
                        var a = 125 - (e.now() - d);
                        c = !0, 6 > a && (a = 6), h(j, a)
                    }
                }
            },
            w = function() {
                var e, k, l, u, w, y, z, A, B, C, D, E, F, G, H, I = /^img$/i,
                    J = /^iframe$/i,
                    K = "onscroll" in a && !/glebot/.test(navigator.userAgent),
                    L = 0,
                    M = 0,
                    N = 0,
                    O = 0,
                    P = function(a) {
                        N--, a && a.target && q(a.target, P), (!a || 0 > N || !a.target) && (N = 0)
                    },
                    Q = function(a, b) {
                        var c, d = a,
                            e = "hidden" != t(a, "visibility");
                        for (B -= b, E += b, C -= b, D += b; e && (d = d.offsetParent);) e = (t(d, "opacity") || 1) > 0, e && "visible" != t(d, "overflow") && (c = d.getBoundingClientRect(), e = D > c.left && C < c.right && E > c.top - 1 && B < c.bottom + 1);
                        return e
                    },
                    R = function() {
                        var a, b, d, f, g, h, i, j, m;
                        if ((w = c.loadMode) && 8 > N && (a = e.length)) {
                            for (b = 0, O++, G > M && 1 > N && O > 3 && w > 2 ? (M = G, O = 0) : M = w > 1 && O > 2 && 6 > N ? F : L; a > b; b++)
                                if (e[b] && !e[b]._lazyRace)
                                    if (K)
                                        if ((j = e[b].getAttribute("data-expand")) && (h = 1 * j) || (h = M), m !== h && (z = innerWidth + h * H, A = innerHeight + h, i = -1 * h, m = h), d = e[b].getBoundingClientRect(), (E = d.bottom) >= i && (B = d.top) <= A && (D = d.right) >= i * H && (C = d.left) <= z && (E || D || C || B) && (l && 3 > N && !j && (3 > w || 4 > O) || Q(e[b], h))) {
                                            if (X(e[b]), g = !0, N > 9) break
                                        } else !g && l && !f && 4 > N && 4 > O && w > 2 && (k[0] || c.preloadAfterLoad) && (k[0] || !j && (E || D || C || B || "auto" != e[b].getAttribute(c.sizesAttr))) && (f = k[0] || e[b]);
                            else X(e[b]);
                            f && !g && X(f)
                        }
                    },
                    S = v(R),
                    T = function(a) {
                        o(a.target, c.loadedClass), p(a.target, c.loadingClass), q(a.target, T)
                    },
                    U = function(a, b) {
                        try {
                            a.contentWindow.location.replace(b)
                        } catch (c) {
                            a.src = b
                        }
                    },
                    V = function(a) {
                        var b, d, e = a.getAttribute(c.srcsetAttr);
                        (b = c.customMedia[a.getAttribute("data-media") || a.getAttribute("media")]) && a.setAttribute("media", b), e && a.setAttribute("srcset", e), b && (d = a.parentNode, d.insertBefore(a.cloneNode(), a), d.removeChild(a))
                    },
                    W = function() {
                        var a, b = [],
                            c = function() {
                                for (; b.length;) b.shift()();
                                a = !1
                            };
                        return function(d) {
                            b.push(d), a || (a = !0, i(c))
                        }
                    }(),
                    X = function(a) {
                        var b, d, e, f, g, i, k, t = I.test(a.nodeName),
                            v = t && (a.getAttribute(c.sizesAttr) || a.getAttribute("sizes")),
                            w = "auto" == v;
                        (!w && l || !t || !a.src && !a.srcset || a.complete || n(a, c.errorClass)) && (w && (k = a.offsetWidth), a._lazyRace = !0, N++, W(function() {
                            a._lazyRace && delete a._lazyRace, p(a, c.lazyClass), (g = r(a, "lazybeforeunveil")).defaultPrevented || (v && (w ? (o(a, c.autosizesClass), x.updateElem(a, !0, k)) : a.setAttribute("sizes", v)), d = a.getAttribute(c.srcsetAttr), b = a.getAttribute(c.srcAttr), t && (e = a.parentNode, f = e && j.test(e.nodeName || "")), i = g.detail.firesLoad || "src" in a && (d || b || f), g = {
                                target: a
                            }, i && (q(a, P, !0), clearTimeout(u), u = h(P, 2500), o(a, c.loadingClass), q(a, T, !0)), f && m.call(e.getElementsByTagName("source"), V), d ? a.setAttribute("srcset", d) : b && !f && (J.test(a.nodeName) ? U(a, b) : a.src = b), (d || f) && s(a, {
                                src: b
                            })), (!i || a.complete) && (i ? P(g) : N--, T(g))
                        }))
                    },
                    Y = function() {
                        if (!l) {
                            if (Date.now() - y < 999) return void h(Y, 999);
                            var a, b = function() {
                                c.loadMode = 3, S()
                            };
                            l = !0, c.loadMode = 3, N || (O ? S() : h(R)), g("scroll", function() {
                                3 == c.loadMode && (c.loadMode = 2), clearTimeout(a), a = h(b, 99)
                            }, !0)
                        }
                    };
                return {
                    _: function() {
                        y = Date.now(), e = b.getElementsByClassName(c.lazyClass), k = b.getElementsByClassName(c.lazyClass + " " + c.preloadClass), H = c.hFac, F = c.expand, G = F * c.expFactor, g("scroll", S, !0), g("resize", S, !0), a.MutationObserver ? new MutationObserver(S).observe(d, {
                            childList: !0,
                            subtree: !0,
                            attributes: !0
                        }) : (d[f]("DOMNodeInserted", S, !0), d[f]("DOMAttrModified", S, !0), setInterval(S, 999)), g("hashchange", S, !0), ["focus", "mouseover", "click", "load", "transitionend", "animationend", "webkitAnimationEnd"].forEach(function(a) {
                            b[f](a, S, !0)
                        }), /d$|^c/.test(b.readyState) ? Y() : (g("load", Y), b[f]("DOMContentLoaded", S), h(Y, 2e4)), S(e.length > 0)
                    },
                    checkElems: S,
                    unveil: X
                }
            }(),
            x = function() {
                var a, d = function(a, b, c) {
                        var d, e, f, g, h = a.parentNode;
                        if (h && (c = u(a, h, c), g = r(a, "lazybeforesizes", {
                                width: c,
                                dataAttr: !!b
                            }), !g.defaultPrevented && (c = g.detail.width, c && c !== a._lazysizesWidth))) {
                            if (a._lazysizesWidth = c, c += "px", a.setAttribute("sizes", c), j.test(h.nodeName || ""))
                                for (d = h.getElementsByTagName("source"), e = 0, f = d.length; f > e; e++) d[e].setAttribute("sizes", c);
                            g.detail.dataAttr || s(a, g.detail)
                        }
                    },
                    e = function() {
                        var b, c = a.length;
                        if (c)
                            for (b = 0; c > b; b++) d(a[b])
                    },
                    f = v(e);
                return {
                    _: function() {
                        a = b.getElementsByClassName(c.autosizesClass), g("resize", f)
                    },
                    checkElems: f,
                    updateElem: d
                }
            }(),
            y = function() {
                y.i || (y.i = !0, x._(), w._())
            };
        return function() {
            var b, e = {
                lazyClass: "lazyload",
                loadedClass: "lazyloaded",
                loadingClass: "lazyloading",
                preloadClass: "lazypreload",
                errorClass: "lazyerror",
                autosizesClass: "lazyautosizes",
                srcAttr: "data-src",
                srcsetAttr: "data-srcset",
                sizesAttr: "data-sizes",
                minSize: 40,
                customMedia: {},
                init: !0,
                expFactor: 1.7,
                hFac: .8,
                expand: d.clientHeight > 600 ? d.clientWidth > 860 ? 500 : 410 : 359,
                loadMode: 2
            };
            c = a.lazySizesConfig || a.lazysizesConfig || {};
            for (b in e) b in c || (c[b] = e[b]);
            a.lazySizesConfig = c, h(function() {
                c.init && y()
            })
        }(), {
            cfg: c,
            autoSizer: x,
            loader: w,
            init: y,
            uP: s,
            aC: o,
            rC: p,
            hC: n,
            fire: r,
            gW: u
        }
    }
});