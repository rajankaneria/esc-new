$(function(){
	$("#loginBtn").on("click",function(){
		var email = $("#email").val();
		var password = $("#password").val();
		changeLoginBtnText("PROCESSING...");
		$.getJSON("admin/login",{email:email,password:password},function(data){
			if(data.status=="success")
			{
				changeLoginBtnText("REDIRECTING...");
				window.location.reload();
			}
			else {
				changeLoginBtnText("LOGIN");
				console.log("please check your login details");
			}
		});
	});
});
function changeLoginBtnText(newString)
{
	$("#loginBtnTxt").html(newString);
}