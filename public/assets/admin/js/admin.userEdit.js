$(function () {
    var userId = $("#userid").val();
    $(".cancelUserEdit").on("click", function () {
        window.location.href = siteUrl + '/admin/users';
    });

    $("#user-form").validate({
        submitHandler: function (form) {
            $.ajaxSetup({
                header: $('meta[name="_token"]').attr('content')
            });
            $.ajax({
                type: "POST",
                url: siteUrl + '/admin/user/' + userId,
                data: $(form).serialize(),
                dataType: 'json',
                success: function (responseData) {
                    if (responseData.status == "success")
                    {
                        window.location.href = siteUrl + '/admin/users';
                    } else {
                        console.log("please check your login details");
                    }
                },
                error: function (errordata) {
                        
                }
            })
        }
    });
    
    
    $("#account-form").validate({
        submitHandler: function (form) {
            $.ajaxSetup({
                header: $('meta[name="_token"]').attr('content')
            });
            $.ajax({
                type: "POST",
                url: siteUrl + '/admin/account/' + userId,
                data: $(form).serialize(),
                dataType: 'json',
                success: function (responseData) {
                    if (responseData.status == "success")
                    {
                        window.location.href = siteUrl + '/admin/users';
                    } else {
                        console.log("please check your login details");
                    }
                },
                error: function (errordata) {
                        
                }
            })
        }
    });
    
    $("#setting-form").validate({
        submitHandler: function (form) {
            $.ajaxSetup({
                header: $('meta[name="_token"]').attr('content')
            });
            $.ajax({
                type: "POST",
                url: siteUrl + '/admin/settings',
                data: $(form).serialize(),
                dataType: 'json',
                success: function (responseData) {
                    if (responseData.status == "success")
                    {
                        window.location.href = siteUrl + '/admin/settings';
                    } else {
                        console.log("please check your login details");
                    }
                },
                error: function (errordata) {
                        
                }
            })
        }
    });
    
});
