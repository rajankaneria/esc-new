$(function () {
    $(".cancelGameCreate").on("click", function () {
        window.location.href = siteUrl + '/admin';
    });

    $("#game-form").validate({
        // Specify the validation rules
        rules: {
            gameName: {
                required: true
            },
            gameAmount: {
                required: true
            },
        },
        // Specify the validation error messages
        messages: {
            gameName: "Please select game.",
            gameAmount: "Please enter game amount.",
        },
        submitHandler: function (form) {
            $.ajaxSetup({
                header: $('meta[name="_token"]').attr('content')
            });
            $.ajax({
                type: "POST",
                url: 'gameCreate',
                data: $(form).serialize(),
                dataType: 'json',
                success: function (responseData) {
                    if (responseData.status == "success")
                    {
                        window.location.reload();
                    } else {
                        console.log("please check your login details");
                    }
                },
                error: function (errordata) {
                        
                }
            })
        }
    });
});
