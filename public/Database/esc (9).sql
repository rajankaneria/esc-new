-- phpMyAdmin SQL Dump
-- version 4.5.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 30, 2016 at 06:55 PM
-- Server version: 5.7.11
-- PHP Version: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `esc`
--

-- --------------------------------------------------------

--
-- Table structure for table `challenge`
--

CREATE TABLE `challenge` (
  `id` int(11) NOT NULL,
  `userId` int(11) DEFAULT NULL,
  `game_master_id` int(11) DEFAULT NULL,
  `region_id` int(11) DEFAULT NULL,
  `match_id` int(11) DEFAULT NULL,
  `amount` double DEFAULT NULL,
  `isAccepted` int(11) NOT NULL DEFAULT '0',
  `isSolo` enum('yes','no') NOT NULL DEFAULT 'yes',
  `challengeStatus` int(11) NOT NULL DEFAULT '1',
  `validUpto` enum('24','36','48') NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challenge`
--

INSERT INTO `challenge` (`id`, `userId`, `game_master_id`, `region_id`, `match_id`, `amount`, `isAccepted`, `isSolo`, `challengeStatus`, `validUpto`, `created_at`, `updated_at`) VALUES
(1, 4, 1, NULL, NULL, 98, 1, 'no', 3, '36', '2016-08-20 14:36:30', '2016-08-20 14:36:30'),
(3, 4, 2, NULL, NULL, 54, 1, 'no', 3, '48', '2016-08-20 16:09:50', '2016-08-20 16:09:50'),
(4, 4, 2, NULL, NULL, 123, 1, 'yes', 3, '36', '2016-08-22 21:27:54', '2016-08-22 21:27:54'),
(5, 4, 2, NULL, NULL, 456, 0, 'no', 1, '36', '2016-08-23 22:12:11', '2016-08-23 22:12:11'),
(6, 4, 2, 1, NULL, 564, 0, 'yes', 1, '24', '2016-08-29 21:26:21', '2016-08-29 21:26:21'),
(7, 4, 2, 2, NULL, 657, 0, 'no', 1, '36', '2016-08-29 21:26:39', '2016-08-29 21:26:39'),
(8, 4, 1, 2, NULL, 345, 0, 'no', 1, '48', '2016-08-29 22:06:15', '2016-08-29 22:06:15'),
(9, 4, 1, 2, NULL, 9999, 0, 'no', 1, '48', '2016-08-29 22:06:54', '2016-08-29 22:06:54'),
(10, 4, 2, NULL, NULL, 456, 0, 'no', 1, '36', '2016-08-29 22:36:13', '2016-08-29 22:36:13'),
(11, 4, 2, NULL, NULL, 456, 0, 'no', 1, '36', '2016-08-29 22:36:56', '2016-08-29 22:36:56'),
(12, 4, 2, NULL, NULL, 456, 0, 'no', 1, '36', '2016-08-29 22:41:02', '2016-08-29 22:41:02'),
(13, NULL, 2, 1, NULL, 34, 0, 'yes', 1, '36', '2016-08-29 23:10:18', '2016-08-29 23:10:18'),
(14, 4, 1, 1, NULL, 45, 0, 'no', 1, '24', '2016-08-29 23:14:01', '2016-08-29 23:14:01'),
(15, 4, 2, 1, 2, 898, 0, 'no', 1, '36', '2016-08-29 23:15:42', '2016-08-29 23:15:42'),
(16, 4, 1, 0, 2, 24, 0, 'yes', 1, '48', '2016-08-29 23:20:00', '2016-08-29 23:20:00'),
(17, 4, 2, 1, 1, 25, 0, 'yes', 1, '24', '2016-08-29 23:20:19', '2016-08-29 23:20:19'),
(18, 4, 1, 2, 2, 18909, 0, 'yes', 1, '48', '2016-08-30 00:01:45', '2016-08-30 00:01:45'),
(19, 4, 2, 1, 1, 32423, 0, 'yes', 1, '36', '2016-08-30 21:16:05', '2016-08-30 21:16:05');

-- --------------------------------------------------------

--
-- Table structure for table `challenge_players`
--

CREATE TABLE `challenge_players` (
  `id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'First entry will be creator of the challenge and second entry will be  front user id who accepts the challenge',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challenge_players`
--

INSERT INTO `challenge_players` (`id`, `challenge_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1, 4, '2016-08-20 14:36:30', '2016-08-20 14:36:30'),
(2, 1, 2, '2016-08-20 14:49:19', '2016-08-20 14:49:19'),
(5, 3, 4, '2016-08-20 16:09:50', '2016-08-20 16:09:50'),
(6, 3, 2, '2016-08-20 16:11:16', '2016-08-20 16:11:16'),
(7, 4, 4, '2016-08-22 21:27:54', '2016-08-22 21:27:54'),
(8, 5, 4, '2016-08-23 22:12:11', '2016-08-23 22:12:11'),
(9, 4, 2, '2016-08-23 22:14:02', '2016-08-23 22:14:02'),
(10, 6, 4, '2016-08-29 21:26:21', '2016-08-29 21:26:21'),
(11, 7, 4, '2016-08-29 21:26:39', '2016-08-29 21:26:39'),
(12, 8, 4, '2016-08-29 22:06:15', '2016-08-29 22:06:15'),
(13, 9, 4, '2016-08-29 22:06:54', '2016-08-29 22:06:54'),
(14, 10, 4, '2016-08-29 22:36:13', '2016-08-29 22:36:13'),
(15, 11, 4, '2016-08-29 22:36:56', '2016-08-29 22:36:56'),
(16, 12, 4, '2016-08-29 22:41:02', '2016-08-29 22:41:02'),
(17, 14, 4, '2016-08-29 23:14:01', '2016-08-29 23:14:01'),
(18, 15, 4, '2016-08-29 23:15:42', '2016-08-29 23:15:42'),
(19, 16, 4, '2016-08-29 23:20:00', '2016-08-29 23:20:00'),
(20, 17, 4, '2016-08-29 23:20:19', '2016-08-29 23:20:19'),
(21, 18, 4, '2016-08-30 00:01:45', '2016-08-30 00:01:45'),
(22, 19, 4, '2016-08-30 21:16:05', '2016-08-30 21:16:05');

-- --------------------------------------------------------

--
-- Table structure for table `challenge_status`
--

CREATE TABLE `challenge_status` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `challenge_status`
--

INSERT INTO `challenge_status` (`id`, `name`) VALUES
(1, 'Created'),
(2, 'Accepted'),
(3, 'Cancelled'),
(4, 'Completed');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `message` longtext NOT NULL,
  `isRead` int(11) NOT NULL DEFAULT '0',
  `isResult` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `challenge_id`, `message`, `isRead`, `isResult`, `created_at`, `updated_at`) VALUES
(1, 4, '3453', 1, 0, '2016-08-23 23:21:44', '2016-08-23 23:21:44'),
(2, 4, '3453', 0, 0, '2016-08-23 23:21:52', '2016-08-23 23:21:52'),
(3, 4, 'asdasdadasd', 0, 0, '2016-08-23 23:22:10', '2016-08-23 23:22:10'),
(4, 4, 'erererwer', 1, 0, '2016-08-23 23:22:19', '2016-08-23 23:22:19'),
(5, 4, 'hkhjkhj', 0, 0, '2016-08-23 23:24:11', '2016-08-23 23:24:11'),
(6, 4, 'gkkkk', 0, 0, '2016-08-23 23:24:13', '2016-08-23 23:24:13'),
(7, 4, 'Result', 0, 1, '2016-08-23 23:48:27', '2016-08-23 23:48:27'),
(8, 4, 'Result', 0, 1, '2016-08-23 23:48:27', '2016-08-23 23:48:27');

-- --------------------------------------------------------

--
-- Table structure for table `coin_transaction_history`
--

CREATE TABLE `coin_transaction_history` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `credit` double NOT NULL COMMENT 'total amount after the user has done the transaction',
  `debit` double NOT NULL COMMENT 'the amount of which user has done the transaction',
  `source_id` int(11) NOT NULL,
  `transaction_date` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `coin_transaction_history`
--

INSERT INTO `coin_transaction_history` (`id`, `user_id`, `credit`, `debit`, `source_id`, `transaction_date`) VALUES
(1, 4, 4564, 0, 1, '2016-08-23 22:09:09'),
(2, 4, 45644545345, 0, 1, '2016-08-23 22:09:27'),
(3, 4, 45644545, 0, 1, '2016-08-23 22:09:57'),
(4, 4, 1823234, 0, 7, '2016-08-23 22:11:50'),
(5, 4, 123123123, 0, 7, '2016-08-23 22:12:52'),
(6, 2, 123123000, 123, 1, '2016-08-23 22:14:02'),
(7, 4, 3444, 0, 7, '2016-08-23 22:16:39'),
(8, 4, 56756756567, 0, 7, '2016-08-23 22:20:08'),
(9, 4, 209.1, 0, 1, '2016-08-24 00:16:31'),
(10, 4, 209.1, 0, 1, '2016-08-24 00:21:52'),
(11, 4, 209.1, 0, 1, '2016-08-24 00:23:37');

-- --------------------------------------------------------

--
-- Table structure for table `country_masters`
--

CREATE TABLE `country_masters` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country_masters`
--

INSERT INTO `country_masters` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'India', '2016-08-20 11:37:00', '2016-08-20 11:37:00'),
(2, 'USA', '2016-08-20 11:37:00', '2016-08-20 11:37:00');

-- --------------------------------------------------------

--
-- Table structure for table `game_master`
--

CREATE TABLE `game_master` (
  `id` int(11) NOT NULL,
  `gameName` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_master`
--

INSERT INTO `game_master` (`id`, `gameName`, `created_at`, `updated_at`) VALUES
(1, 'Dota', '2016-08-11 21:00:00', '2016-08-11 21:00:00'),
(2, 'LOL', '2016-08-11 21:05:00', '2016-08-11 21:05:00');

-- --------------------------------------------------------

--
-- Table structure for table `game_results`
--

CREATE TABLE `game_results` (
  `id` int(11) NOT NULL,
  `challenge_id` int(11) NOT NULL,
  `winner_user_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `game_results`
--

INSERT INTO `game_results` (`id`, `challenge_id`, `winner_user_id`, `created_at`, `updated_at`) VALUES
(1, 4, 4, '2016-08-24 00:21:52', '2016-08-24 00:21:52');

-- --------------------------------------------------------

--
-- Table structure for table `global_settings`
--

CREATE TABLE `global_settings` (
  `id` int(11) NOT NULL,
  `commision` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `global_settings`
--

INSERT INTO `global_settings` (`id`, `commision`) VALUES
(1, 15);

-- --------------------------------------------------------

--
-- Table structure for table `match_master`
--

CREATE TABLE `match_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `match_master`
--

INSERT INTO `match_master` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Captains', '2016-08-29 22:51:31', '2016-08-29 22:51:31'),
(2, 'Team', '2016-08-29 22:51:31', '2016-08-29 22:51:31');

-- --------------------------------------------------------

--
-- Table structure for table `region_master`
--

CREATE TABLE `region_master` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `region_master`
--

INSERT INTO `region_master` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Gujarat', '2016-08-29 21:04:30', '2016-08-29 21:04:30'),
(2, 'Kerela', '2016-08-29 21:04:30', '2016-08-29 21:04:30');

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `id` int(11) NOT NULL,
  `from_id` int(11) DEFAULT NULL,
  `to_id` int(11) DEFAULT NULL,
  `message` text,
  `report_type_id` int(11) DEFAULT NULL,
  `challenge_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`id`, `from_id`, `to_id`, `message`, `report_type_id`, `challenge_id`, `created_at`, `updated_at`) VALUES
(1, 4, 2, 'This is abuse', 2, 3, '2016-08-30 21:45:05', '2016-08-30 21:45:05');

-- --------------------------------------------------------

--
-- Table structure for table `report_type_master`
--

CREATE TABLE `report_type_master` (
  `id` int(11) NOT NULL,
  `report_type` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `report_type_master`
--

INSERT INTO `report_type_master` (`id`, `report_type`, `created_at`, `updated_at`) VALUES
(1, 'Abuse', '2016-08-30 21:09:59', '2016-08-30 21:09:59'),
(2, 'Cheating', '2016-08-30 21:09:59', '2016-08-30 21:09:59');

-- --------------------------------------------------------

--
-- Table structure for table `source_type`
--

CREATE TABLE `source_type` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `source_type`
--

INSERT INTO `source_type` (`id`, `name`) VALUES
(1, 'Credit Card'),
(2, 'Debit Card'),
(3, 'PayPal'),
(4, 'Skrill'),
(5, 'Challenge'),
(6, 'Tournament'),
(7, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `steam_user`
--

CREATE TABLE `steam_user` (
  `id` int(11) NOT NULL,
  `steam_id` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `steam_user`
--

INSERT INTO `steam_user` (`id`, `steam_id`, `user_id`) VALUES
(1, '76561198205411648', 1),
(4, '76561198325197620', 2);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `esc_id` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '1' COMMENT '1=front user,0=admin',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `ip_address` varchar(50) NOT NULL,
  `status_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `esc_id`, `email`, `password`, `role`, `created_at`, `updated_at`, `ip_address`, `status_id`) VALUES
(1, '14707470761', 'rajan.kaneria@gmail.com', 'e60b8b2b36f4cfc760bad37fca1b3c18059f64b9', 0, '2016-08-09 12:51:16', '2016-08-09 12:51:16', '::1', 1),
(2, '14710173892', 'tannachirag1@gmail.com', '0917016d7638327bfd265b2fbd7575668cc274cd', 1, '2016-08-12 15:56:29', '2016-08-12 15:56:29', '::1', 1),
(3, '14713395893', 'rajan.kaneria@gmail.com', '61b07edadc82d8c47ab5338e3c1bb2114ef98733', 0, '2016-08-16 09:26:29', '2016-08-16 09:26:29', '::1', 1),
(4, '14713657214', 'a@a.com', 'e29d7d21133724e78712778e14cf68ffd0fda9c9', 0, '2016-08-16 16:42:01', '2016-08-16 16:42:01', '::1', 1),
(5, '14715130485', 'sameer@gmail.com', 'a0c39b2d136396082d2e604c79e9dd8c9f937a9d', 1, '2016-08-18 09:37:28', '2016-08-18 09:37:28', '::1', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_coins`
--

CREATE TABLE `user_coins` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount` double NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_coins`
--

INSERT INTO `user_coins` (`id`, `user_id`, `amount`) VALUES
(1, 4, 518.2),
(2, 2, 3444),
(3, 3, 0),
(4, 5, 56756756567),
(5, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `user_detail`
--

CREATE TABLE `user_detail` (
  `id` int(11) NOT NULL,
  `login_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `mobile_number` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `esc_id` varchar(50) NOT NULL,
  `address_1` text NOT NULL,
  `address_2` text NOT NULL,
  `pincode` double DEFAULT NULL,
  `city` varchar(50) NOT NULL,
  `state` varchar(50) NOT NULL,
  `country_id` int(11) NOT NULL,
  `status_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_detail`
--

INSERT INTO `user_detail` (`id`, `login_id`, `username`, `first_name`, `last_name`, `mobile_number`, `email`, `esc_id`, `address_1`, `address_2`, `pincode`, `city`, `state`, `country_id`, `status_id`, `created_at`, `updated_at`) VALUES
(1, 1, 'rajankaneria', '534', '', '', 'rajan.kaneria@gmail.com', '14707470761', '', '', NULL, '', '', 0, 0, '2016-08-09 12:51:16', '2016-08-09 12:51:16'),
(2, 2, 'chiragt', 'chirag', 'tanna1', '', 'tannachirag1@gmail.com', '14710173892', '', '', 456456456, '', '', 1, 1, '2016-08-12 15:56:29', '2016-08-12 15:56:29'),
(3, 3, 'ratan', 'tnnn', '324234', '324234', 'kavit_varma11@yahoo.co.in', '14713395893', '', '', 345345345, '', '', 2, 4, '2016-08-16 09:26:29', '2016-08-16 09:26:29'),
(4, 4, 'rajan1', 'Rajan', '', '', 'a@a.com', '14713657214', '', '', 0, '', '', 1, 1, '2016-08-16 16:42:01', '2016-08-16 16:42:01'),
(5, 5, '', 'Sam', 'Chiragt', 'Chiragt', 'sameer@gmail.com', '14715130485', 'test1', 'test32', 5345345345, 'rajkot', 'gujarat', 1, 2, '2016-08-18 09:37:28', '2016-08-18 09:37:28');

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

CREATE TABLE `user_status` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_status`
--

INSERT INTO `user_status` (`id`, `name`) VALUES
(1, 'Active'),
(2, 'Inactive'),
(3, 'Deleted'),
(4, 'Locked'),
(5, 'Suspended');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `challenge`
--
ALTER TABLE `challenge`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challenge_players`
--
ALTER TABLE `challenge_players`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `challenge_status`
--
ALTER TABLE `challenge_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coin_transaction_history`
--
ALTER TABLE `coin_transaction_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `country_masters`
--
ALTER TABLE `country_masters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_master`
--
ALTER TABLE `game_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game_results`
--
ALTER TABLE `game_results`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `global_settings`
--
ALTER TABLE `global_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `match_master`
--
ALTER TABLE `match_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `region_master`
--
ALTER TABLE `region_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `report_type_master`
--
ALTER TABLE `report_type_master`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `source_type`
--
ALTER TABLE `source_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `steam_user`
--
ALTER TABLE `steam_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_coins`
--
ALTER TABLE `user_coins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_detail`
--
ALTER TABLE `user_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `challenge`
--
ALTER TABLE `challenge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `challenge_players`
--
ALTER TABLE `challenge_players`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `challenge_status`
--
ALTER TABLE `challenge_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `coin_transaction_history`
--
ALTER TABLE `coin_transaction_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `country_masters`
--
ALTER TABLE `country_masters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `game_master`
--
ALTER TABLE `game_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `game_results`
--
ALTER TABLE `game_results`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `global_settings`
--
ALTER TABLE `global_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `match_master`
--
ALTER TABLE `match_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `region_master`
--
ALTER TABLE `region_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `report_type_master`
--
ALTER TABLE `report_type_master`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `source_type`
--
ALTER TABLE `source_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `steam_user`
--
ALTER TABLE `steam_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_coins`
--
ALTER TABLE `user_coins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_detail`
--
ALTER TABLE `user_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
