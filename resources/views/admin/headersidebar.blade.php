<div class="brand clearfix">
		<a href="index.html" class="logo"><img src="<?php echo url('/assets/admin'); ?>/img/logo.jpg" class="img-responsive" alt=""></a>
		<span class="menu-btn"><i class="fa fa-bars"></i></span>
		<ul class="ts-profile-nav">
			<li><a href="#">Help</a></li>
			<li><a href="{{ url('admin/settings') }}">Settings</a></li>
			<li class="ts-account">
				<a href="#"><img src="<?php echo url('/assets/admin'); ?>/img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
				<ul>
					<li><a href="#">My Account</a></li>
					<li><a href="#">Edit Account</a></li>
					<li><a href="<?php echo url('admin/logout'); ?>">Logout</a></li>
				</ul>
			</li>
		</ul>
	</div>