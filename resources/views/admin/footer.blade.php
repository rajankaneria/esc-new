<!-- Loading Scripts -->
	<script src="<?php echo url('/assets/admin'); ?>/js/bootstrap-select.min.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/bootstrap.min.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/Chart.min.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/fileinput.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/chartData.js"></script>
	<script src="<?php echo url('/assets/admin'); ?>/js/main.js"></script>
	<?php foreach ($js_files as $file_name) { ?>
	<script src="<?php echo url('/assets/admin'); ?>/js/<?php echo $file_name; ?>"></script>
	<?php } ?>
        <script type="text/javascript">
            siteUrl = '<?php echo Request::root(); ?>'
        </script>

</body>

</html>