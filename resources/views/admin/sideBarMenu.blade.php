		<nav class="ts-sidebar">
			<ul class="ts-sidebar-menu">
				<li class="ts-label">Search</li>
				<li>
					<input type="text" class="ts-sidebar-search" placeholder="Search here...">
				</li>
				<li class="ts-label">Main</li>
				<li <?php if($currentView == "dashboard"){?>class="open"<?php } ?>><a href="<?php echo url('admin'); ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
				<li <?php if($currentView == "gameCreate"){?>class="open"<?php } ?>><a href="<?php echo url('admin/gameCreate'); ?>"><i class="fa fa-desktop"></i> Create Challenge</a></li>
				<li <?php if($currentView == "users"){?>class="open"<?php } ?>><a href="<?php echo url('admin/users'); ?>"><i class="fa fa-desktop"></i> User Management</a></li>
				
				<!-- Account from above -->
				<ul class="ts-profile-nav">
					<li><a href="#">Help</a></li>
					<li><a href="#">Settings</a></li>
					<li class="ts-account">
						<a href="#"><img src="img/ts-avatar.jpg" class="ts-avatar hidden-side" alt=""> Account <i class="fa fa-angle-down hidden-side"></i></a>
						<ul>
							<li><a href="#">My Account</a></li>
							<li><a href="#">Edit Account</a></li>
							<li><a href="<?php echo url('admin/logout'); ?>">Logout</a></li>
						</ul>
					</li>
				</ul>

			</ul>
		</nav>