@include('admin/headersidebar')
<div class="ts-main-content">
    <?php echo $sideBarMenu; ?>
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">

                    <h2 class="page-title">Create Challenge</h2>
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <form method="get" class="form-horizontal" name="game-form" id="game-form" onsubmit="return false;">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Game Name</label>
                                    <div class="col-sm-10">
                                        <select id="gameSolo" class="form-control input-sm" name="gameName">
                                            <option value="">Select Game</option>
                                            @foreach($MasterData as $games)
                                            <option value="{{ $games->id }}">{{ $games->gameName }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="hr-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Amount</label>
                                    <div class="col-sm-10">
                                        <input type="text" id="gameAmount" name="gameAmount" placeholder="Amount" class="form-control input-sm">
                                    </div>
                                </div>
                                <div class="hr-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Is Solo?</label>
                                    <div class="col-sm-10">
                                        <select id="gameSolo" class="form-control input-sm" name="gameSolo">
                                            <option>Yes</option>
                                            <option>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="hr-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Region</label>
                                    <div class="col-sm-10">
                                        <select id="region_id" class="form-control input-sm" name="region_id">
                                            <option value="">Select Region</option>
                                            @foreach($regionMasterData as $regions)
                                            <option value="{{ $regions->id }}">{{ $regions->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="hr-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Match</label>
                                    <div class="col-sm-10">
                                        <select id="match_id" class="form-control input-sm" name="match_id">
                                            <option value="">Select Match</option>
                                            @foreach($matchMasterData as $matches)
                                            <option value="{{ $matches->id }}">{{ $matches->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="hr-dashed"></div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Valid Upto
                                        <br>
                                    </label>
                                    <div class="col-sm-10">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2" value="24" name="validUpto" checked="">
                                            <label for="inlineRadio2"> 24 </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2" value="36" name="validUpto">
                                            <label for="inlineRadio2"> 36 </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2" value="48" name="validUpto">
                                            <label for="inlineRadio2"> 48 </label>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <button class="btn btn-default cancelGameCreate" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="submit" id="gameCreateBtn">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Challenges</div>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Game Name</th>
                                        <th>Amount</th>
                                        <th>Is Solo?</th>
                                        <th>Valid Upto</th>
                                        <th>Challenge Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($challengeData)  != 0)
                                    @foreach ($challengeData as $game)
                                    <tr>
                                        <td>{{ $game->Game->gameName }}</td>
                                        <td>{{ $game->amount }}</td>
                                        <td>{{ $game->isSolo }}</td>
                                        <td>{{ $game->validUpto }}</td>
                                        @if($game->challengeStatus == 1)
                                        <td>Created</td>
                                        @elseif($game->challengeStatus == 2)
                                        <td>Accepted</td>
                                        @elseif($game->challengeStatus == 3)
                                        <td>Cancelled</td>
                                        @endif
                                    </tr>
                                    @endforeach 
                                    @else
                                    <tr><td colspan="4">{{ 'No Data found.' }}</tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>