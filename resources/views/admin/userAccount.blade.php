@include('admin/headersidebar')
<div class="ts-main-content">
    <?php echo $sideBarMenu; ?>
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <form method="get" class="form-horizontal" name="account-form" id="account-form" onsubmit="return false;">
                                {{ csrf_field() }}
                                <input type="hidden" name="user_id" id='userid' value="{{ $account['user_id'] }}">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Balance</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="amount" placeholder="Balance" class="form-control input-sm" value="{{ $account['amount'] }}">
                                    </div>
                                </div><br/>
                                

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <button class="btn btn-default cancelUserEdit" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
</div>