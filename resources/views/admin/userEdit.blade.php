@include('admin/headersidebar')
<div class="ts-main-content">
    <?php echo $sideBarMenu; ?>
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <form method="get" class="form-horizontal" name="user-form" id="user-form" onsubmit="return false;">
                                {{ csrf_field() }}
                                <input type="hidden" name="id" id='userid' value="{{ $user['id'] }}">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-10">
                                        <input type="text"  name="first_name" placeholder="First Name" class="form-control input-sm" value="{{ $user['user_detail']['first_name'] }}">
                                    </div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-10">
                                        <input type="text"  name="last_name" placeholder="Last Name" class="form-control input-sm" value="{{ $user['user_detail']['last_name'] }}">
                                    </div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Mobile Number</label>
                                    <div class="col-sm-10">
                                        <input type="text"  name="mobile_number" placeholder="Mobile Number" class="form-control input-sm" value="{{ $user['user_detail']['last_name'] }}">
                                    </div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Address 1</label>
                                    <div class="col-sm-10">
                                        <textarea name="address_1" class="form-control input-sm" placeholder="Address 1">{{ $user['user_detail']['address_1'] }}</textarea>
                                    </div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Address 2</label>
                                    <div class="col-sm-10">
                                        <textarea name="address_2" class="form-control input-sm" placeholder="Address 2">{{ $user['user_detail']['address_2'] }}</textarea>
                                    </div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Pincode</label>
                                    <div class="col-sm-10">
                                        <input type="text"  name="pincode" placeholder="Pincode" class="form-control input-sm" value="{{ $user['user_detail']['pincode'] }}">
                                    </div>
                                </div><br/>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">City</label>
                                    <div class="col-sm-10">
                                        <input type="text"  name="city" placeholder="City" class="form-control input-sm" value="{{ $user['user_detail']['city'] }}">
                                    </div>
                                </div><br/>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">State</label>
                                    <div class="col-sm-10">
                                        <input type="text"  name="state" placeholder="State" class="form-control input-sm" value="{{ $user['user_detail']['state'] }}">
                                    </div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Country</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="country_id">
                                            @if(!empty($countries) && count($countries) != 0)
                                            @foreach($countries as $country)
                                            <option <?php if ($user['user_detail']['country_id'] == $country->id) { ?>selected="selected" <?php } ?> value="{{ $country->id }}">{{ $country->name }}</option>
                                            @endforeach
                                            @endif    
                                        </select></div>
                                </div><br/>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Status</label>
                                    <div class="col-sm-10">
                                        <select class="form-control" name="status_id">
                                            @if(!empty($status) && count($status) != 0)
                                            @foreach($status as $statusVal)
                                            <option <?php if ($user['user_detail']['status_id'] == $statusVal->id) { ?>selected="selected" <?php } ?> value="{{ $statusVal->id }}">{{ $statusVal->name }}</option>
                                            @endforeach
                                            @endif    
                                        </select></div>
                                </div><br/>

                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Make Admin
                                        <br>
                                    </label>
                                    <div class="col-sm-10">
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2"  value="yes" name="role" <?php if($user['role'] == 0) { ?>checked="" <?php } ?>>
                                            <label for="inlineRadio2"> Yes </label>
                                        </div>
                                        <div class="radio radio-inline">
                                            <input type="radio" id="inlineRadio2" value="no" name="role" <?php if($user['role'] == 1) { ?>checked="" <?php } ?>>
                                            <label for="inlineRadio2"> No </label>
                                        </div>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <button class="btn btn-default cancelUserEdit" type="button">Cancel</button>
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
</div>