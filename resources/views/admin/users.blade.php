@include('admin/headersidebar')

<div class="ts-main-content">
    <?php echo $sideBarMenu; ?>
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <h2 class="page-title">User Management</h2>
                </div>

                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Users</div>
                        <div class="panel-body">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Status</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(count($usersData)  != 0)
                                    @foreach($usersData as $user)
                                    <tr>
                                        <td>{{ $user['user_detail']['first_name'] }}</td>
                                        <td>{{ $user['user_detail']['last_name'] }}</td>
                                        <td>{{ $user['user_detail']['email'] }}</td>
                                        @if($user['user_detail']['status_id'] == 1)
                                        <td>{{ 'Active' }}</td>
                                        @elseif($user['user_detail']['status_id'] == 2)
                                        <td>{{ 'Inactive' }}</td>
                                        @elseif($user['user_detail']['status_id'] == 3)
                                        <td>{{ 'Deleted' }}</td>
                                        @elseif($user['user_detail']['status_id'] == 4)
                                        <td>{{ 'Locked' }}</td>
                                        @else
                                        <td>{{ 'Suspended' }}</td>
                                        @endif
                                        <td>
                                            <a href="{{ url('admin/user/'.$user['id']) }}">Edit</a> ||
                                            <a href="{{ url('admin/account/'.$user['id']) }}">Manage Account</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                    @else
                                    <tr><td colspan="4">{{ 'No Data found.' }}</td></tr>
                                    @endif
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
</div>