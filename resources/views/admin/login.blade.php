
	
	<div class="login-page bk-img" style="background-image: url(<?php echo url('/assets/admin'); ?>/img/login-bg.jpg);">
		<div class="form-content">
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<h1 class="text-center text-bold text-light mt-4x" style="margin-top:300px;">Sign in</h1>
						<div class="well row pt-2x pb-3x bk-light" style="opacity:0.9; ">
							<div class="col-md-8 col-md-offset-2">
								<form action="" class="mt" onsubmit="return false;">

									<label for="" class="text-uppercase text-sm">Your Username or Email</label>
									<input id="email" type="text" placeholder="Username" class="form-control mb">

									<label for="" class="text-uppercase text-sm">Password</label>
									<input id="password" type="password" placeholder="Password" class="form-control mb">

									<div class="checkbox checkbox-circle checkbox-info">
										<input id="checkbox7" type="checkbox" checked>
										<label for="checkbox7">
											Keep me signed in
										</label>
									</div>
									<button id="loginBtn" class="btn btn-primary btn-block" type="submit"><span id="loginBtnTxt">LOGIN</span></button>

								</form>
							</div>
						</div>
						<div class="text-center text-light">
							<a href="#" class="text-light">Forgot password?</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
