@include('admin/headersidebar')
<div class="ts-main-content">
    <?php echo $sideBarMenu; ?>
    <div class="content-wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <form method="get" class="form-horizontal" name="setting-form" id="setting-form" onsubmit="return false;">
                                {{ csrf_field() }}
                                <input type="hidden" name="id"  value="{{ $setting['id'] }}">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Commision</label>
                                    <div class="col-sm-10">
                                        <input type="number" name="commision" placeholder="Commision" class="form-control input-sm" value="{{ $setting['commision'] }}">
                                    </div>
                                </div><br/>
                                

                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-2">
                                        <button class="btn btn-default" type="button" onclick="">Cancel</button>
                                        <button class="btn btn-primary" type="submit">Save</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
</div>
</div>