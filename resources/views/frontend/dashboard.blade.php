<div id="RightPannel" class="content-right">
    <div class="col-xs-12 esport_game_banner_box">
        <h3 class="esport-subpage-gametitle">Counter Strike:Go</h3>
    </div>
    <div class="col-xs-12 mo_pad10">
        <div class="esport-tournament-chalange-box">
            <div class="col-xs-12 col-sm-6 tc-box">
                <div class="full">
                    <div class="tour-chal-box">
                        <a href="#">
                            <img src="{{URL::asset('assets/frontend/images/tournament-bg.jpg')}}">
                            <span><mr>Esport Colosseum</mr><br> Tournament</span>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 col-sm-6 tc-box">
                <div class="full">
                    <div class="tour-chal-box">
                        <a href="#">
                            <img src="{{URL::asset('assets/frontend/images/chalange-bg.jpg')}}">
                            <span><mr>Open</mr><br> Challenges</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xs-12">
        <div class="col-xs-12 user-history">
            <div class="full">
                <h3 class="full esport-header">History</h3>
                <div class="full bg-dot"></div>
            </div>
            <div class="col-xs-12 mo_pad0 history_main">
                <table class="table history_tbl">
                    <thead class="thead-inverse">
                    <tr>
                        <th style="text-align:center;">#</th>
                        <th>Game Name</th>
                        <th>Opponent</th>
                        <th>Result</th>
                        <th>Coins</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td>Dota-2</td>
                        <td>Kavit Varma</td>
                        <td>win</td>
                        <td>1,000</td>
                    </tr>
                    <tr>
                        <th scope="row">2</th>
                        <td>Dota-2</td>
                        <td>Kavit Varma</td>
                        <td>win</td>
                        <td>1,000</td>
                    </tr>
                    <tr>
                        <th scope="row">3</th>
                        <td>Dota-2</td>
                        <td>Kavit Varma</td>
                        <td>win</td>
                        <td>1,000</td>
                    </tr>
                    <tr>
                        <th scope="row">4</th>
                        <td>Dota-2</td>
                        <td>Kavit Varma</td>
                        <td>win</td>
                        <td>1,000</td>
                    </tr>
                    <tr>
                        <th scope="row">5</th>
                        <td>Dota-2</td>
                        <td>Kavit Varma</td>
                        <td>win</td>
                        <td>1,000</td>
                    </tr>
                    </tbody>
                </table>
                <div class="col-xs-12 shadow"><img src="{{URL::asset('assets/frontend/images/shadow.png')}}" width="100%;"></div>

            </div>
        </div>
    </div>
</div>