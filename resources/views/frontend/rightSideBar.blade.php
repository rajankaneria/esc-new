<div id="RightPannel" class="content-right" >
    <div class="esport-content">
        <div class="full" data-bind="with:$data.homeViewModel">
            <div class="esport-game-review" data-bind="foreach:$data.challengesModel().challengeListArray">
                <div class="esport-match-result-box" >
                    <div class="esport-user-match-left">
                        <div class="full">
                            <div class="esport-user-img"><img  data-bind="attr:{src:user_image_full_path}" width="40px" height="40px"></div>
                            <div class="esport-user-team">
                                <div class="full esport-user-teamName" data-bind="text:user_full_name!=''?user_full_name:'test'">Vega Squadron Team A</div>
                                <div class="full esport-userName" data-bind="text:username!=''?username:'test'">User name</div>
                            </div>
                            <div class="esport-trans-bg">
                                <div class="f-left"><img src="{{URL::asset('assets/frontend/images/coin-icon.png')}}"></div>
                                <div class="esport-money" data-bind="text:amount">1,000</div>
                            </div>

                        </div>
                    </div>
                    <div class="esport-user-match-right">
                        <div class="right-sideTeam">
                            <div class="full">
                                <div class="esport-user-img-right"><img data-bind="attr:{src:opponent.user_image_full_path}" width="40px" height="40px"></div>
                                <div class="esport-user-team-right">
                                    <div class="full esport-user-teamName-right" data-bind="text:opponent.user_full_name">Vega Squadron Team B</div>
                                    <div class="full esport-userName-right" data-bind="text:opponent.username">User name</div>
                                </div>
                                <div class="esport-trans-bg-right">
                                    <div class="esport-money" data-bind="text:challenge_created_date" style="padding:0 0 0 0px;">01-09-2016 04:00 PM</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="esport-vs-box">Vs.</div>
                </div>


            </div>
            <div class="esport-pagination" data-bind="if:$data.challengesModel().challengeListArray().length>0" >
                <center>
                    <nav aria-label="Page navigation" data-bind="with:$data.pager">
                      <ul class="pagination">
                        <li class="page-item">
                          <a class="page-link" href="#" aria-label="Previous" data-bind="click:previousPage,disable :currentPage ==1">
                            <span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
                            <span class="sr-only">Previous</span>
                          </a>
                        </li>
                        <!-- ko foreach: pagesToShow -->
                            <li class="page-item" data-bind="css: {active : $parent.currentPage == ($index+1) }"><a class="page-link" href="#" data-bind="click:$parent.gotoPage.bind($index+1),text:($index() + 1)">1</a></li>
                        <!-- /ko  -->
                        <li class="page-item">
                          <a class="page-link" href="#" aria-label="Next" data-bind="click:nextPage">
                            <span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
                            <span class="sr-only">Next</span>
                          </a>
                        </li>
                      </ul>
                    </nav>
                </center>
            </div>
        </div>

        <div class="esport-featuredGame">
            <div class="full">
                <h3 class="full esport-header">Featured <span>Games</span></h3>
                <div class="full bg-dot"></div>
            </div>

            <div class="full featuredgame-sec">

                <div id="owl-esport">
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img1.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img2.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img3.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img1.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img2.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img3.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img1.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img2.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img3.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                  <div class="item">
                    <a href="javascript:void(0);"><img src="{{URL::asset('assets/frontend/images/featuredgame-img1.png')}}" alt="Featured Games"></a>
                    <div class="game-title">Morbi vel ipsum vel augue mattis ultricies non et mauris</div>

                  </div>
                </div>
                <div class="esport-all"><a href="javascript:void(0);">See all Games <i class="fa fa-angle-double-right"></i></a></div>


            </div>
        </div>

        <div class="esport-blog">
            <div class="full">
                <h3 class="full esport-header" style="margin-top:0px;">Blog</h3>
                <div class="full bg-dot"></div>
            </div>

            <div class="full blog-sec">
                <div class="col-xs-12 col-sm-4 esport-blog-box">
                    <div class="full bg-black">
                        <div class="esport-blog-img"><img src="{{URL::asset('assets/frontend/images/user1.png')}}" class="img-responsive"></div>
                        <div class="esport-bolg-title">
                            <h5>FNATIC AND GODSENT ROSTER CHANGES </h5>
                            <span>Written by Jess Colwill</span>
                        </div>
                    </div>
                    <div class="bg-shadow"><img src="{{URL::asset('assets/frontend/images/shadow.png')}}" width="100%"></div>
                </div>
                <div class="col-xs-12 col-sm-4 esport-blog-box">
                    <div class="full bg-black">
                        <div class="esport-blog-img"><img src="{{URL::asset('assets/frontend/images/user2.png')}}" class="img-responsive"></div>
                        <div class="esport-bolg-title">
                            <h5>FNATIC AND GODSENT ROSTER CHANGES </h5>
                            <span>Written by Jess Colwill</span>
                        </div>
                    </div>
                    <div class="bg-shadow"><img src="{{URL::asset('assets/frontend/images/shadow.png')}}" width="100%"></div>
                </div>
                <div class="col-xs-12 col-sm-4 esport-blog-box">
                    <div class="full bg-black">
                        <div class="esport-blog-img"><img src="{{URL::asset('assets/frontend/images/user1.png')}}" class="img-responsive"></div>
                        <div class="esport-bolg-title">
                            <h5>FNATIC AND GODSENT ROSTER CHANGES </h5>
                            <span>Written by Jess Colwill</span>
                        </div>
                    </div>
                    <div class="bg-shadow"><img src="{{URL::asset('assets/frontend/images/shadow.png')}}" width="100%"></div>
                </div>
            </div>
        </div>
    </div>
</div>