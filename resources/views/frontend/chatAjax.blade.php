@if(isset($chats) && !empty($chats))
@foreach($chats as $chat)
@if($chat->isResult == 0 && $chat->message != "")
        @if($chat->to_id == Session::get('user')->id)
        <div class="from-msg chat-msg">
            <div class="msg-content">
                <div class="username">{{ $chat->from_name }}</div>
                <div class="msg"><span>{{ $chat->message }}</span></div>
                <div class="time">{{ date_format(date_create($chat->created_at), 'g:ia') }}</div>
            </div>
        </div>
        @else
        <div class="to-msg chat-msg">
            <div class="msg-content">
                <div class="username">{{ $chat->from_name }}</div>
                <div class="msg"><span>{{ $chat->message }}</span></div>
                <div class="time">{{ date_format(date_create($chat->created_at), 'g:ia') }}</div>
            </div>
        </div>
        @endif
@else
@if($chat->to_id == Session::get('user')->id)
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Game Name</th>
            <th>Amount</th>
            <th>Winner User Name</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>{{ $chatData["chats"]["gameDetails"]->Game->gameName }}</td>
            <td>{{ $chatData["chats"]["gameDetails"]->amount }}</td>
            <td>{{ $chatData["chats"]["winnerUserDetail"]->first_name }}</td>
        </tr>
    </tbody>
</table>
@endif
@endif
@endforeach
@endif


