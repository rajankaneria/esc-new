<div id="RightPannel" class="content-right" data-bind="with:$root.UserModel()">
    <div class="col-xs-12 no-padding user-banner-main">
        <div class="esport-user_banner"></div>
        <div class="user-avtar-detail">
            <div class="user-avatar"><img src="{{URL::asset('assets/frontend/images/avtar-1.jpg')}}" width="100%"></div>
            <div class="user-detail">
                <div class="full user-name">
                    <h3><span data-bind="text:$data.first_name"></span> <span data-bind="text:$data.last_name"></span></h3>
                    <img src="{{URL::asset('assets/frontend/images/user-flag.png')}}">
                </div>
                <div class="full mar-b3 text-shadow">Last login - <span data-bind="text:$data.last_login">Aug 18, 2016 12:12PM</span></div>
                <div class="full text-shadow">Level - <span>12</span></div>
            </div>
        </div>
        <div class="banner-shadow"></div>
    </div>
    <div class="col-xs-12 mar-b20">
        <div class="col-xs-12 no-padding dashboard-box">
            <div class="col-xs-12 col-sm-3 no-padding mo_width50">
                <div class="user-sapret-box">
                    <div class="clearfix">
                        <div class="user-sapret-box-icon"><i class="fa fa-clock-o border-member member"></i></div>
                        <div class="user-sapret-box-body">
                            <span class="title">member since</span>
                            <span class="description" data-bind="text:$data.last_login">Aug 18, 2016</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 no-padding mo_width50">
                <div class="user-sapret-box">
                    <div class="clearfix">
                        <div class="user-sapret-box-icon"><i class="fa fa-database border-coins coins"></i></div>
                        <div class="user-sapret-box-body">
                            <span class="title">total coins</span>
                            <span class="description">0.00</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 no-padding mo_width50">
                <div class="user-sapret-box">
                    <div class="clearfix">
                        <div class="user-sapret-box-icon"><i class="fa fa-trophy border-achievement achievement"></i></div>
                        <div class="user-sapret-box-body">
                            <span class="title">achievement</span>
                            <span class="description">0</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-3 no-padding mo_width50">
                <div class="user-sapret-box">
                    <div class="clearfix">
                        <div class="user-sapret-box-icon"><i class="fa fa-line-chart border-rank rank"></i></div>
                        <div class="user-sapret-box-body">
                            <span class="title">esport rank</span>
                            <span class="description">0</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 mar-b20" data-bind="with:$root.GamesDataModel">
        <div class="col-xs-12 mar-b20 dashboard-box">
            <div class="full mar-b20">
                <h3 class="full esport-header">Games Playing</h3>
                <div class="full bg-dot"></div>
            </div>

            <div class="col-xs-12 no-padding">
                <div class="full mar-b20 ">
                    <div class="full gameplay-box">
                        <div class="col-xs-12 gameplay-box-leftside">
                            <img src="{{URL::asset('assets/frontend/images/dota-gameplayimg.png')}}">
                            <span class="gameplay-box-gamename">Dota-2 </span>
                        </div>
                        <div class="col-xs-12 no-padding gameplay-box-rightside">
                            <div class="full mar-b5">
                                <div class="pull-left gameplay-rightsideTop-header">Game Statstics</div>
                                <div class="pull-right gameplay-rightsideTop-buttons">
                                    <button id="gameplay-tournament" class="btn btn-gameplay active">Tournament</button>
                                    <button id="gameplay-team" class="btn btn-gameplay">Team</button>
                                </div>
                            </div>
                            <div id="gameplay-content-tournament" class="full gameplay-rightside-content">
                                <div class="full gameplay-content-top">
                                    <i class="fa fa-user"></i>
                                    <span>Turenament Statstics</span>
                                </div>
                                <div class="full gameplay-content-statestics">
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p100 small">
                                                        <span data-bind="text:$data.totalGames">20</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">total match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-gamepad"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 small green" id="win_percentage">
                                                        <span data-bind="text:$data.won_games">15</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">win match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-trophy"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100  small orange" id="lost_percentage">
                                                        <span data-bind="text:$data.lost_games">05</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">loss match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-star"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-progress">
                                    <div class="row">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" id="win_progress_bar">
                                                <span data-bind="text:$data.won_percentage"></span>%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-result-box">
                                    <div class="pull-left gameplay-result-title">Last 10 match result</div>
                                    <div class="pull-right gameplay-result">
                                        <ul data-bind="foreach: $data.last_ten_matches">
                                            <li  data-bind="css: {'gameplay-result-loss' : $data.win_result() == 0,'gameplay-result-win' : $data.win_result() == 1 }">
                                                <span data-bind="text:$data.win_result()==1?'W':'L'">W</span>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="gameplay-content-team" class="full gameplay-rightside-content">
                                <div class="full gameplay-content-top">
                                    <i class="fa fa-users"></i>
                                    <span>Team Blue Sky</span>
                                </div>
                                <div class="full gameplay-content-statestics">
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p100 small">
                                                        <span>20</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">total match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-gamepad"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p75 small green">
                                                        <span>15</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">win match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-trophy"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p25 small orange">
                                                        <span>05</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">loss match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-star"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-progress">
                                    <div class="row">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                                75%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-result-box">
                                    <div class="pull-left gameplay-result-title">Last 10 match result</div>
                                    <div class="pull-right gameplay-result">
                                        <ul>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full"><img class="gameplay-boxshadow" src="{{URL::asset('assets/frontend/images/shadow.png')}}" width="100%"></div>
                </div>

                {{--<div class="full mar-b20 ">
                    <div class="full gameplay-box">
                        <div class="col-xs-12 gameplay-box-leftside">
                            <img src="{{URL::asset('assets/frontend/images/counterstrike-gameplayimg-img.png')}}">
                            <span class="gameplay-box-gamename">counter strike : go </span>
                        </div>
                        <div class="col-xs-12 no-padding gameplay-box-rightside">
                            <div class="full mar-b5">
                                <div class="pull-left gameplay-rightsideTop-header">Game Statstics</div>
                                <div class="pull-right gameplay-rightsideTop-buttons">
                                    <button id="gameplay-tournament1" class="btn btn-gameplay active">Tournament</button>
                                    <button id="gameplay-team1" class="btn btn-gameplay">Team</button>
                                </div>
                            </div>
                            <div id="gameplay-content-tournament1" class="full gameplay-rightside-content">
                                <div class="full gameplay-content-top">
                                    <i class="fa fa-user"></i>
                                    <span>Turenament Statstics</span>
                                </div>
                                <div class="full gameplay-content-statestics">
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p100 small">
                                                        <span>20</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">total match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-gamepad"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p75 small green">
                                                        <span>15</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">win match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-trophy"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p25 small orange">
                                                        <span>05</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">loss match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-star"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-progress">
                                    <div class="row">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                                75%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-result-box">
                                    <div class="pull-left gameplay-result-title">Last 10 match result</div>
                                    <div class="pull-right gameplay-result">
                                        <ul>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="gameplay-content-team1" class="full gameplay-rightside-content">
                                <div class="full gameplay-content-top">
                                    <i class="fa fa-users"></i>
                                    <span>Team Blue Sky</span>
                                </div>
                                <div class="full gameplay-content-statestics">
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p100 small">
                                                        <span>20</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">total match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-gamepad"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p75 small green">
                                                        <span>15</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">win match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-trophy"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-xs-12 col-sm-4 pad-lr10 mo_cols3">
                                        <div class="clearfix">
                                            <div class="gameplay-content-statesticsBox">
                                                <center>
                                                    <div class="c100 p25 small orange">
                                                        <span>05</span>
                                                        <div class="slice">
                                                            <div class="bar"></div>
                                                            <div class="fill"></div>
                                                        </div>
                                                    </div>
                                                    <div class="gameplay-content-statestics-name">loss match</div>
                                                    <div class="gameplay-content-statestics-icon"><i class="fa fa-star"></i></div>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-progress">
                                    <div class="row">
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-theme" role="progressbar" aria-valuenow="75" aria-valuemin="0" aria-valuemax="100" style="width: 75%;">
                                                75%
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="full gameplay-result-box">
                                    <div class="pull-left gameplay-result-title">Last 10 match result</div>
                                    <div class="pull-right gameplay-result">
                                        <ul>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-win"><span>W</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                            <li class="gameplay-result-loss"><span>L</span></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="full"><img class="gameplay-boxshadow" src="{{URL::asset('assets/frontend/images/shadow.png')}}" width="100%"></div>
                </div>--}}
            </div>
        </div>
    </div>
</div>