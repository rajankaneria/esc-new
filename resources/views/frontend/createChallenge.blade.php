<div class="main-content-container">
    <div id="menu_type">
        @include('frontend/leftSideBar')
        <div class="content">
            <form method="post" action="" name="game-form" id="game-form" onsubmit="return false;">
                {{ csrf_field() }}
                <div class="form-group">
                    <span class="input-wrap">
                        <select id="gameSolo" class="form-control valid" name="gameName">
                            <option value="">Select Game</option>
                            @foreach($MasterData as $games)
                            <option value="{{ $games->id }}">{{ $games->gameName }}</option>
                            @endforeach
                        </select>
                    </span>
                </div>

                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="Amount" class="form-control valid"  id="gameAmount" name="gameAmount" type="text">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <select id="gameSolo" class="form-control valid" name="gameSolo">
                            <option>Yes</option>
                            <option>No</option>
                        </select>
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <select id="region_id" class="form-control input-sm" name="region_id">
                            <option value="">Select Region</option>
                            @foreach($regionMasterData as $regions)
                            <option value="{{ $regions->id }}">{{ $regions->name }}</option>
                            @endforeach
                        </select>
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <select id="match_id" class="form-control input-sm" name="match_id">
                            <option value="">Select Match</option>
                            @foreach($matchMasterData as $matches)
                            <option value="{{ $matches->id }}">{{ $matches->name }}</option>
                            @endforeach
                        </select>
                    </span>
                </div>

                <div class="form-group">
                    <span class="input-wrap">
                        <div class="radio radio-inline">
                            <input type="radio" id="inlineRadio2" value="24" name="validUpto" checked="">
                            <label for="inlineRadio2"> 24 </label>
                        </div>
                        <div class="radio radio-inline">
                            <input type="radio" id="inlineRadio2" value="36" name="validUpto">
                            <label for="inlineRadio2"> 36 </label>
                        </div>
                        <div class="radio radio-inline">
                            <input type="radio" id="inlineRadio2" value="48" name="validUpto">
                            <label for="inlineRadio2"> 48 </label>
                        </div>
                    </span>
                </div>

                <button class="btn btn-default cancelGameCreate" type="button">Cancel</button>
                <button class="btn btn-primary" type="submit" id="gameCreateBtn">Save</button>
            </form>  
        </div>
        @include('frontend/rightSideBar')
    </div>
</div>