<section id="footer">
    	<div class="footer">
        	<div class="top-footer">
            	<div class="f-left">
                	<ul id="esport-footer-links">
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">Home</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">About</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">FAQ</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">Press</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">Contact</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">Careers</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">Privacy</a></li>
                    	<li><a href="javascript:void(0);" class="hvr-underline-from-center">Legal</a></li>
                    </ul>
                </div>
                <div class="f-right topfoot-right">
                	<div class="f-left text-white esport-contactNo">1-222-333-4567</div>
                	<div class="f-left esport-mail"><a href="javascript:void(0);" class="hvr-underline-from-center text-white">support @esport.com</a></div>
                </div>
            </div>
        	<div class="bottom-footer">
            	<div class="esport-footer-logo">
                	<div class="f-left"><img src="{{URL::asset('assets/frontend/images/spon-1.png')}}"></div>
                	<div class="f-left"><img src="{{URL::asset('assets/frontend/images/spon-2.png')}}"></div>
                	<div class="f-left"><img src="{{URL::asset('assets/frontend/images/spon-3.png')}}"></div>
                </div>
                <div class="f-right esport-botfoot-right"><span>Copyright © 2015</span> <a href="javascript:void(0);" class="hvr-underline-from-center">All rights reserved by Esport</a></div>
            </div>
        </div>
    </section>
  
	<!--Modal Signup Popups-->
<?php
		if(!empty($include)){
	foreach($include  as $item){ ?>
		@include($item)
<?php
	}
	}
?>
    
  </body>

</html>