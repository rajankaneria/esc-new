<div id="open-leftPannel" class="left-pannel-arrow" ><a href="javascript:void(0);" ><i class="fa fa-chevron-right"></i></a></div>
<div class="right-pannel-arrow"><a href="javascript:void(0);"><i class="fa fa-chevron-left"></i></a></div>
<div id="LeftPannel" style="position: fixed;"class="content-left">
    <div class="acesso_bg"></div>
    <div class="esport-gamelist-header">Games</div>

    <div class="esport-gamelist" >
        <?php
            if(isset($gamesList) && !empty($gamesList)){
                $defaultGameID = $gamesList[0]->id;
                foreach($gamesList as $game){
                    $gameImage = "cs-game.png";
                    if($game->image){
                        $gameImage = $game->image;
                    }
        ?>
        <div class="esport-game-img">
            <a href="{{ URL::to('/game/'.$game->id) }}" >
                <img src="{{URL::asset('assets/frontend/images/game_images/'.$gameImage)}}" class="img-responsive">
                <div class="ImageOverlayH"></div>
                <span><?php echo $game->gameName; ?></span>
            </a>
        </div>
        <?php
                }
            }
        ?>
    </div>
</div>