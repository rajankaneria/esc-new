<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <img class="img-circle" id="img_logo" src="http://bootsnipp.com/img/logo.jpg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                </button>
            </div>

            <!-- Begin # DIV Form -->
            <div id="div-forms">

                <!-- Begin # Login Form -->
                <div class="body register">
                    <div section="quest">
                        <div class="clearfix" style="height: 100%;">
                            <h5>Login to eSportColosseum
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </h5>
                            <div class="col-md-12 login_box">
                                <!--<a class="close-btn" href="#">×</a>-->
                                <form role="form" action="" method="post" name="form2" id="login-popup" onsubmit="return false;">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <span class="input-wrap">
                                            <i class="fa fa-user"></i>
                                            <input type="email" name="email_address" id="email_address" class="form-control" placeholder="Email">
                                        </span>
                                    </div>
                                    <div class="form-group">
                                        <span class="input-wrap">
                                            <i class="fa fa-unlock"></i>
                                            <input type="password" name="login_password" id="login_password" class="form-control" placeholder="Password">
                                        </span>
                                    </div>
                                    <div class="form-actions">
                                        <button type="submit" class="btn buttons new default lg caps get-started" vartrans="signup_getstarted">Get Started</button>
                                        <br>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- End # Login Form -->
            </div>
            <!-- End # DIV Form -->

        </div>
    </div>
</div>
<script type="text/javascript">
                        $(document).ready(function () {
                            $("#login-popup").validate({
                                // Specify the validation rules
                                rules: {
                                    email_address: {
                                        required: true,
                                        email: true
                                    },
                                    login_password: {
                                        required: true,
                                    },
                                },
                                // Specify the validation error messages
                                messages: {
                                    email_address: "Please enter a valid email address",
                                    login_password: {
                                        required: "Please provide a password",
                                    },
                                },
                                submitHandler: function (form) {
                                    $.ajaxSetup({
                                        header: $('meta[name="_token"]').attr('content')
                                    });
                                    $.ajax({
                                        type: "POST",
                                        url: 'doLogin',
                                        data: $(form).serialize(),
                                        dataType: 'json',
                                        success: function (response) {
                                            if (response == 1)
                                            {
                                                window.location.reload();
                                            } else if (response == 0)
                                            {
                                                alert('Incorrect email address or password.');
                                            }
                                        },
                                        error: function (data) {

                                        }
                                    })
                                }
                            });
                        });
</script>