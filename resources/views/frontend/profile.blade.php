<div id="RightPannel" class="content-right">

    <div class="col-xs-12 mar-b20">
        <div class="col-xs-12 mar-b20 dashboard-box" style="position: relative; top:90px; margin-bottom: 20px;">
            <div class="full mar-b20" >
                <h3 class="full esport-header">Profile Update</h3>
                <div class="full bg-dot"></div>
            </div>

            <div class="col-xs-12 no-padding" style="min-height: 650px; color:#000 !important;">
                <form data-bind="with:UserModel()">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            First Name:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:first_name">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Last Name:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:last_name">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Mobile Number:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:mobile_number">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Email
                        </div>
                        <div class="col-md-5">
                            <span data-bind="text:email"></span>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Address 1:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:address_1">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Address 2:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:address_2">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Pincode:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:pincode">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            City:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:city">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            State:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:state">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Country:
                        </div>
                        <div class="col-md-5">
                            <select data-bind="options: $parent.countryArray(), optionsCaption: 'Select country',optionsText:'name', optionsValue: 'id', value: country_id"></select>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            <input type="button" data-bind="click:$parent.saveUserData" value="Save">
                        </div>
                    </div>
                    <div  class="row success_message" data-bind="if:$parent.displaySuccessMessage" style="margin-bottom: 10px;">
                        <div class="col-md-12" data-bind="text:$parent.successMessage"></div>
                    </div>
                </form>
            </div>
            <div class="col-xs-12 no-padding" style="min-height: 650px; color:#000 !important;">
                <div class="full mar-b20" >
                    <h3 class="full esport-header">Connect</h3>
                    <div class="full bg-dot"></div>
                </div>
                <form data-bind="with:requestModel()">
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Enter Name:
                        </div>
                        <div class="col-md-5">
                            <input data-bind="value:requestName">
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            Select region:
                        </div>
                        <div class="col-md-5">
                            <select data-bind="options: $parent.regionListArray(), optionsCaption: 'Select region',optionsText:'region', optionsValue: 'region_id', value: region_id"></select>
                        </div>
                    </div>
                    <div class="row" style="margin-bottom: 10px;">
                        <div class="col-md-2">
                            <input type="button" data-bind="click:$parent.connectApi" value="Connect">
                        </div>
                    </div>
                    <div  class="row" data-bind="if:$parent.displayConnectSuccessMessage,css:{errorMessage: $parent.errorClass() == 1,success_message :$parent.errorClass() == 0} " style="margin-bottom: 10px;">
                        <div class="col-md-12" data-bind="text:$parent.successMessage"></div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>