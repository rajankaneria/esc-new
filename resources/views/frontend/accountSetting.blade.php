<div class="main-content-container">
    <div id="menu_type">
        @include('frontend/leftSideBar')
        <div class="content">
            <form method="post" action="" name="profileForm" id="profileForm" onsubmit="return false;">
                {{ csrf_field() }}
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="Username" class="form-control valid" id="username" name="username" type="text" value="{{ $profile->username }}">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="First Name" class="form-control valid" id="email" name="first_name" type="text" value="{{ $profile->first_name }}">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="Last Name" class="form-control valid" id="email" name="last_name" type="text" value="{{ $profile->last_name }}">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="Mobile Number" class="form-control valid" id="email" name="mobile_number" type="text" value="{{ $profile->mobile_number }}">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <textarea placeholder="Address1" name="address_1" class="form-control valid">{{ $profile->address_1 }}</textarea>
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <textarea placeholder="Address2" name="address_2" class="form-control valid">{{ $profile->address_2 }}</textarea>
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="Pincode" class="form-control valid" id="email" name="pincode" type="text" value="{{ $profile->pincode }}">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="City" class="form-control valid" id="email" name="city" type="text" value="{{ $profile->city }}">
                    </span>
                </div>
                <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="State" class="form-control valid" id="email" name="state" type="text" value="{{ $profile->state }}">
                    </span>
                </div>
                <div class="form-group">
                    <select class="form-control" name="country_id">
                            @if(!empty($countries) && count($countries) != 0)
                                @foreach($countries as $country)
                                <option <?php if($profile->country_id == $country->id) { ?>selected="selected" <?php } ?> value="{{ $country->id }}">{{ $country->name }}</option>
                                @endforeach
                            @endif    
                        </select>
                </div>
                @if(isset($steamData))
                 <div class="form-group">
                    <span class="input-wrap">
                        <input placeholder="Steam Id" class="form-control valid" id="email" readonly="readonly" name="steamid" type="text" value="{{ $steamData->steam_id }}">
                    </span>
                </div>
                @else
                <button type="button" onclick="steamLogin();">Connect with Steam</button><br/><br/>
                @endif
                <button type="button" class="profileSubmit">Save</button>
            </form>  
        </div>
        @include('frontend/rightSideBar')
    </div>
</div>