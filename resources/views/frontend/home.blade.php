<!-- home page -->

<?php
        if(isset($mainContent) && isset($mainContent["gamesList"])){
            $gamesList = $mainContent["gamesList"];

        }
?>

@include("frontend/header")
@include("frontend/navbar")
@include("frontend/banner")
@include("frontend/leftSideBar")
@include('frontend/'.$contentView)


<!--Modal Popups-->
@include("frontend/footer")


<?php
if(!empty($jsRequire)){
foreach($jsRequire as $fileName){
?>
<script type="text/javascript" src="{{URL::asset('assets/frontend/js/'.$fileName)}}"></script>
<?php

}
}
?>
<script type="text/javascript" src="{{URL::asset('assets/frontend/js/models/basemodel.js')}}"></script>
