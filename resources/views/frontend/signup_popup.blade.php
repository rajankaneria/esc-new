<!--Modal Signup Popups-->
<div class="modal fade" id="myModal_signup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
      	<div class="heros"><img src="{{URL::asset('assets/frontend/images/heros2.png')}}"></div>
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title" id="myModalLabel">Esport Registration</h4>
          </div>
          <div class="modal-body">
              <div data-bind="with:$data.signUpViewModel.UserModel()">
                    <div class="form-group">
                        <label>Email Address</label>
                        <input data-bind="value:$data.email,decorateErrorElement:email,attr:{'placeholder':'Email'}" type="email" class="form-control signup-input">
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input data-bind="value:$data.password,decorateErrorElement:password,attr:{'placeholder':'Password'}" type="password" class="form-control signup-input">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input data-bind="value:$data.cPassword,decorateErrorElement:cPassword,attr:{'placeholder':'Confirm password'}" type="password" class="form-control signup-input">
                    </div>
                    <div class="form-group">
                    	<button type="button" data-bind="click:$parent.signUpViewModel.saveUserData" class="btn btn-theme full">Sign up and play!</button>
                    </div>
                    <!--<div class="s5"></div>
                    <p class="separator-line text-center">or</p>
                    <div class="s5"></div>
                    <div class="form-group">
                    	<button type="button" class="btn btn-fb full">Sign up with Facebook</button>
                    </div>-->
                    <div class="s10"></div>
                </div>
          </div>
          <div class="modal-footer">
            <strong class="pull-left">Already registered?</strong>
            <p><a href="javascript:void(0);" class="text-white">Log in now </a></p>

          </div>
        </div>
      </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{URL::asset('assets/frontend/js/jquery.min.js')}}"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="{{URL::asset('assets/frontend/js/bootstrap.min.js')}}"></script>
	<script src="{{URL::asset('assets/frontend/js/owl.carousel.js')}}"></script>

  <style>
  .small{background-color:rgba(0,0,0,1) !important;}
  .small .top-login{line-height:50px;}
   .small .logo{line-height:50px;}
    .small.head{min-height:50px; !important;}
  </style>
    <script type="text/javascript">
    $(function(){
		$('#header_nav').data('size','big');
	});

//left
var offset = $('.content-left').offset().top;

$(window).scroll(function(){
    if($(document).scrollTop() > 0)
    {
        if($('#header_nav').data('size') == 'big')
        {
            $('#header_nav').data('size','small');
			 $('#header_nav .logo img').stop().animate({
                height:'25px'
            },600);
			 $('#header_nav').stop().animate({
                height:'40px', position:'',top:''
            },600);
			 $('#header_nav').addClass('small');
        }
    }
    else
    {
        if($('#header_nav').data('size') == 'small')
        {
			$('#header_nav .logo img').stop().animate({
                height:'50px'
            },600);
            $('#header_nav').data('size','big');
			 $('#header_nav').removeClass('small');
            $('#header_nav .top-login').stop().animate({
                height:'80px', position:'',top:'0'
            },600);



        }
    }

	//leftside
	var lefttop = parseInt(offset) - $('#header_nav').height();
	var leftscroll = $(document).scrollTop() + 5;
	if(leftscroll  > lefttop )
	{
		$('.content-left').css('position','fixed');
		$('.content-left').css('top',$('#header_nav').height() );
	}
	else{
		$('.content-left').css('position','');
		$('.content-left').css('top','0');
	}
});


	//feeatured game slider

    $(document).ready(function() {

      $("#owl-esport").owlCarousel({

          autoPlay: 4000, //Set AutoPlay to 3 seconds

          items : 4,
          itemsDesktop : [1199,3],
          itemsDesktopSmall : [979,3]

      });

	  $("#open-leftPannel").click(function(e) {
		if($('#RightPannel').is(':visible'))
		{
			$('#RightPannel').hide(1000);
			$('#LeftPannel').show(1000);
			$('#content').addClass('open');
		}
		else
		{
			$('#LeftPannel').hide(1000);
			$('#RightPannel').show(1000);
			$('#content').removeClass('open');
		}
      });

    });


    </script>