<!DOCTYPE html>
<!--[if lt IE 7]>      <html lang="en" class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html lang="en" class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html lang="en" class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script>
        window.baseUrl = "<?php echo Request::root(); ?>";

    </script>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Esport Colosseum</title>

    <!-- Bootstrap -->
    <link href="{{URL::asset('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" media="all" >
    <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/font-awesome.min.css')}}">


    <link href="{{URL::asset('assets/frontend/css/hover.css')}}" rel="stylesheet">
    <!-- Important Owl stylesheet -->
    <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/owl.carousel.css')}}">
     
    <!-- Default Theme -->
    <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/owl.theme.css')}}">
    <link href="{{URL::asset('assets/frontend/css/animation.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{URL::asset('assets/frontend/css/circle.css')}}">
    <link href="{{URL::asset('assets/frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{URL::asset('assets/frontend/css/responsive.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="{{URL::asset('assets/frontend/js/html5shiv.min.js')}}"></script>
        <script src="{{URL::asset('assets/frontend/js/respond.min.js')}}"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{URL::asset('assets/frontend/js/jquery.min.js')}}"></script>

    <script src="{{URL::asset('assets/frontend/js/knockout-3.4.0.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/frontend/js/knockout.mapping-latest.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/frontend/js/knockout.validation.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/frontend/js/common.js')}}" type="text/javascript"></script>
    <script src="{{URL::asset('assets/frontend/js/pager.js')}}" type="text/javascript"></script>
  </head>
  <body>
  <script type="text/javascript">
      var gameID = 0;
      var assestPath = '<?php echo URL::asset('assets/frontend/images/coin-icon.png'); ?>';
      var imagePath = '<?php echo URL::asset('assets/frontend/images/'); ?>';
  </script>
  <?php
      if(isset($mainContent["gameID"])){
      ?>
      <script type="text/javascript">
          gameID = '<?php echo $mainContent["gameID"]; ?>';
      </script>
    <?php
        }else if(isset($gamesList) && !empty($gamesList)){
            $defaultGameID = $gamesList[0]->id;
    ?>
      <script type="text/javascript">
          gameID = '<?php echo $defaultGameID; ?>';
      </script>
    <?php
        }
    ?>
