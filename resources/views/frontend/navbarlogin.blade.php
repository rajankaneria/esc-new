<?php
    $loginUser = $headerData;

?>
<script>
        var loginUserData  = '<?php echo json_encode($loginUser["id"]); ?>';
</script>
<section id="header">
    <div class="head sub_head" id="header_nav"	style="position:fixed;top:0px; z-index:100;">
        <div class="logo mobile-display-none"><a href="javascript:void(0);" class="main_link"><img src="{{URL::asset('assets/frontend/images/logo.png')}}" height="50px"></a></div>
        <div class="logo desktop_none"><a href="javascript:void(0);" class="main_link"><img src="{{URL::asset('assets/frontend/images/small-logo.png')}}" height="50px"></a></div>
        <div class="top-login">
            <ul class="nav navbar-nav navbar-right dashboardNavbar">
                <li><a href="javascript:void(0);" class="main_link"><i class="fa fa-bell"></i> <span class="badge-counter">1</span></a></li>
                <li><a href="javascript:void(0);" class="main_link"><span class="coins-img"><img src="{{URL::asset('assets/frontend/images/coins-512.png')}}" width="25px"></span><span class="mobile-display-none">ADD Coins</span></a></li>
                <li class="dropdown">
                    <a href="javascript:void(0);" class="dropdown-toggle main_link" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <sapn class="avatar">
                            <img src="{{URL::asset('assets/frontend/images/user-img.png')}}">
                        </sapn>
                        <b>
                            <span class="mobile-display-none"><?php print_r($loginUser["email"]); ?></span>
                        </b>
                        <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu user-setting-dropdown animated flipInY">
                        <li class="dropdown-heading">User Settings</li>
                        <li role="separator" class="divider"></li>
                        <li><a href="{{URL::to('/profile')}}">Account</a></li>
                        <li><a href="javascript:void(0);">Personal Info</a></li>
                        <li><a href="javascript:void(0);">Game Settings</a></li>
                        <li><a href="javascript:void(0);">Manage Prizes</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="javascript:void(0);">Feedback</a></li>
                        <li><a href="javascript:void(0);">Supoort</a></li>
                        <li><a href="{{URL::to('/logout')}}">Logout</a></li>
                    </ul>
                </li>
                <li class="desktop_none"><a id="mobile_scondrymenu_arrow" href="javascript:void(0);" class="main_link"><i class="fa fa-chevron-left"></i></a></li>
            </ul>
        </div>
    </div>
</section>