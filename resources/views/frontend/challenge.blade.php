<div class="main-content-container">
    <div id="menu_type">
        @include('frontend/leftSideBar')
        <div class="content">
            <div class="top">
                <div class="marketing feature">
                    <img src="{{URL::asset('assets/frontend/img/header_banner.jpg')}}" class="img-responsive" alt=""/>
                </div>
            </div>
            <div class="hidden-sm hidden-xs" id="main">
                <section class="main">
                    <div class="market-tiles all">
                        <div class="event sc2 bonus competitiveintegrity upcoming">
                            <div class="event-row">

                                <table style="color: black;">
                                    <thead>
                                        <tr>
                                            <th>Game</th>
                                            <th>Created By</th>
                                            <th>Amount</th>
                                            <th>Date & Time</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody data-bind="foreach: challenges">
                                        <tr>
                                            <td data-bind="text: challenge().game"></td>
                                            <td data-bind="text: challenge().creator"></td>
                                            <td data-bind="text: challenge().amount"></td>
                                            <td data-bind="text: challenge().date"></td>
                                            <td>

                                            </td>
                                        </tr>    
                                    </tbody>
                                </table>



                                <?php $userId = 0; ?>
                                @if (Session::has('user'))
                                <?php $userId = Session::get('user')->id ?>
                                @endif
                                <input type="hidden" name="userId" value="{{ $userId }}" id="userid">
                                <input type="hidden" name="challengeId" id="challengeid"> 
                                <input type="hidden" name="amount" id="amounts"> 
                                </form>
                                <div class="btn-block text-center padT20 padB20">
                                    <a href="#">
                                        <img src="{{URL::asset('assets/frontend/img/load_more-btn.jpg')}}" width="162" height="33" alt=""/>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>   
        </div>
        <div class="sidebar visible-md visible-lg fixed">
            <div class="body register">
                <div section="quest">
                    <div style="height: 100%;" class="clearfix">
                        @if (Session::has('user'))
                        @include('frontend/rightSideBar')
                        @else
                        @include('frontend/signup')
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    function SeatReservation(initialMeal) {
        var self = this;
        self.challenge = ko.observable(initialMeal);
    }

// Overall viewmodel for this screen, along with initial state
    function ReservationsViewModel() {
        var self = this;

        // Non-editable catalog data - would come from the server
        self.availableChallenges = [
<?php
foreach ($challanges as $challenge) {
    $effectiveDate = strtotime("+" . $challenge['validUpto'] . " hours", strtotime($challenge['created_at']));
    $effectiveDate = date("Y-m-d h:i:s", $effectiveDate);
    $effectiveDate = date_create($effectiveDate);
    
    ?>
                {game: '<?php echo $challenge['game']['gameName']; ?>', creator: '<?php echo $challenge['user']['first_name']; ?>', amount: '<?php echo $challenge['amount']; ?>', date: '<?php echo date_format($effectiveDate, 'M j, Y / g:i:s')." (IST)"; ?>'},
<?php } ?>
        ];

        // Editable data
        self.challenges = ko.observableArray(
        [
<?php foreach ($challanges as $key => $challenge) { ?>
            new SeatReservation(self.availableChallenges[<?php echo $key; ?>]),
<?php } ?>
        ]
                );
    }

    ko.applyBindings(new ReservationsViewModel());


</script> 