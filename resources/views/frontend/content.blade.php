<!-- home page -->

<?php
if(isset($mainContent) && isset($mainContent["gamesList"])){
    $gamesList = $mainContent["gamesList"];
}
?>
@include("frontend/header")
@include("frontend/navbarlogin")
@include("frontend/leftSideBar")
@include('frontend/'.$contentView)
<!--Modal Popups-->
@include("frontend/footer")

<?php
        if(!empty($jsRequire)){
            foreach($jsRequire as $fileName){
                ?>
                <script type="text/javascript" src="{{URL::asset('assets/frontend/js/'.$fileName)}}"></script>
                <?php

            }
        }
?>

