<div class="content">
        <?php
        $chats = "";
        $chatData = loadChatData();
        if (!empty($chatData["chats"])) {
            $chats = $chatData["chats"];
        }
        if(!empty($chats)){ ?>
    <div class="replaceContent" style="padding: 20px; height: 100%;">
        
        @include('frontend/chatAjax')
    </div>
        <?php } ?>
    <?php if(!empty($chatData['challenge_id'])) { ?>
    <div class="replaceContent1" style="padding: 20px; height: 100%;">
        
    </div>
    <div class="chatForm" style=" bottom: 65px; width: 100%; margin: 10px; padding-right: 30px;">                <form method="post" action="" name="chatForm" id="chatForm" onsubmit="return false;">
            {{ csrf_field() }}
            <input type="text" name="message" id="message" class="form-control"  autocomplete="off"/>
            <?php $userId = 0; ?>
            @if (Session::has('user'))
            <?php $userId = Session::get('user')->id; ?>
            @endif
            <input type="hidden" name="from_id" value="{{ $userId }}" id="fromid">
            <input type="hidden" name="to_id" id="toid" value="{{$chatData['to_id']}}">
            <input type="hidden" name="challenge_id" id="challenge_id" value="{{$chatData['challenge_id']}}" />
        </form>
    </div> 
    
        <?php } else { ?>
    <p> There is no Active challenge.</p>
        <?php }  ?>
</div>
<script type="text/javascript" src="{{URL::asset('assets/frontend/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{URL::asset('assets/frontend/js/chat.js')}}"></script>
<style>
    .to-msg .msg-content{
        color: #ffffff;
        background: #aaadaa;
        padding: 5px 15px;
        margin: 5px;
        border-radius: 7px;
        /* border: 1px solid #9a9a9a; */
        float: right;
        text-align: right;
    }
    .from-msg .msg-content{
        color: #878c87;
        background: #ffffff;
        padding: 5px 15px;
        margin: 5px;
        border-radius: 7px;
        border: 1px solid #aaadaa;
        float: left;
    }
    .chat-msg{
        display: block;
        width: 100%;
        overflow: hidden;
        font-weight: 200;
    }
    .username {
        font-size: 11px;
        color: #ccc;
    }

    .time {
        font-size: 10px;
        color: #ccc;
    }
</style>