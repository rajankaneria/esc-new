<navbar>
    <section id="header">
        <div class="head" id="header_nav"	style="position:fixed;top:0px; z-index:100;">
            <div class="logo"><img src="{{URL::asset('assets/frontend/images/logo.png')}}" height="50px"></div>
            <div class="top-login">
                <ul class="nav navbar-nav navbar-right">
                	
                    <li><p class="navbar-text already-acc no-margin mobile-display-none">Already have an account?</p></li>
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle login-icon" data-toggle="dropdown"><i class="fa fa-user text-white"></i>&nbsp;&nbsp;<b class="text-white"><span class="mobile-display-none">Login</span></b></a>
                      	
                        <ul id="login-dp" class="dropdown-menu animated flipInX">
                            <div class="arrow-up"></div>
                            <li>
                                 <div class="row">
                                        <div class="col-md-12">
                                             <form class="form" role="form" method="post" action="login" accept-charset="UTF-8" id="login-nav" data-bind="with:$data.signUpViewModel.loginUserModel">
                                                    <div class="form-group">
                                                         <label class="sr-only" for="exampleInputEmail2">Email address</label>
                                                         <input data-bind="value:$data.email,decorateErrorElement:email,attr:{'placeholder':'Email'}" type="email" class="form-control" id="exampleInputEmail2" placeholder="Email address" >
                                                    </div>
                                                    <div class="form-group">
                                                         <label class="sr-only" for="exampleInputPassword2">Password</label>
                                                         <input data-bind="value:$data.password,decorateErrorElement:password,attr:{'placeholder':'Password'}" type="password" class="form-control" id="exampleInputPassword2" placeholder="Password" >
                                                         <div class="help-block text-right"><a href="" class="text-white">Forget the password ?</a></div>
                                                    </div>
                                                    <div class="form-group">
                                                        <button data-bind="click:$parent.signUpViewModel.signInUser" type="button" class="btn btn-block btn-theme">Login</button>
                                                    </div>
                                             </form>
                                        </div>
                                        <div class="bottom text-center">
                                            New here ? <a href="#" data-toggle="modal" data-target="#myModal_signup"><b class="text-white">Join Us</b></a>
                                        </div>
                                 </div>
                            </li>
                        </ul>
                    </li>
                  </ul>
            </div>
        </div>

    </section>
</navbar>
