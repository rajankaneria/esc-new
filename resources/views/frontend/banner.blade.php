<navbar>
    <section>
        <div class="banner">
            <div class="banner-img"><img src="{{URL::asset('assets/frontend/images/banner-img.png')}}" class="img-responsive"></div>
            <div class="container mobile-display-none">
                <div class="signup-step">
                    <center>
                        <div class="welcome-logo"><img src="{{URL::asset('assets/frontend/images/steps-logo.png')}}" /></div>
                        <div class="welcome-text"><span>Welcome to</span><br><span class="welcome-name">Esport Colosseum</span></div>
                    </center>

                    <div class="steps stp1">
                        <div class="steps-img"><img src="{{URL::asset('assets/frontend/images/step1-icon.png')}}" width="34"></div>
                        <div class="step-content">Signup in <br><span>Esport Colosseum</span></div>
                    </div>
                    <div class="steps">
                        <div class="steps-img"><img src="{{URL::asset('assets/frontend/images/step2-icon.png')}}"></div>
                        <div class="step-content">Challenge game <br><span>Join Esport Colosseum</span></div>
                    </div>
                    <div class="steps">
                        <div class="steps-img"><img src="{{URL::asset('assets/frontend/images/step3-icon.png')}}"></div>
                        <div class="step-content">Earn Esport Colosseum Cash<br> as you play and Redeem money</div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</navbar>