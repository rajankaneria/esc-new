<div class="sidebar visible-md visible-lg fixed">
    <div class="body register">
        <div section="quest">
            <div style="height: 100%;" class="clearfix">
                <div class="col-md-12 register">
                    <!--<a class="close-btn" href="#">×</a>-->
                    <h5>Sign Up to eSportColosseum</h5>
                    <form role="form" action="" method="post" name="form" id="register-form" onsubmit="return false;">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <span class="input-wrap">
                                <i class="fa fa-user"></i>
                                <input type="text"  placeholder="Email" class="form-control" id="email" name="email">
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="input-wrap">
                                <i class="fa fa-unlock"></i>
                                <input type="password"  placeholder="Password" class="form-control" id="password" name="password">
                            </span>
                        </div>
                        <div class="form-group">
                            <span class="input-wrap">
                                <i class="fa fa-lock"></i>
                                <input type="password"  placeholder="Confirm Password" class="form-control" id="confirmPassword" name="confirmPassword">
                            </span>
                        </div>
                        <div class="form-group">
                            <span style="float:none" class="captch_img">
                                {{getCaptchaImage()}}
                                <img src="image.png" alt="{{getCaptchaImage()}}"/>
                            </span>
                            <span class="input-wrap w_50perc">
                                <input name="captcha" id="captcha" placeholder="Captcha" type="text">
                            </span>
                        </div>
                        <div class="age-limit">
                            <input type="checkbox"  name="identityCheckbox" id="identityCheckbox">
                            <span class="ar-13">I'm over 14 or have parental consent for raffles.</span>
                        </div>
                        <div class="form-actions">
                            <button vartrans="signup_getstarted" class="btn buttons new default lg caps get-started" type="submit">Get Started</button>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="{{URL::asset('assets/frontend/js/jquery.min.js')}}"></script>
<script type="text/javascript">
                        $(document).ready(function () {
                            $("#register-form").validate({
                                // Specify the validation rules
                                rules: {
                                    email: {
                                        required: true,
                                        email: true
                                    },
                                    password: {
                                        required: true,
                                        minlength: 5
                                    },
                                    confirmPassword: {
                                        equalTo: "#password"
                                    },
                                    identityCheckbox: {
                                        required: true
                                    },
                                    captcha: {required: true}

                                },
                                // Specify the validation error messages
                                messages: {
                                    email: "Please enter a valid email address",
                                    password: {
                                        required: "Please provide a password",
                                        minlength: "Your password must be at least 5 characters long"
                                    },
                                    confirmPassword: "Password did not match",
                                    identityCheckbox: "Please check the checkbox.",
                                    captcha: "Please enter captcha."
                                },
                                submitHandler: function (form) {
                                    $.ajaxSetup({
                                        header: $('meta[name="_token"]').attr('content')
                                    });
                                    $.ajax({
                                        type: "POST",
                                        url: 'create',
                                        data: $(form).serialize(),
                                        dataType: 'json',
                                        success: function (response) {
                                            if (response == 1)
                                            {
                                                window.location.reload();
                                            } else if (response == 0)
                                            {
                                                alert('Please use differnt email address.');
                                            } else if (response == -1) {
                                                alert('Please enter correct captcha.');
                                            }
                                        },
                                        error: function (data) {

                                        }
                                    })
                                }
                            });

                        });
</script>